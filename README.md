# Zend Framework 2 Skeleton - Univiçosa

## Usando ActiveMenuHelper
Basta chamar o nome no arquivo view.phtml:

```php
<?php

echo $this->activeMenuHelper('route1', 'route2', 'route3');
```


## Usando PaginatorControllerPlugin (Controller)
O **paginatorControllerPlugin** espera no parâmetro, que o retorno do seu objeto seja do tipo **Zend\Paginator\Paginator**
A chamada do objeto dentro do método do controller:

```php
<?php

use Zend\Mvc\Controller\AbstractActionController;

class ListAllUsers extends AbstractActionController
{
    /**
     * @var ListAllUsersService $service
     */
    private $service;

    /**
     * @param ListAllUsersService $service
     */
    public function __construct(ListAllUsersService $service)
    {
        $this->service = $service;
    }

    /**
     * @return ViewModel
     */
    public function listAllUsersAction()
    {
        return (new ViewModel())
            ->setTemplate('user/list-all-users/index')
            ->setVariable('users', $this->paginatorControllerPlugin($this->service->listAllUsers()));
    }
}
```

## Montando paginação com Doctrine
Para montar sua paginação, basta fazer o examplo a seguir

```php
<?php

namespace User\Repository;

use Core\Doctrine\ObjectManagerAbstract;
use Core\Doctrine\PaginatorCollection\PaginatorCollectionAdapterTrait;
use User\Entity\User;

class ListAllUsersRepository extends ObjectManagerAbstract
{
    use PaginatorCollectionAdapterTrait;

    /**
     * @return User
     */
    public function __invoke()
    {
        $collection = $this->objectManager->getRepository(User::class)->findBy(
            [],
            ['name' => 'ASC']
        );

        /** @method PaginatorCollectionAdapterTrait */
        return $this->mountCollection($collection);
    }
}
```

Você também pode fazer consultas SQL normalmente, com **EntityManager** e retornar o **fetchAll**, por exemplo:
```php
<?php

namespace User\Repository;

use Core\Doctrine\EntityManagerAbstract;
use Core\Doctrine\PaginatorCollection\PaginatorCollectionAdapterTrait;
use User\Entity\User;

class ListAllUsersRepository extends EntityManagerAbstract
{
    use PaginatorCollectionAdapterTrait;

    /**
     * @return \[]
     * @throws \Doctrine\DBAL\DBALException
     */
    public function __invoke()
    {
        $sql = <<<"SQL"
            SELECT * FROM users
SQL;

        $query = $this->entityManager->getConnection()->prepare($sql);
        $query->execute();

        /** @method PaginatorCollectionAdapterTrait */
        return $this->mountCollection($query->fetchAll());
    }
}
```

## Montando paginação na View, configuração da rota e arquivo de configuração:
**users.phtml**
```php
<?php
$paginationControl = $this->paginationControl($this->cvs, 'Sliding', 'layout/components/paginator', [
        'route' => 'users',
        'count' => $this->users->count()
    ]
);
```

Para funcionar sua paginação, é necessário adicionar parâmetro com nome **pagina** dentro do **module.config.php**.
```php
<?php

use Zend\Mvc\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'users' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/users[/:pagina]',
                    'constraints' => [
                        'pagina' => '[0-9]+'
                    ],
                    'defaults' => [
                        'controller' => ListAllUsers::class,
                        'action' => 'onDispatch'
                    ]
                ]
            ]
        ]
    ]
];
```

A paginação também aceita parâmetros adicionais, caso sua página possui filtros de pesquisa. 
**users.phtml**

```php
<?php
$paginationControl = $this->paginationControl($this->cvs, 'Sliding', 'layout/components/paginator', [
        'route' => 'users',
        'count' => $this->users->count(),
        'additionalParameters' => [
            'cidade' => 'Vicosa'
        ]
    ]
);
```

Lembre-se que, o nome da chave dentro de **additionalParameters** é o nome do parâmetro da sua rota. 
**module.config.php**

```php
<?php

use Zend\Mvc\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'users' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/users[/cidade/:cidade][/:pagina]',
                    'constraints' => [
                        'pagina' => '[0-9]+',
                        'cidade' => '[a-zA-Z]+',
                    ],
                    'defaults' => [
                        'controller' => ListAllUsers::class,
                        'action' => 'onDispatch'
                    ]
                ]
            ]
        ]
    ]
];
```

## Paginação - configurações
Para alterar a quantidade de itens exibidos por página, abra o arquivo que fica em **config/paginator/paginator.config.php** e altere o valor da chave **item_count_per_page**.

## Autor
Guilherme P. Nogueira - guilhermenogueira@univicosa.com.br