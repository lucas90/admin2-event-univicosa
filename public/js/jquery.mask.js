/**
 * Created by Lucas on 08/08/2016.
 */

function mascara(o,f){
    v_obj=o;
    v_fun=f;
    setTimeout("execmascara()",1);
}

function execmascara(){
    v_obj.value=v_fun(v_obj.value);
}

function maskOnlyNumber(v){
    return v.replace(/\D/g,"");
}

function voidExclude(v){
    str = v.trim();
    if(str === ""){
        return v.replace(" ","");
    }
    return v;
}