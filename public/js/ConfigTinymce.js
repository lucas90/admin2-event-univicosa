/**
 * @author Guilherme Pereira Nogueira <guilhermenogueira90@gmail.com>
 * @author Olimar Ferraz <olimarferraz@univicosa.com.br>
 */
'use strict';

/**
 * TinyMCE Toolbars declaration
 * @type {{toolbar1: string, toolbar2: string, toolbar3: string, linetoolbar: string}}
 */
var toolbars = {
	toolbar1: 'undo redo | bold italic underline strikethrough | superscript subscript | alignleft aligncenter alignright alignjustify | outdent indent',
	toolbar2: 'bullist numlist | link copy paste pagebreak fullscreen autolink preview',
	toolbar3: 'responsivefilemanager | media | image',
	linetoolbar: 'copy paste | bold italic underline strikethrough | superscript subscript | undo redo'
};

/**
 * TinyMCE default config
 * @type {string}
 */
var language = 'pt_BR',
	theme = 'modern',
	entity_encoding = 'raw',
	cache_suffix = '?v=4.1.6';

var	remove_trailing_brs = true,
	   paste_as_text = true;

/**
 * Initialize tinyMCE instances
 * @constructor
 */
function ConfigTinymce() {
	tinymce.init({
		selector: '#text-id', //post main body
		theme: theme,
		language: language,
		entity_encoding: entity_encoding,
		remove_trailing_brs: remove_trailing_brs,
		paste_as_text: paste_as_text,
		cache_suffix: cache_suffix,
		height: 400,
		menubar: false,
		plugins: [
			'autolink link lists preview pagebreak media image paste wordcount',
			'fullscreen responsivefilemanager'
		],
		toolbar1: toolbars.toolbar1,
		toolbar2: toolbars.toolbar2,
		toolbar3: toolbars.toolbar3,

		image_advtab: true,
		relative_urls: false,
		filemanager_crossdomain: true,
		filemanager_access_key: '901ea9c5-4510-4773-ac72-5691f93cdfbc',
		external_filemanager_path:"https://cdn.univicosa.com.br/img/portal/evento/filemanager/",
		filemanager_title:"Responsive Filemanager",
		external_plugins: { "filemanager" : "https://cdn.univicosa.com.br/img/portal/evento/filemanager/plugin.min.js"},
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			})
		}
	});

	tinymce.init({
		selector: '#text-id-2', //post main body
		theme: theme,
		language: language,
		entity_encoding: entity_encoding,
		remove_trailing_brs: remove_trailing_brs,
		paste_as_text: paste_as_text,
		cache_suffix: cache_suffix,
		height: 400,
		menubar: false,
		plugins: [
			'autolink link lists preview pagebreak media image paste wordcount',
			'fullscreen responsivefilemanager'
		],
		toolbar1: toolbars.toolbar1,
		toolbar2: toolbars.toolbar2,

		image_advtab: true,
		relative_urls: false,
		filemanager_crossdomain: true,
		filemanager_access_key: '901ea9c5-4510-4773-ac72-5691f93cdfbc',
		external_filemanager_path:"https://cdn.univicosa.com.br/img/portal/evento/filemanager/",
		filemanager_title:"Responsive Filemanager",
		external_plugins: { "filemanager" : "https://cdn.univicosa.com.br/img/portal/evento/filemanager/plugin.min.js"},
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			})
		}
	});

	tinymce.init({
		selector: 'textarea.editable.h1', //post title
		theme: theme,
		language: language,
		entity_encoding: entity_encoding,
		remove_trailing_brs: remove_trailing_brs,
		paste_as_text: paste_as_text,
		cache_suffix: cache_suffix,
		forced_root_block : 'h1',
		toolbar: toolbars.linetoolbar,
		plugins: [
			'paste wordcount'
		],
		menubar: false,
		setup: function (editor) {
			editor.on('change', function() {
				tinymce.triggerSave();
			})
			.on('keydown',function(e) {
				if(e.keyCode == 13){
					e.preventDefault();
					e.stopPropagation();
					return false;
				}
			})
		}
	});

	tinymce.init({
		selector: 'textarea.editable.h3', //post description
		theme: theme,
		language: language,
		entity_encoding: entity_encoding,
		remove_trailing_brs: remove_trailing_brs,
		paste_as_text: paste_as_text,
		cache_suffix: cache_suffix,
		forced_root_block : 'h3',
		toolbar: toolbars.linetoolbar,
		menubar: false,
		plugins: [
			'paste wordcount'
		],
		setup: function (editor) {
			editor.on('change', function() {
				tinymce.triggerSave();
			})
			.on('keydown',function(e) {
				if(e.keyCode == 13){
					e.preventDefault();
					e.stopPropagation();
					return false;
				}
			})
		}
	});
}