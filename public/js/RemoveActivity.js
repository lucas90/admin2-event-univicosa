/**
 * Created by Lucas on 25/07/2016.
 */

function clickDisplayModalConfirm(activityId) {
    swal({
        title:'Hey!',
        text:"Deseja realmente remover essa atividade?",
        type:'warning',
        showCancelButton:true,
        confirmButtonColor:'#3085d6',
        cancelButtonColor:'#d33',
        confirmButtonText:'Sim',
        closeOnConfirm:true
    }).then(function(isConfirm) {
        if (isConfirm === true) {
            var payload = removeActivity(activityId);
        }
        if(isConfirm === false){

        }
    })
}

function removeActivity(activityId) {
    $('.btn').prop('disabled', true);
    $.ajax({
        url:'/atividade/remover-atividade',
        method:'post',
        dataType:'json',
        data:{
            activityId:activityId
        }
    }).done((function (payload) {
        if (payload.success) {
            swal({
                title: 'Year!',
                text: payload.message,
                type: 'success',
                timer: 2000
            }).then(function(isConfirm) {
                window.location.href = '/atividades/' + payload.eventId;
            });
        }

        if(!payload.success){
            swal({
                title: 'Opsz!',
                text: 'Não foi possível remover a atividade!',
                type: 'error',
                timer: 2000
            }).then(function(isConfirm) {
                $('.btn').prop('disabled', false);
            });
        }
    }));
}
