/**
 * Created by Lucas on 21/07/2016.
 */

function clickDisplayModalConfirm(eventId) {
    swal({
        title:'Hey!',
        text:"Deseja realmente remover esse evento?",
        type:'warning',
        showCancelButton:true,
        confirmButtonColor:'#3085d6',
        cancelButtonColor:'#d33',
        confirmButtonText:'Sim',
        closeOnConfirm:true
    }).then(function(isConfirm) {
        if (isConfirm === true) {
            var payload = removeEvent(eventId);
        }
        if(isConfirm === false){

        }
    })
}

function removeEvent(eventId) {
    $('.btn').prop('disabled', true);
    $.ajax({
        url:'/evento/remover-evento',
        method:'post',
        dataType:'json',
        data:{
            eventId:eventId
        }
    }).done((function (payload) {
        if (payload.success) {
            swal({
                title: 'Year!',
                text: 'Evento removido com sucesso!',
                type: 'success',
                timer: 2000
            }).then(function(isConfirm) {
                window.location.href = '/eventos';
            });
        }

        if(!payload.success){
            swal({
                title: 'Opsz!',
                text: 'Não foi possível remover o evento!',
                type: 'error',
                timer: 2000
            }).then(function(isConfirm) {
                $('.btn').prop('disabled', false);
            });
        }
    }));
}
