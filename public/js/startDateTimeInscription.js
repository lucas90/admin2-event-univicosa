/**
 * Start datetimepicker
 */

'use strict';

$(function () {
    /**
     * @type {*|jQuery|HTMLElement}
     */
    var datePickerStartInscription = $('#dataInicialInscricao'),
        datePickerEndInscription = $('#dataFinalInscricao'),
        datePickerDisclosure = $('#dataDivulgacao');

    datePickerStartInscription.datetimepicker({
        startDate: null,
        format: 'dd/mm/yyyy hh:ii',
        language: 'br',
        autoclose: true
    });

    datePickerEndInscription.datetimepicker({
        startDate: null,
        format: 'dd/mm/yyyy hh:ii',
        language: 'br',
        autoclose: true
    });

    datePickerDisclosure.datetimepicker({
        startDate:null,
        format: 'dd/mm/yyyy hh:ii',
        language: 'br',
        autoclose: true
    });

});

