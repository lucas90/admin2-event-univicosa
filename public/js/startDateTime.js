/**
 * Start datetimepicker
 */

'use strict';

$(function () {
	/**
	 * @type {*|jQuery|HTMLElement}
	 */
	var datePickerStart = $('#dataInicial'),
		datePickerEnd = $('#dataFinal');

	datePickerStart.datetimepicker({
		startDate: null,
		format: 'dd/mm/yyyy hh:ii',
		language: 'br',
		autoclose: true
	});


	datePickerEnd.datetimepicker({
		startDate: null,
		format: 'dd/mm/yyyy hh:ii',
		language: 'br',
		autoclose: true
	});

});

