/**
 * Created by Lucas on 15/08/2016.
 */

function next(max, current, response, moduleId, title) {
    max = max + 2; //necessário pois o step inicial e o final são contados tambem
    if(current === max){
        if(!response){
            location.reload();
        }

        if(response){
            url = window.location.href;
            url = url.slice(-36);
            var answers = [];

            count = 0;
            for(var i = 2; i < max; i++){
                if(sessionStorage.getItem('"' + i + '"') !== null){
                    answers[count++] = sessionStorage.getItem('"' + i + '"');
                }
            }

            console.log(answers);

            $('#tab' + current).removeClass('active');
            $('#nav-tab-' + current).addClass('done');

            $('.btn').prop('disabled', true);
            $.ajax({
                url:'/modulo/selecao',
                method:'post',
                dataType:'json',
                data:{
                    eventId: url,
                    answers: answers
                }
            }).done((function (payload) {
                if (payload.success) {
                    swal({
                        title: 'Year!',
                        text: 'Módulos adicionados com sucesso!',
                        type: 'success',
                        timer: 2000
                    }).then(function(isConfirm) {
                        window.location.href = '/modulos/' + payload.eventId;
                    });
                }

                if(!payload.success){
                    swal({
                        title: 'Opsz!',
                        text: 'Não foi possível adicionar os módulos!',
                        type: 'error',
                        timer: 2000
                    }).then(function(isConfirm) {
                        $('.btn').prop('disabled', false);
                    });
                }
            }));
        }

    }

    if(current < max){
        $('#tab' + current).removeClass('active');
        $('#nav-tab-' + current).addClass('done');
        $('#nav-tab-' + ++current).addClass('active');
        $('#tab' + current).addClass('active');

        $('#modules').append("<h5><b class='uppercase font-green'>" + title + "</b>: <span id='" + moduleId + "'> </span></h5>");

        if(response){
            $('#' + moduleId).append("<b class='uppercase font-green-jungle'>Sim</b>");
            sessionStorage.setItem('"' + --current + '"',  moduleId);
        }

        if(!response){
            $('#' + moduleId).append("<b class='uppercase font-red'>Não</b>");
        }

    }

}