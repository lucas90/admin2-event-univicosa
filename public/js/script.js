/**
 * @author Olimar Ferraz <olimarferraz@univicosa.com.br>
 */
'use strict';
var d = document;

/**
 * Form submit loading
 */
$("form").submit(function() {
	Pace.restart();
	$(".submit-btn").button("loading");
});

/**
 * Change image preview on event fileupload callback
 */
$('#cover').on('change', function(){
	$('#cover-preview').attr('src', $(this).val());
});

/**
 * Triger the click to upload on image preview
 */
$('.img-clickable').on('click', function() {
	$('.btn.fancybox').trigger('click');
});

/**
 * Initialize Toastr
 */
toastr.options = {
	"closeButton": true,
	"debug": false,
	"positionClass": "toast-top-center",
	"onclick": null,
	"showDuration": "1000",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "1000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
};

/**
 * Default error message using SweetAlert.js
 * @param {string} msg
 */
function defaultErrorMessage(msg)
{
	swal({
		title: 'Oops!',
		text: 'Ocorreu um erro durate a requisição, tente novamente por favor. <br>' + msg,
		type: 'error'
	})
}

/**
 * Show the initial system message according
 * of the hour of the day using Notify.js
 */
function welcomeMessage()
{

	var name = d.querySelector('#user').dataset.value;
	name = name.substr(0, name.indexOf(" "));

	var today = new Date();
	var w = new Date();

	var curHr = today.getHours();
	var curTm = today.getTime();
	var day, className, wTime, t;

	var storage = localStorage.getItem("wTime") ? parseInt(localStorage.getItem("wTime")) : false;

	if (curHr<12) {
		day = "Bom dia";
		className = "morning";

		wTime = w.setHours(12,0,0,0);
	} else if (curHr<18) {
		day = "Boa tarde";
		className = "afternoon";

		w = new Date();
		wTime = w.setHours(18,0,0,0)
	} else {
		day = "Boa noite";
		className = "night";

		w = new Date();
		wTime = w.setHours(23,59,0,0)
	}

	if (storage) {
		if (curTm > storage) {
			toastr['info']("<strong>" + day + " " + name + "</strong>");
		}
	} else {
		toastr['info']("<strong>" + day + " " + name + "</strong>");
	}

	localStorage.setItem("wTime", wTime);
	t = d.querySelector('.toast.toast-info');
	null !== t ? t.classList.add(className) : null;

}

function addRevenueExpenditure(div, idOther){
	var otherInput = $('#' + idOther).val();
	otherInput = otherInput.trim();

	if(otherInput !== "")
	{
		$('#' + div).append(
			'<div class="row">' +
			'<div class="col-md-6">' +
			'<div class="form-group form-md-line-input has-info">' +
			'<input id="form_control_1" class="form-control" type="text" value="' + otherInput + '" readonly name="RevenueExpenditureOthersName[]">' +
			'<label for="form_control_1">Outro</label>' +
			'</div>' +
			'</div>' +
			'<input hidden name="RevenueExpenditureOthersId[]" checked type="checkbox" class="md-check" value="' + idOther + '" data-toogle="checkbox">' +

			'<div class="col-md-6">' +
			'<div class="form-group form-md-line-input has-info">' +
			'<div class="input-group left-addon right-addon">' +
			'<span class="input-group-addon">$</span>' +
			'<input class="form-control mask-money-full" type="text" name="RevenueExpenditureOthersValue[]" >' +
			'<label for="form_control_1">Valor</label>' +
			'</div>' +
			'</div>' +
			'</div>'+
			'</div>'
		);
	}

	$('#' + idOther).val('');

	//iniciando um script de forma assíncrona
	var element = document.createElement("script");
	$('body script[src="https://cdn.univicosa.com.br/plugins/jquery.mask/1.6.5/jquery.mask.js"]').remove();
	element.src = "https://cdn.univicosa.com.br/plugins/jquery.mask/1.6.5/jquery.mask.js";
	document.body.appendChild(element);
}

function addSponsor()
{
	var sponsor = $('#sponsor').val();

	if(sponsor !== ""){
		$('#sponsors').append(
			'<div class="row">' +
			'<div class="col-md-6">' +
			'<div class="form-group form-md-line-input has-info">' +
			'<input id="form_control_1" class="form-control" type="text" value="' + sponsor + '" placeholder="' + sponsor + '" readonly name="sponsor[]">' +
			'<label for="form_control_1">Patrocinador</label>' +
			'</div>' +
			'</div>' +


			'<div class="col-md-6">' +
			'<div class="form-group form-md-line-input has-info form-md-floating-label">' +
			'<div class="input-group left-addon right-addon">' +
			'<span class="input-group-addon">$</span>' +
			'<input class="form-control" type="text" name="sponsorCust[]" required>' +
			'<label for="form_control_1">Valor recebido</label>' +
			'<span class="help-block">Exemplo.: 35.00</span>' +
			'</div>' +
			'</div>' +
			'</div>'+
			'</div>'

		);

		$('#sponsor').val('');
	}

}

function addOthers(idOther, div)
{
	var otherInput = $('#otherInput' + idOther).val();
	otherInput = otherInput.trim();

	var uuid = uuidGenerate();

	if(otherInput !== "")
	{
		$('#' + div).append(
			'<div class="md-checkbox-inline">' +
			'<div class="md-checkbox">' +
			'<input checked type="checkbox" id="' + uuid + '" class="md-check" name="othersValue[]" value="' + otherInput + '" data-toogle="checkbox">' +
			'<label for="' + uuid + '">' +
			'<input hidden name="othersId[]" checked type="checkbox" class="md-check" value="' + idOther + '" data-toogle="checkbox">' +
			'<span class="inc"></span>' +
			'<span class="check"></span>' +
			'<span class="box"></span>' +
			otherInput +
			'</label>' +
			'</div>' +
			'</div>'
		);
	}

	$('#otherInput' + idOther).val('');
}

function uuidGenerate() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		s4() + '-' + s4() + s4() + s4();
}

function OnMessage(e){
	var event = e.originalEvent;
	// Make sure the sender of the event is trusted
	if(event.data.sender === 'responsivefilemanager'){
		if(event.data.field_id){
			var fieldID=event.data.field_id;
			var url=event.data.url;
			console.log(url);
			$('#'+fieldID).val(url).trigger('change');
			$.fancybox.close();

			//Delete handler of the message from ResponsiveFilemanager
			$(window).off('message', OnMessage);
		}
	}
}

function activateEvent(checkbox, id) {
	Pace.restart();

	var active = (checkbox.checked) ? true : false;

	Pace.track(function(){
		$.ajax({
			method: 'post',
			url: '/evento/ativar/' + id,
			data: {active : active},
			dataType: 'json'
		})
			.done(function(msg) {
				if(msg.success) {
					var message = (active === true) ? 'Evento ativado' : 'Evento desativada';
					toastr['success'](message);
				} else {
					defaultErrorMessage(msg);
				}
			})
			.fail(function() {
				defaultErrorMessage('')
			});
	})
}

$(function() {
	//welcomeMessage();
});


function buttonNextHelp (atual, containerAtual) {
	var texts = $('.tut-text');
	var btNext = $('#btnext');
	if(texts.length === atual){
		var container = $('.tut-container');
		container.addClass('fade-out');
		window.setInterval(function(){
			container.remove();
			$('#help-listener-' + ++containerAtual).trigger('click');
		}, 1200);

	}

	$('#text-' + containerAtual + '-' + atual).removeClass('text-transition');
	$('#text-' + containerAtual + '-' + ++atual).addClass('text-transition');


	btNext.attr('onclick', 'buttonNextHelp(' + atual + ', ' + containerAtual +')')
}
function closeTutorial() {
	var container = $('.tut-container');
	container.addClass('fade-out');
	window.setInterval(function(){
		container.remove();
	}, 1000);
}