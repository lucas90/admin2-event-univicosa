<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

$configPath = '{,*.}{global}.php';
//if (getenv('APPLICATION_ENV') === 'development') {
    $configPath = '{,*.}{local}.php';
//}

return [
    'modules' => [
        'DoctrineModule',
        'DoctrineORMModule',
        'AssetManager',
        'Application',
		'Core',
        'OAuth',
        'Event',
        'Date',
        'Activity',
        'Modules',
        'Demand',
        'Costs',
        'Wizard',
        'Participant'
    ],
    'module_listener_options' => [
        'module_paths' => [
            './module',
            './vendor',
        ],
        'config_glob_paths' => [
            'config/autoload/' .$configPath,
	        'config/autoload/doctrine.config.php',
			'config/paginator/paginator.config.php'
        ]
    ]
];
