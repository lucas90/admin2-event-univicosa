<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use Doctrine\DBAL\Driver\PDOPgSql\Driver;

return [
    'doctrine' => [
        'connection' => [
            'orm_pgsql' => [
                'driverClass' => Driver::class,
                'params' => [
                    'host' => getenv('PGSQL_DEV_HOST'),
                    'user' => 'postgres',
                    'password' => 'postgreunivicosa',
                    'port' => getenv('PGSQL_DEV_PORT'),
                    'dbname' => 'evento'
                ]
            ],
            'orm_auth' => [
                'driverClass' => Driver::class,
                'params' => [
                    'host' => getenv('PGSQL_PROD_HOST'),
                    'user' => 'postgres',
                    'password' => 'postgreunivicosa',
                    'port' => getenv('PGSQL_PROD_PORT'),
                    'dbname' => 'auth'
                ]
            ]
        ]
    ]
];
