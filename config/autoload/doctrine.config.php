<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use DoctrineORMModule\Service\ConfigurationFactory;
use DoctrineORMModule\Service\DBALConnectionFactory;
use DoctrineORMModule\Service\EntityManagerFactory;

$configuration = [
	'metadata_cache' => 'array',
	'query_cache' => 'array',
	'result_cache' => 'array',
	'generate_proxies' => true,
	'proxy_dir' => 'data/DoctrineORMModule/Proxy',
	'proxy_namespace' => 'DoctrineORMModule\Proxy',
	'filters' => []
];

return [
	'service_manager' => [
		'factories' => [
			'doctrine.entitymanager.em_pgsql' => new EntityManagerFactory('em_pgsql'),
			'doctrine.connection.em_pgsql' => new DBALConnectionFactory('em_pgsql'),
			'doctrine.configuration.em_pgsql' => new ConfigurationFactory('em_pgsql'),

			'doctrine.entitymanager.em_auth' => new EntityManagerFactory('em_auth'),
			'doctrine.connection.em_auth' => new DBALConnectionFactory('em_auth'),
			'doctrine.configuration.em_auth' => new ConfigurationFactory('em_auth')
		]
	],
	'doctrine' => [
		'entitymanager' => [
			'em_pgsql' => [
				'connection' => 'orm_pgsql'
			],
			'em_auth' => [
				'connection' => 'orm_auth'
			]
		],
		'configuration' => [
			'config_pgsql' => [
				$configuration,
				'driver' => 'orm_pgsql'
			],
			'config_auth' => [
				$configuration,
				'driver' => 'orm_auth'
			]
		]
	]
];
