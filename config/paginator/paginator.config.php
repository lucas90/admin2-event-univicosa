<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

return [
    'paginator' => [
        'item_count_per_page' => 10
    ]
];
