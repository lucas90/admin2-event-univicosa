<?php

namespace Application;

use OAuth\Listener\AuthenticationListener;
use OAuth\Service\GetUserService;
use OAuth\Service\ListPermissionsByUserService;
use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\ModuleRouteListener;

/**
 * @author  Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class Module implements ConfigProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function onBootstrap(EventInterface $event)
    {
        $application = $event->getApplication();
        $serviceLocator = $application->getServiceManager();
        $eventManager = $application->getEventManager();

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        /*if (getenv('APPLICATION_ENV') === 'production') {
            $getUserService = $serviceLocator->get(GetUserService::class);
            $permissionsByUserService = $serviceLocator->get(ListPermissionsByUserService::class);

            $eventManager->attachAggregate(
                new AuthenticationListener($getUserService, $permissionsByUserService)
            );
         }*/
    }

    /**
     * {@inheritDoc}
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}