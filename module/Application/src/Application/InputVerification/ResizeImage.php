<?php

namespace Application\InputVerification;

use Imagine\Image\ImageInterface;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class ResizeImage
{
    /**
     * @var \[]
     */
    private $options = [
        'resolution-x' => 300,
        'resolution-y' => 300,
        'jpeg_quality' => 90
    ];

    /**
     * @const int
     */
    const WIDTH = 1920;

    /**
     * @const int
     */
    const HEIGHT = 500;

    /**
     * @param string $filename
     * @param string $newName
     * @param int $width
     * @param int $height
     *
     * @throws \Exception
     */
    public function __construct(
        $filename,
        $newName,
        $width = self::WIDTH,
        $height = self::HEIGHT
    ) {
        $imagine = new Imagine();
        $image = $imagine->open($filename);
        $size = $image->getSize();

        if ($size->getWidth() < $width || $size->getHeight() < $height) {
            throw new \Exception(sprintf('A largura e altura da imagem não podem ser inferior a %dx%d (largura x altura)', $width, $height));
        }

        $ext = pathinfo($filename, \PATHINFO_EXTENSION);

        $this->unlinkFile($newName . '.' . $ext);
        $this->resize($image, $newName . '.' . $ext, $width, $height);
        $this->unlinkFile($filename);
    }

    /**
     * @param $filename
     */
    private function unlinkFile($filename)
    {
        if (file_exists($filename)) {
            unlink($filename);
        }
    }

    /**
     * @param ImageInterface $image
     * @param string         $newName
     * @param string         $width
     * @param string         $height
     */
    private function resize(
        ImageInterface $image,
        $newName,
        $width,
        $height
    ) {
        $image->resize(new Box($width, $height));
        $image->save($newName, $this->options);
    }
}