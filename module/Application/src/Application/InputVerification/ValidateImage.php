<?php

namespace Application\InputVerification;

use Application\InputVerification\ResizeImage;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class ValidateImage
{
    /**
     * @param $filename
     *
     * @throws \Exception
     */
    public function __construct($filename)
    {
        list($width, $height) = getimagesize($filename);

        if ($width !== ResizeImage::WIDTH || $height !== ResizeImage::HEIGHT) {
            throw new \Exception(sprintf('A largura e altura da imagem não pode ser diferente de %d x %d (largura x altura)', ResizeImage::WIDTH, ResizeImage::HEIGHT));
        }
    }
}