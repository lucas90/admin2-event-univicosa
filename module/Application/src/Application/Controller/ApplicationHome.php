<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class ApplicationHome extends AbstractActionController
{
    /**
     * @return ViewModel
     */
    public function onDispatchAction()
    {
        return $this->redirect()->toRoute('eventos');
    }
}
