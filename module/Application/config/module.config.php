<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use Application\Controller\ApplicationHome;
use Application\Controller\Factory\ApplicationHomeFactory;
use Zend\Navigation\Service\DefaultNavigationFactory;
use Zend\Mvc\Router\Http\Literal;

return [
    'controllers' => [
        'factories' => [
            ApplicationHome::class => ApplicationHomeFactory::class
        ]
    ],
    'service_manager' => [
        'factories' => [
            'navigation' => DefaultNavigationFactory::class
        ],
        'aliases' => [
            'translator' => 'MvcTranslator'
        ]
    ],
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => ApplicationHome::class,
                        'action' => 'onDispatch',
                    ],
                ]
            ]
        ],
    ],
    'translator' => [
        'locale' => 'pt_BR',
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ]
        ]
    ],
    'view_manager' => [
        'base_path'                => '/',
        'display_not_found_reason' => getenv('APPLICATION_ENV') === 'development' ? true : false,
        'display_exceptions'       => getenv('APPLICATION_ENV') === 'development' ? true : false,
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml'
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ]
    ],
    'asset_manager' => [
        'resolver_configs' => [
            'paths' => [
                'Application' => __DIR__ . '/../public',
            ]
        ]
    ]
];
