<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'doctrine' => [
        'driver' => [
            'demand_entities' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Demand/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Demand\Entity' => 'demand_entities'
                ]
            ]
        ]
    ]
];