<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use Demand\Service\Factory\MergeOrPersistServiceFactory;
use Demand\Service\MergeOrPersistService;

use Demand\Controller\Factory\MergeOrPersistFactory;
use Demand\Controller\MergeOrPersist;

use Demand\Service\ListAllOrByIdService;
use Demand\Service\Factory\ListAllOrByIdServiceFactory;

use Demand\Controller\ListAllOrById;
use Demand\Controller\Factory\ListAllOrByIdFactory;

use Demand\Service\RemoveService;
use Demand\Service\Factory\RemoveServiceFactory;

use Zend\Mvc\Router\Http\Literal;
use Zend\Mvc\Router\Http\Segment;

return [
    'controllers' => [
        'factories' => [
            ListAllOrById::class => ListAllOrByIdFactory::class,
            MergeOrPersist::class => MergeOrPersistFactory::class
        ]
    ],
    'service_manager' => [
        'factories' => [
            ListAllOrByIdService::class => ListAllOrByIdServiceFactory::class,
            MergeOrPersistService::class => MergeOrPersistServiceFactory::class,
            RemoveService::class => RemoveServiceFactory::class
        ],
    ],
    'router' => [
        'routes' => [
            'demandas' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/demandas[/:idevent]',
                    'constraints' => [
                        'idevent' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}'
                    ],
                    'defaults' => [
                        'controller' => ListAllOrById::class,
                        'action' => 'selectDemand'
                    ],
                ]
            ],

            'demanda' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/demanda',
                    'defaults' => [
                        'controller' => MergeOrPersist::class
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'adicionar-editar' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/adicionar-editar[/:idevent][/:idsector]',
                            'constraints' => [
                                'idevent' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}',
                                'idsector' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}'
                            ],
                            'defaults' => [
                                'action' => 'mergeOrPersist'
                            ]
                        ]
                    ],
                ]
            ]
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy'
        ]
    ]
];
