<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 26/07/2016
 * Time: 13:44
 */

namespace Demand\Controller\Factory;

use Demand\Controller\ListAllOrById;
use Demand\Service\ListAllOrByIdService;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ListAllOrByIdFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $realServiceLocator  = $serviceLocator->getServiceLocator();
        $listAllOrByIdService = $realServiceLocator->get(ListAllOrByIdService::class);

        return new ListAllOrById($listAllOrByIdService);
    }
}