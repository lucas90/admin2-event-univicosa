<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 26/07/2016
 * Time: 11:39
 */

namespace Demand\Controller\Factory;

use Demand\Service\ListAllOrByIdService;
use Demand\Service\MergeOrPersistService;
use Demand\Controller\MergeOrPersist;
use Event\Service\Event\ListAllOrByIdService as ListAllOrByIdServiceEvent;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MergeOrPersistFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $realServiceLocator = $serviceLocator->getServiceLocator();
        $mergeOrPersistServiceEvent = $realServiceLocator->get(MergeOrPersistService::class);
        $service = $realServiceLocator->get(ListAllOrByIdService::class);
        $serviceEvent = $realServiceLocator->get(ListAllOrByIdServiceEvent::class);

        return new MergeOrPersist(
            $mergeOrPersistServiceEvent,
            $service,
            $serviceEvent
        );
    }
}