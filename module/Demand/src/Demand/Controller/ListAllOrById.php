<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 26/07/2016
 * Time: 13:45
 */

namespace Demand\Controller;

use Demand\Service\ListAllOrByIdService;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ListAllOrById extends AbstractActionController
{
    private $service;

    public function __construct(ListAllOrByIdService $listAllOrByIdService)
    {
        $this->service = $listAllOrByIdService;
    }

    public function selectDemandAction()
    {
        $id = (string) $this->params()->fromRoute('idevent');
        $ascom = $this->service->getSectorByName('ASCOM');
        $almoxarifado = $this->service->getSectorByName('Almoxarifado');

        $viewModel = new ViewModel();
        $viewModel->setTemplate('demand/list-all/index');
        $viewModel->setVariables([
            'eventId' => $id,
            'ascomId' => $ascom->getSectorId(),
            'almoxarifadoId' => $almoxarifado->getSectorId()
        ]);

        return $viewModel;
    }
}