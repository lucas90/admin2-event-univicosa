<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 26/07/2016
 * Time: 11:39
 */

namespace Demand\Controller;

use Demand\Service\MergeOrPersistService as MergeOrPersistDemand;
use Demand\Service\ListAllOrByIdService;
use Event\Service\Event\ListAllOrByIdService as ListAllOrByIdServiceEvent;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class MergeOrPersist extends AbstractActionController
{
    private $mergeOrPersistDemand;
    private $service;
    private $serviceEvent;

    public function __construct(
        MergeOrPersistDemand $mergeOrPersistDemand,
        ListAllOrByIdService $service,
        ListAllOrByIdServiceEvent $serviceEvent
    ){
        $this->mergeOrPersistDemand = $mergeOrPersistDemand;
        $this->service = $service;
        $this->serviceEvent = $serviceEvent;
    }

    public function mergeOrPersistAction()
    {
        $idSector = (string) $this->params()->fromRoute('idsector');
        $idEvent = (string) $this->params()->fromRoute('idevent');

        $event = $this->serviceEvent->getById($idEvent);
        $demand = $this->service->getDemandByEvent($event);
        $checklistItems = $this->service->getChecklistByDemand($demand);


        $sector = $this->service->getSectorById($idSector);
        $groups = $this->service->findGroupsBySector($sector);
        $items = $this->service->findAllItem();

        $request = $this->getRequest();

        if($request->isXmlHttpRequest()){
            $jsonModel = new JsonModel();
            $data = $request->getPost()->toArray();
            $data['eventId'] = $idEvent;
            $data['sectorId'] = $idSector;

            try{
                $mergeOrPersistDemand = $this->mergeOrPersistDemand;
                $mergeOrPersistDemand($data);

                $jsonModel->setVariables([
                    'success' => true,
                    'message' => 'Demandas aceitas' ,
                    'eventId' => $idEvent
                ]);
            }catch (\Exception $e){
                $jsonModel->setVariables([
                    'success' => false,
                    'message' => $e->getMessage()
                ]);
            }
            
            return $jsonModel;
        }

        $viewModel = new ViewModel();
        $viewModel->setTemplate('demand/merge-or-persist/index');
        $viewModel->setVariables([
            'groups' => $groups,
            'items' => $items,
            'eventId' => $idEvent,
            'checklistItems' => $checklistItems,
            'event' => $event
        ]);

        return $viewModel;
    }
}