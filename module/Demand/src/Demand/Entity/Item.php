<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 26/07/2016
 * Time: 14:52
 */

namespace Demand\Entity;

use Demand\Entity\Group;

use Rhumsaa\Uuid\Uuid;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Date
 * @package Date\Entity
 * @ORM\Entity
 * @ORM\Table(name="demanda.item")
 */
class Item
{
    /**
     * @ORM\Id
     * @ORM\Column(name="item_id", type="string")
     */
    private $itemId;

    /**
     * @ORM\ManyToOne(targetEntity="Demand\Entity\Group")
     * @ORM\JoinColumn(name="grupo_id", referencedColumnName="grupo_id")
     */
    private $group;

    /**
     * @ORM\Column(name="item", type="string")
     */
    private $item;

    /**
     * @ORM\Column(name="ordem", type="integer")
     */
    private $order;

    /**
     * @return mixed
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }


}