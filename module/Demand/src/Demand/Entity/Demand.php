<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 04/07/2016
 * Time: 17:19
 */

namespace Demand\Entity;

use Doctrine\ORM\Mapping as ORM;
use Event\Entity\Event;
use Rhumsaa\Uuid\Uuid;

/**
 * Class Demand
 * @package Demand\Entity
 * @ORM\Entity
 * @ORM\Table(name="demanda.demanda")
 */
class Demand
{
    /**
     * @ORM\Id
     * @ORM\Column(name="demanda_id", type="string")
     */
    private $demandId;

    /**
     * @ORM\OneToOne(targetEntity="Event\Entity\Event")
     * @ORM\JoinColumn(name="evento_id", referencedColumnName="evento_id")
     */
    private $event;

    /**
     * Demand constructor.
     * @param $event
     */
    public function __construct(Event $event)
    {
        $this->demandId = Uuid::uuid4();
        $this->event = $event;
    }


    /**
     * @return mixed
     */
    public function getDemandId()
    {
        return $this->demandId;
    }

    /**
     * @return mixed
     */
    public function getEventId()
    {
        return $this->event;
    }


}