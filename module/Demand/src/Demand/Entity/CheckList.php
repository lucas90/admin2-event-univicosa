<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 26/07/2016
 * Time: 14:49
 */

namespace Demand\Entity;

use Doctrine\ORM\Mapping as ORM;
use Rhumsaa\Uuid\Uuid;

/**
 * Class Date
 * @package Date\Entity
 * @ORM\Entity
 * @ORM\Table(name="demanda.checklist")
 */
class CheckList
{
    /**
     * @ORM\Id
     * @ORM\Column(name="checklist_id", type="string")
     */
    private $checkListId;

    /**
     * @ORM\ManyToOne(targetEntity="Demand\Entity\Demand")
     * @ORM\JoinColumn(name="demanda_id", referencedColumnName="demanda_id")
     */
    private $demand;

    /**
     * @ORM\ManyToOne(targetEntity="Demand\Entity\Item")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="item_id")
     */
    private $item;

    /**
     * @ORM\Column(name="descricao", type="string")
     */
    private $description;

    /**
     * CheckList constructor.
     * @param $demand
     * @param $item
     * @param $description
     */
    public function __construct(Demand $demand, Item $item, $description)
    {
        $this->checkListId = Uuid::uuid4();
        $this->demand = $demand;
        $this->item = $item;
        $this->description = $description;
    }


    /**
     * @return mixed
     */
    public function getCheckListId()
    {
        return $this->checkListId;
    }

    /**
     * @return mixed
     */
    public function getDemand()
    {
        return $this->demand;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }


}