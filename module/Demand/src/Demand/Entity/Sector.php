<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 26/07/2016
 * Time: 17:44
 */

namespace Demand\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Date
 * @package Date\Entity
 * @ORM\Entity
 * @ORM\Table(name="demanda.setor")
 */
class Sector
{
    /**
     * @ORM\Id
     * @ORM\Column(name="setor_id", type="string")
     */
    private $sectorId;

    /**
     * @ORM\Column(name="setor", type="string")
     */
    private $sector;

    /**
     * @return mixed
     */
    public function getSectorId()
    {
        return $this->sectorId;
    }

    /**
     * @return mixed
     */
    public function getSector()
    {
        return $this->sector;
    }


}