<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 26/07/2016
 * Time: 17:41
 */

namespace Demand\Entity;

use Rhumsaa\Uuid\Uuid;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Date
 * @package Date\Entity
 * @ORM\Entity
 * @ORM\Table(name="demanda.grupo")
 */
class Group
{
    /**
     * @ORM\Id
     * @ORM\Column(name="grupo_id", type="string")
     */
    private $groupId;

    /**
     * @ORM\ManyToOne(targetEntity="Demand\Entity\Sector")
     * @ORM\JoinColumn(name="setor_id", referencedColumnName="setor_id")
     */
    private $sector;

    /**
     * @ORM\Column(name="grupo", type="string")
     */
    private $group;

    /**
     * @ORM\Column(name="ordem", type="integer")
     */
    private $order;

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @return mixed
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }



}