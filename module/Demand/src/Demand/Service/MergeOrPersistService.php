<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 26/07/2016
 * Time: 11:42
 */

namespace Demand\Service;

use Demand\Entity\Demand;
use Demand\Entity\CheckList;
use Demand\Repository\Item\GetItemByIdRepository;
use Demand\Repository\Demand\GetDemandByEventRepository;
use Demand\Repository\CheckList\FindAllChecklistByDemandRepository;
use Event\Repository\Event\GetByIdRepository as GetEventByIdRepository;

use Core\Doctrine\ObjectManagerAbstract;
use Doctrine\Common\Persistence\ObjectManager;

class MergeOrPersistService extends ObjectManagerAbstract
{
    private $getDemandByEvent;
    private $getItemById;
    private $getEventById;
    private $findAllChecklistByDemand;

    public function __construct(
        ObjectManager $objectManager,
        GetItemByIdRepository $getItemByIdRepository,
        GetEventByIdRepository $getEventByIdRepository,
        GetDemandByEventRepository $getDemandByEventRepository,
        FindAllChecklistByDemandRepository $findAllChecklistByDemandRepository
    ){
        parent::__construct($objectManager);
        $this->getItemById = $getItemByIdRepository;
        $this->getEventById = $getEventByIdRepository;
        $this->getDemandByEvent = $getDemandByEventRepository;
        $this->findAllChecklistByDemand = $findAllChecklistByDemandRepository;
    }

    public function __invoke(array $data)
    {
        $getEventById = $this->getEventById;
        $getDemandByEvent = $this->getDemandByEvent;
        $findAllChecklistByDemand = $this->findAllChecklistByDemand;

        $event = $getEventById($data['eventId']);
        $demand = $getDemandByEvent($event);
        $checklistItems = $findAllChecklistByDemand($demand);

        try{
            if($demand !== null) {
                if(count($checklistItems) > 0){
                    foreach($checklistItems as $checklistItem) {
                        if($checklistItem->getItem()->getGroup()->getSector()->getSectorId() === $data['sectorId']) {
                            $this->objectManager->remove($checklistItem);
                        }
                    }
                    $this->objectManager->flush();
                }
                $this->insert($data, $demand);
            }

            if($demand === null) {
                $demand = new Demand($event);

                $this->objectManager->persist($demand);
                $this->objectManager->flush();

                $this->insert($data, $demand);
            }

            return $demand;
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function insert($data, Demand $demand)
    {
        $getItemById = $this->getItemById;

        if(isset($data['items']) && count($data['items']) > 0){

            foreach($data['items'] as $value){

                $item = $getItemById($value);

                $checklist = new CheckList($demand, $item, '');

                $this->objectManager->persist($checklist);

            }
        }

        if(isset($data['othersValue']) && count($data['othersValue']) > 0){
            $count = 0;

            foreach($data['othersValue'] as $value){
                $item = $getItemById($data['othersId'][$count++]);
                $checklist = new CheckList($demand, $item, $value);

                $this->objectManager->persist($checklist);
            }
        }

        $this->objectManager->flush();
    }
}