<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 26/07/2016
 * Time: 11:41
 */

namespace Demand\Service\Factory;

use Demand\Service\MergeOrPersistService;
use Demand\Repository\Item\GetItemByIdRepository;
use Demand\Repository\Demand\GetDemandByEventRepository;
use Demand\Repository\CheckList\FindAllChecklistByDemandRepository;
use Event\Repository\Event\GetByIdRepository as GetEventByIdRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MergeOrPersistServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new MergeOrPersistService(
            $entityManager,
            new GetItemByIdRepository($entityManager),
            new GetEventByIdRepository($entityManager),
            new GetDemandByEventRepository($entityManager),
            new FindAllChecklistByDemandRepository($entityManager)
        );
    }
}