<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 24/08/2016
 * Time: 17:00
 */

namespace Demand\Service\Factory;

use Demand\Service\RemoveService;

use Demand\Repository\Demand\GetDemandByEventRepository;
use Demand\Repository\CheckList\FindAllChecklistByDemandRepository;
use Event\Repository\Event\GetByIdRepository as GetEventByIdRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RemoveServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new RemoveService(
            $entityManager,
            new GetDemandByEventRepository($entityManager),
            new FindAllChecklistByDemandRepository($entityManager),
            new GetEventByIdRepository($entityManager)
        );
    }
}