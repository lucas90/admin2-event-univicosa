<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 26/07/2016
 * Time: 13:43
 */

namespace Demand\Service\Factory;

use Demand\Service\ListAllOrByIdService;
use Demand\Repository\CheckList\GetChecklistByDemandRepository;
use Demand\Repository\Demand\GetDemandByEventRepository;
use Demand\Repository\Sector\GetSectorByNameRepository;
use Demand\Repository\Group\FindGroupsBySectorRepository;
use Demand\Repository\Sector\GetSectorByIdRepository;
use Demand\Repository\Item\FindAllItemRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ListAllOrByIdServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new ListAllOrByIdService(
            new GetChecklistByDemandRepository($entityManager),
            new GetDemandByEventRepository($entityManager),
            new GetSectorByNameRepository($entityManager),
            new FindGroupsBySectorRepository($entityManager),
            new GetSectorByIdRepository($entityManager),
            new FindAllItemRepository($entityManager)
        );
    }
}