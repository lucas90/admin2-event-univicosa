<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 26/07/2016
 * Time: 13:43
 */

namespace Demand\Service;

use Demand\Repository\CheckList\GetChecklistByDemandRepository;
use Demand\Repository\Demand\GetDemandByEventRepository;
use Demand\Repository\Sector\GetSectorByNameRepository;
use Demand\Repository\Group\FindGroupsBySectorRepository;
use Demand\Repository\Sector\GetSectorByIdRepository;
use Demand\Repository\Item\FindAllItemRepository;

class ListAllOrByIdService
{
    private $getDemandByEvent;
    private $getChecklistByDemand;
    private $getSectorByName;
    private $findGroupsBySector;
    private $getSectorById;
    private $findAllItem;

    public function __construct(
        GetChecklistByDemandRepository $getChecklistByDemandRepository,
        GetDemandByEventRepository $getDemandByEventRepository,
        GetSectorByNameRepository $getSectorByNameRepository,
        FindGroupsBySectorRepository $findGroupsBySectorRepository,
        GetSectorByIdRepository $getSectorByIdRepository,
        FindAllItemRepository $findAllItemRepository
    ){
        $this->getDemandByEvent = $getDemandByEventRepository;
        $this->getChecklistByDemand = $getChecklistByDemandRepository;
        $this->getSectorByName = $getSectorByNameRepository;
        $this->findGroupsBySector = $findGroupsBySectorRepository;
        $this->getSectorById = $getSectorByIdRepository;
        $this->findAllItem = $findAllItemRepository;
    }

    public function getDemandByEvent($id)
    {
        $repository = $this->getDemandByEvent;

        return $repository($id);
    }

    public function getChecklistByDemand($id)
    {
        $repository = $this->getChecklistByDemand;

        return $repository($id);
    }

    public function getSectorByName($name)
    {
        $repository = $this->getSectorByName;

        return $repository($name);
    }

    public function findGroupsBySector($sector)
    {
        $repository = $this->findGroupsBySector;

        return $repository($sector);
    }

    public function getSectorById($id)
    {
        $repository = $this->getSectorById;

        return $repository($id);
    }

    public function findAllItem()
    {
        $repository = $this->findAllItem;

        return $repository;
    }
}