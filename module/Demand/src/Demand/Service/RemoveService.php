<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 24/08/2016
 * Time: 17:00
 */

namespace Demand\Service;

use Demand\Repository\Demand\GetDemandByEventRepository;
use Demand\Repository\CheckList\FindAllChecklistByDemandRepository;
use Event\Repository\Event\GetByIdRepository as GetEventByIdRepository;

use Doctrine\Common\Persistence\ObjectManager;
use Core\Doctrine\ObjectManagerAbstract;

class RemoveService extends ObjectManagerAbstract
{
    private $getDemandByEvent;
    private $findAllCheckListByDemand;
    private $getEventById;

    public function __construct(
        ObjectManager $objectManager,
        GetDemandByEventRepository $getDemandByEventRepository,
        FindAllChecklistByDemandRepository $findAllChecklistByDemandRepository,
        GetEventByIdRepository $getEventByIdRepository
    ){
        parent::__construct($objectManager);
        $this->getDemandByEvent = $getDemandByEventRepository;
        $this->findAllCheckListByDemand = $findAllChecklistByDemandRepository;
        $this->getEventById = $getEventByIdRepository;
    }

    public function removeAllByEvent($eventId)
    {
        try {
            $getEvent = $this->getEventById;
            $event = $getEvent($eventId);

            $getDemand = $this->getDemandByEvent;
            $demand = $getDemand($event);

            if($demand !== null){
                $findCheckList = $this->findAllCheckListByDemand;
                $list = $findCheckList($demand);

                foreach ($list as $item) {
                    $this->objectManager->remove($item);
                    $this->objectManager->flush();
                }

                $this->objectManager->remove($demand);
                $this->objectManager->flush();
            }


        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}