<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 28/07/2016
 * Time: 14:39
 */

namespace Demand\Repository\Item;

use Demand\Entity\Item;

use Core\Doctrine\ObjectManagerAbstract;

class FindAllItemRepository extends ObjectManagerAbstract
{
    public function __invoke()
    {
        return $this->objectManager->getRepository(Item::class)->findBy(
            [],
            ['order' => 'ASC']
            );
    }
}