<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 27/07/2016
 * Time: 17:08
 */

namespace Demand\Repository\Item;

use Core\Doctrine\ObjectManagerAbstract;
use Demand\Entity\Item;


class FindItemByGroupRepository extends ObjectManagerAbstract
{
    public function __invoke($group)
    {
        return $this->objectManager->getRepository(Item::class)->findBy([
            'group' => $group
        ]);
    }
}