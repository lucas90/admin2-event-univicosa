<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 28/07/2016
 * Time: 17:44
 */

namespace Demand\Repository\Item;

use Demand\Entity\Item;

use Core\Doctrine\ObjectManagerAbstract;

class GetItemByIdRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(Item::class)->findOneBy(['itemId' => $id]);
    }
}