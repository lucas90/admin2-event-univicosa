<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 27/07/2016
 * Time: 08:52
 */

namespace Demand\Repository\Demand;

use Demand\Entity\Demand;

use Core\Doctrine\ObjectManagerAbstract;

class GetDemandByEventRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(Demand::class)->findOneBy(
            ['event' => $id]
        );
    }
}