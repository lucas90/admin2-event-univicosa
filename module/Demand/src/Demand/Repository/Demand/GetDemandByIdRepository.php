<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 28/07/2016
 * Time: 17:43
 */

namespace Demand\Repository\Demand;

use Demand\Entity\Demand;
use Core\Doctrine\ObjectManagerAbstract;

class GetDemandByIdRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(Demand::class)->findOneBy(['demandId' => $id]);
    }
}