<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 27/07/2016
 * Time: 08:58
 */

namespace Demand\Repository\CheckList;

use Demand\Entity\CheckList;

use Core\Doctrine\ObjectManagerAbstract;

class GetChecklistByDemandRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(CheckList::class)->findBy([
            'demand' => $id
        ]);
    }
}