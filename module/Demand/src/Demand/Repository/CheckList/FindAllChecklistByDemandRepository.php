<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 29/07/2016
 * Time: 16:38
 */

namespace Demand\Repository\CheckList;

use Core\Doctrine\ObjectManagerAbstract;
use Demand\Entity\CheckList;

class FindAllChecklistByDemandRepository extends ObjectManagerAbstract
{
    public function __invoke($demand)
    {
        return $this->objectManager->getRepository(CheckList::class)->findBy([
            'demand' => $demand
        ]);
    }
}