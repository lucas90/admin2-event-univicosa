<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 27/07/2016
 * Time: 17:04
 */

namespace Demand\Repository\Group;

use Demand\Entity\Group;
use Core\Doctrine\ObjectManagerAbstract;

class FindGroupsBySectorRepository extends ObjectManagerAbstract
{
    public function __invoke($sector)
    {
        return $this->objectManager->getRepository(Group::class)->findBy(
            ['sector' => $sector],
            ['order' => 'ASC']
            );
    }
}