<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 27/07/2016
 * Time: 17:02
 */

namespace Demand\Repository\Sector;

use Core\Doctrine\ObjectManagerAbstract;
use Demand\Entity\Sector;

class FindAllSectorRepository extends ObjectManagerAbstract
{
    public function __invoke()
    {
        return $this->objectManager->getRepository(Sector::class)->findAll();
    }
}