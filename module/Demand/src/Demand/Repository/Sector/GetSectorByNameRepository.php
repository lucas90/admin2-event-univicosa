<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 28/07/2016
 * Time: 09:12
 */

namespace Demand\Repository\Sector;

use Demand\Entity\Sector;
use Core\Doctrine\ObjectManagerAbstract;

class GetSectorByNameRepository extends ObjectManagerAbstract
{
    public function __invoke($name)
    {
        return $this->objectManager->getRepository(Sector::class)->findOneBy([
            'sector' => $name
        ]);
    }
}