<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 28/07/2016
 * Time: 11:20
 */

namespace Demand\Repository\Sector;

use Demand\Entity\Sector;
use Core\Doctrine\ObjectManagerAbstract;

class GetSectorByIdRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(Sector::class)->findOneBy(['sectorId' => $id]);
    }
}