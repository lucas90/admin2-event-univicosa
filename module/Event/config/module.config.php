<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use Event\Controller\ListAllOrById;
use Event\Controller\Factory\ListAllOrByIdFactory;

use Event\Controller\MergeOrPersist;
use Event\Controller\Factory\MergeOrPersistFactory;

use Event\Service\Event\Factory\MergeOrPersistServiceFactory;
use Event\Service\Event\MergeOrPersistService;

use Event\Service\Event\ListAllOrByIdService;
use Event\Service\Event\Factory\ListAllOrByIdServiceFactory;

use Event\Service\Event\RemoveService;
use Event\Service\Event\Factory\RemoveServiceFactory;

use Event\Service\Category\ListAllOrByIdService as ListAllOrByIdServiceCategory;
use Event\Service\Category\Factory\ListAllOrByIdServiceFactory as ListAllOrByIdServiceFactoryCategory;

use Event\Service\Category\MergeOrPersistService as MergeOrPersistServiceCategory;
use Event\Service\Category\Factory\MergeOrPersistServiceFactory as MergeOrPersistServiceFactoryCategory;

use Event\Service\EventType\ListAllOrByIdService as ListAllOrByIdServiceEventType;
use Event\Service\EventType\Factory\ListAllOrByIdServiceFactory as ListAllOrByIdServiceFactoryEventType;

use Event\Service\EventType\MergeOrPersistService as MergeOrPersistServiceEventType;
use Event\Service\EventType\Factory\MergeOrPersistServiceFactory as MergeOrPersistServiceFactoryEventType;



use Event\Controller\Remove;
use Event\Controller\Factory\RemoveFactory;



use Zend\Mvc\Router\Http\Literal;
use Zend\Mvc\Router\Http\Segment;

return [
    'controllers' => [
        'factories' => [
            ListAllOrById::class => ListAllOrByIdFactory::class,
            MergeOrPersist::class => MergeOrPersistFactory::class,
            Remove::class => RemoveFactory::class
        ]
    ],
    'service_manager' => [
        'factories' => [
            //Eventos
            ListAllOrByIdService::class => ListAllOrByIdServiceFactory::class,
            MergeOrPersistService::class => MergeOrPersistServiceFactory::class,
            RemoveService::class => RemoveServiceFactory::class,
            //Category
            ListAllOrByIdServiceCategory::class => ListAllOrByIdServiceFactoryCategory::class,
            MergeOrPersistServiceCategory::class => MergeOrPersistServiceFactoryCategory::class,
            //EventType
            ListAllOrByIdServiceEventType::class => ListAllOrByIdServiceFactoryEventType::class,
            MergeOrPersistServiceEventType::class => MergeOrPersistServiceFactoryEventType::class
        ],
    ],
    'router' => [
        'routes' => [
            'eventos' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/eventos',
                    'defaults' => [
                        'controller' => ListAllOrById::class,
                        'action' => 'onDispatch',
                    ],
                ]
            ],

            'evento' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/evento',
                    'defaults' => [
                        'controller' => MergeOrPersist::class
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'adicionar' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/adicionar-editar[/:idevent]',
                            'constraints' => [
                                'idevent' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}'
                            ],
                            'defaults' => [
                                'action' => 'persistOrMergeBasicForm'
                            ]
                        ]
                    ],
                    'editar-complemento' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/editar-complemento[/:idevent]',
                            'constraints' => [
                                'idevent' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}'
                            ],
                            'defaults' => [
                                'action' => 'mergeFormComplement'
                            ]
                        ]
                    ],
                    'selecionar-modulo' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/selecionar-modulo[/:idevent]',
                            'constraints' => [
                                'idevent' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}'
                            ],
                            'defaults' => [
                                'action' => 'selectModule'
                            ]
                        ]
                    ],
                    'remover-evento' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/remover-evento',
                            'defaults' => [
                                'controller' => Remove::class,
                                'action' => 'removeEvent'
                            ]
                        ]
                    ],
                    'ativar' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/ativar[/:id]',
                            'constraints' => [
                                'id' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}'
                            ],
                            'defaults' => [
                                'controller' => MergeOrPersist::class,
                                'action' => 'activate'
                            ]
                        ]
                    ],
                ]
            ]
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy'
        ]
    ]
];
