<?php

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'doctrine' => [
        'driver' => [
            'event_entities' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Event/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Event\Entity' => 'event_entities'
                ]
            ]
        ]
    ]
];
