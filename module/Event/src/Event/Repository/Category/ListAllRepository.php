<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 13/07/2016
 * Time: 14:24
 */

namespace Event\Repository\Category;

use Event\Entity\Category;

use Core\Doctrine\ObjectManagerAbstract;

class ListAllRepository extends ObjectManagerAbstract
{
    public function __invoke()
    {
        return $this->objectManager->getRepository(Category::class)->findAll();
    }
}