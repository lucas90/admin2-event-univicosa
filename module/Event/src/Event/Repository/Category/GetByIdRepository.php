<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 16:42
 */

namespace Event\Repository\Category;

use Event\Entity\Category;

use Rhumsaa\Uuid\Uuid;

use Core\Doctrine\ObjectManagerAbstract;

class GetByIdRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        if (! Uuid::isValid($id)) {
            //disparo de exceção
        }

        return $this->objectManager->getRepository(Category::class)->find($id);
    }
}