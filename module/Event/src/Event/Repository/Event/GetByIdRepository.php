<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 14/07/2016
 * Time: 15:34
 */

namespace Event\Repository\Event;

use Event\Entity\Event;

use Core\Doctrine\ObjectManagerAbstract;

class GetByIdRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(Event::class)->findOneBy([
            'eventId' => $id
        ]);
    }
}