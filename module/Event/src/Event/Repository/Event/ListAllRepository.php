<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 04/07/2016
 * Time: 14:43
 */

namespace Event\Repository\Event;

use Core\Doctrine\ObjectManagerAbstract;
use Event\Entity\Event;

class ListAllRepository extends ObjectManagerAbstract
{
    public function __invoke()
    {
        return $this->objectManager->getRepository(Event::class)->findAll();
    }
}