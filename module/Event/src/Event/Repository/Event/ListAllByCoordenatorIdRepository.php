<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 31/08/2016
 * Time: 14:59
 */

namespace Event\Repository\Event;

use Event\Entity\Event;

use Core\Doctrine\ObjectManagerAbstract;

class ListAllByCoordenatorIdRepository extends ObjectManagerAbstract
{
    public function __invoke($coordenatorId)
    {
        return $this->objectManager->getRepository(Event::class)->findBy(
            ['coordenatorId' => $coordenatorId]
        );
    }
}