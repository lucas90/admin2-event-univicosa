<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 13/07/2016
 * Time: 17:24
 */

namespace Event\Service\EventType\Factory;

use Event\Service\EventType\ListAllOrByIdService;
use Event\Repository\EventType\ListAllRepository;
use Event\Repository\EventType\GetByIdRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ListAllOrByIdServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new ListAllOrByIdService(
            new ListAllRepository($entityManager),
            new GetByIdRepository($entityManager)
        );
    }
}