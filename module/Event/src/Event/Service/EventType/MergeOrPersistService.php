<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 05/07/2016
 * Time: 11:23
 */

namespace Event\Service\EventType;

use Event\Entity\EventType;
use Event\Repository\EventType\GetByIdRepository;

class MergeOrPersistService
{

    private $getEventTypeById;

    public function __construct(GetByIdRepository $getEventTypeByIdRepository)
    {
        $this->getEventTypeById = $getEventTypeByIdRepository;
    }

    public function __invoke(array $dataEventType)
    {
        if(! $dataEventType['eventTypeId']){
            return new EventType($dataEventType['type']);
        }

        $repository = $this->getEventTypeById;

        return $repository($dataEventType['eventTypeId']);
    }
}