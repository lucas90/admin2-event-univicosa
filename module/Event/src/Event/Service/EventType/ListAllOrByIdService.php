<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 13/07/2016
 * Time: 17:21
 */

namespace Event\Service\EventType;

use Event\Repository\EventType\ListAllRepository;
use Event\Repository\EventType\GetByIdRepository;

class ListAllOrByIdService
{
    private $listAll;
    private $getById;

    public function __construct(
        ListAllRepository $listAllRepository,
        GetByIdRepository $getByIdRepository
    ){
        $this->listAll = $listAllRepository;
        $this->getById = $getByIdRepository;
    }

    public function listAll()
    {
        $repository = $this->listAll;

        return $repository;
    }

    public function getById($id)
    {
        $repository = $this->getById;

        return $repository($id);
    }
}