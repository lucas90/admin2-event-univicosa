<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 05/07/2016
 * Time: 14:43
 */

namespace Event\Service\Event;

use Event\Entity\Event;
use Date\Entity\Date;

use Date\DateValidate\DateValidate;

use Event\Repository\Category\GetByIdRepository as GetCategoryByIdRepository;
use Event\Repository\EventType\GetByIdRepository as GetEventTypeByIdRepository;

use Doctrine\Common\Persistence\ObjectManager;
use Rhumsaa\Uuid\Uuid;

use Core\Doctrine\ObjectManagerAbstract;

class MergeOrPersistService extends ObjectManagerAbstract
{
    const BASIC_FORM = 1;
    const COMPLEMENT_FORM = 2;

    private $getCategoryById;
    private $getEventTypeById;

    public function __construct(
        ObjectManager $objectManager,
        GetCategoryByIdRepository $getCategoryByIdRepository,
        GetEventTypeByIdRepository $getEventTypeByIdRepository
    ){
        parent::__construct($objectManager);
        $this->getEventTypeById = $getEventTypeByIdRepository;
        $this->getCategoryById = $getCategoryByIdRepository;
    }

    public function __invoke(Event $event = null, array $dataEvent)
    {

        $getEventTypeById = $this->getEventTypeById;
        $getCategoryById = $this->getCategoryById;

        try{

            if(null !== $event){

                if($dataEvent['form'] === $this::BASIC_FORM){
                    $startInscription = $event->getDate()->getStartInscription();
                    $endInscription = $event->getDate()->getEndInscription();

                    $eventName = $dataEvent['event'];
                    $category = $dataEvent['categoryId'] !== "" ? $getCategoryById($dataEvent['categoryId']) : null ;
                    $eventType = $dataEvent['eventTypeId'] !== "" ? $getEventTypeById($dataEvent['eventTypeId']) : null;
                    $startEvent = \DateTime::createFromFormat('d/m/Y H:i', $dataEvent['dataInicial']) ;
                    $endEvent = \DateTime::createFromFormat('d/m/Y H:i', $dataEvent['dataFinal']) ;

                    $dates = [
                        'startEvent' => $startEvent,
                        'endEvent' => $endEvent,
                        'startInscription' => $startInscription ,
                        'endInscription' => $endInscription
                    ];

                    new DateValidate($dates);

                    $event->getDate()->setStartEvent($startEvent);
                    $event->getDate()->setEndEvent($endEvent);
                    $event->setEventType($eventType);
                    $event->setCategory($category);
                    $event->setEvent($eventName);
                }

                if($dataEvent['form'] === $this::COMPLEMENT_FORM){

                    $startEvent = $event->getDate()->getStartEvent();
                    $endEvent = $event->getDate()->getEndEvent();

                    $startInscription = $dataEvent['dataInicialInscricao'] !== "" ? \DateTime::createFromFormat('d/m/Y H:i', $dataEvent['dataInicialInscricao']) : null;
                    $endInscription = $dataEvent['dataFinalInscricao'] !== "" ? \DateTime::createFromFormat('d/m/Y H:i', $dataEvent['dataFinalInscricao']) : null;
                    $startDisclosure = $dataEvent['dataDivulgacao'] !== "" ? \DateTime::createFromFormat('d/m/Y H:i', $dataEvent['dataDivulgacao']) : null;;
                    $description = $dataEvent['description'];
                    $open = isset($dataEvent['open']) ? 1 : 0;
                    $active = isset($dataEvent['active']) ? 1 : 0;
                    $url = $dataEvent['image'];

                    $dates = [
                        'startEvent' => $startEvent,
                        'endEvent' => $endEvent,
                        'startInscription' => $startInscription ,
                        'endInscription' => $endInscription,
                        'startDisclosure' => $startDisclosure
                    ];

                    new DateValidate($dates);

                    $event->setDescription($description);
                    $event->setOpen($open);
                    $event->setActive($active);
                    $event->getDate()->setStartInscription($startInscription);
                    $event->getDate()->setEndInscription($endInscription);
                    $event->getDate()->setStartDisclosure($startDisclosure);
                    $event->setBanner($url);
                }

                $this->objectManager->merge($event);
            }

            if(null === $event){
                $category = $dataEvent['categoryId'] !== "" ? $getCategoryById($dataEvent['categoryId']) : null;
                $eventType = $dataEvent['eventTypeId'] !== "" ? $getEventTypeById($dataEvent['eventTypeId']) : null;

                $startEvent = $dataEvent['dataInicial'] !== "" ? \DateTime::createFromFormat('d/m/Y H:i', $dataEvent['dataInicial']) : null;
                $endEvent = $dataEvent['dataFinal'] !== "" ? \DateTime::createFromFormat('d/m/Y H:i', $dataEvent['dataFinal']) : null;

                if($startEvent >= $endEvent){
                    throw new \Exception('Os campos de inicio e fim do evento foram preenchidos incorretamente.');
                }

                $newDate = new Date(
                    $startEvent,
                    $endEvent,
                    null,
                    null,
                    null
                );
                $this->objectManager->persist($newDate);

                $event = new Event(
                    $newDate,
                    $eventType,
                    $category,
                    $dataEvent['coordenatorId'],
                    $dataEvent['event'],
                    null,
                    null,
                    0,
                    1
                );
                $this->objectManager->persist($event);
            }

            $this->objectManager->flush();

            return $event;
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function activateEvent(Event $event, $active)
    {
        try {
            $event->setActive($active);

            $this->objectManager->merge($event);
            $this->objectManager->flush();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}