<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 21/07/2016
 * Time: 09:15
 */

namespace Event\Service\Event;

use Event\Repository\Event\GetByIdRepository;

use Core\Doctrine\ObjectManagerAbstract;

use Doctrine\Common\Persistence\ObjectManager;

class RemoveService extends ObjectManagerAbstract
{
    private $getEventById;

    public function __construct(ObjectManager $objectManager, GetByIdRepository $getEventByIdRepository)
    {
        parent::__construct($objectManager);

        $this->getEventById = $getEventByIdRepository;
    }

    public function removeEvent($id)
    {
        try{
            $getEvent = $this->getEventById;
            $event = $getEvent($id);

            $this->objectManager->remove($event);

            $this->objectManager->flush();
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }
}