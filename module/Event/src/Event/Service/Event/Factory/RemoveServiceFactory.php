<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 21/07/2016
 * Time: 09:26
 */

namespace Event\Service\Event\Factory;

use Event\Service\Event\RemoveService;
use Event\Repository\Event\GetByIdRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RemoveServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new RemoveService(
            $entityManager,
            new GetByIdRepository($entityManager)
        );
    }
}