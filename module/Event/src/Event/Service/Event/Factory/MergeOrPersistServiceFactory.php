<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 05/07/2016
 * Time: 14:43
 */

namespace Event\Service\Event\Factory;

use Event\Service\Event\MergeOrPersistService;

use Event\Repository\Category\GetByIdRepository as GetCategoryByIdRepository;
use Event\Repository\EventType\GetByIdRepository as GetEventTypeByIdRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MergeOrPersistServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new MergeOrPersistService(
            $entityManager,
            new GetCategoryByIdRepository($entityManager),
            new GetEventTypeByIdRepository($entityManager)
        );
    }
}