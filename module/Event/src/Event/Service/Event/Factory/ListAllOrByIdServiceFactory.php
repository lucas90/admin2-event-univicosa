<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 04/07/2016
 * Time: 14:54
 */

namespace Event\Service\Event\Factory;

use Event\Service\Event\ListAllOrByIdService;
use Event\Repository\Event\ListAllRepository;
use Event\Repository\Event\GetByIdRepository;
use Event\Repository\Event\ListAllByCoordenatorIdRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ListAllOrByIdServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new ListAllOrByIdService(
            new ListAllRepository($entityManager),
            new GetByIdRepository($entityManager)
            //, new ListAllByCoordenatorIdRepository($entityManager)
        );
    }
}