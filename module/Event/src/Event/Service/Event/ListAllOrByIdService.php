<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 04/07/2016
 * Time: 14:51
 */

namespace Event\Service\Event;

use Event\Repository\Event\ListAllRepository;
use Event\Repository\Event\GetByIdRepository;
use Event\Repository\Event\ListAllByCoordenatorIdRepository;

class ListAllOrByIdService
{
    private $listAll;
    private $getById;
    private $listAllByCoordenatorId;

    public function __construct(
        ListAllRepository $listAllRepository,
        GetByIdRepository $getByIdRepository
    //    , ListAllByCoordenatorIdRepository $listAllByCoordenatorIdRepository
    ){
        $this->listAll = $listAllRepository;
        $this->getById = $getByIdRepository;
    //    $this->listAllByCoordenatorId = $listAllByCoordenatorIdRepository;
    }

    public function listAll()
    {
        $repository = $this->listAll;

        return $repository;
    }

    public function getById($id)
    {
        $repository = $this->getById;

        return $repository($id);
    }

    public function listAllByCoordenatorId($coordenatorId)
    {
        $repository = $this->listAllByCoordenatorId;

        return $repository($coordenatorId);
    }
}