<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 05/07/2016
 * Time: 09:02
 */

namespace Event\Service\Category;

use Event\Repository\Category\GetByIdRepository;
use Event\Entity\Category;

class MergeOrPersistService
{
    private $getCategoryById;

    public function __construct(GetByIdRepository $getCategoryByIdRepository)
    {
        $this->getCategoryById = $getCategoryByIdRepository;
    }

    public function __invoke(array $dataCategory)
    {
        if(! $dataCategory['categoryId']){
            return new Category($dataCategory['category']);
        }

        $repository = $this->getCategoryById;

        return $repository($dataCategory['categoryId']);
    }
}