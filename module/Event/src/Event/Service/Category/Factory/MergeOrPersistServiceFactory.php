<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 05/07/2016
 * Time: 09:03
 */

namespace Event\Service\Category\Factory;

use Event\Service\Category\MergeOrPersistService;
use Event\Repository\Category\GetByIdRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MergeOrPersistServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new MergeOrPersistService(
            new GetByIdRepository($entityManager)
        );
    }
}