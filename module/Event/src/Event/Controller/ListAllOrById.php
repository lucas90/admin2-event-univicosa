<?php

namespace Event\Controller;

use Event\Service\Event\ListAllOrByIdService;
use OAuth\Entity\Permission;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class ListAllOrById extends AbstractActionController
{
    private $service;
    private $permission;

    public function __construct(
        ListAllOrByIdService $listAllOrByIdService
     //   , Permission $permission
    ){
        $this->service = $listAllOrByIdService;
     //   $this->permission = $permission;
    }

    /**
     * @return ViewModel
     */
    public function onDispatchAction()
    {
       /* if($this->permission->getAdmin()){
            $allEvents = $this->service->listAll();
        }else{
            $allEvents = $this->service->listAllByCoordenatorId($this->permission->getUser()->getId());
        }*/
        $allEvents = $this->service->listAll();

        $viewModel = new ViewModel;
        $viewModel->setTemplate('event/list-all/events');
        $viewModel->setVariables([
            'events' => $allEvents
        ]);

        return $viewModel;
    }
}
