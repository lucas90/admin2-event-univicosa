<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 13/07/2016
 * Time: 09:19
 */

namespace Event\Controller;

use Event\Service\Event\MergeOrPersistService;
use Event\Service\Event\ListAllOrByIdService;
use Event\Service\Category\ListAllOrByIdService as ListAllOrByIdServiceCategory;
use Event\Service\EventType\ListAllOrByIdService as ListAllOrByIdServiceEventType;
use Modules\Service\Module\ListAllOrByIdService as ListAllOrByIdServiceModules;
use Wizard\Service\Wizard\ListAllOrByIdService as ListAllOrByIdServiceWizard;
use Wizard\Service\WizardEvent\MergeOrPersistService as MergeOrPersistServiceWizardEvent;

use OAuth\Entity\User\User;

use Application\InputVerification\ValidateImage;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class MergeOrPersist extends AbstractActionController
{
    private $service;
    private $mergeOrPersistEvent;
    private $mergeOrPersistWizardEvent;
    private $serviceCategory;
    private $serviceEventType;
    private $serviceModules;
    private $serviceWizard;
    private $user;

    public function __construct(
        MergeOrPersistService $mergeOrPersistService,
        ListAllOrByIdService $service,
        ListAllOrByIdServiceCategory $listAllOrByIdServiceCategory,
        ListAllOrByIdServiceEventType $listAllOrByIdServiceEventType,
        ListAllOrByIdServiceModules $listAllOrByIdServiceModules,
        ListAllOrByIdServiceWizard $listAllOrByIdServiceWizard,
        MergeOrPersistServiceWizardEvent $mergeOrPersistServiceWizardEvent
       // , User $user

    ){
        $this->service = $service;
        $this->mergeOrPersistEvent = $mergeOrPersistService;
        $this->serviceCategory = $listAllOrByIdServiceCategory;
        $this->serviceEventType = $listAllOrByIdServiceEventType;
        $this->serviceModules = $listAllOrByIdServiceModules;
        $this->serviceWizard = $listAllOrByIdServiceWizard;
        $this->mergeOrPersistWizardEvent = $mergeOrPersistServiceWizardEvent;
       // $this->user = $user;
    }

    public function persistOrMergeBasicFormAction()
    {
        $request = $this->getRequest();
        $id = (string) $this->params()->fromRoute('idevent');
        $event = null;

        if($id !== ""){
            $event = $this->service->getById($id);
        }

        $wizard = [];
        $modules[] = $this->serviceModules->getByName('Básico');
        $modules[] = $this->serviceModules->getByName('Complemento');
        $modules[] = $this->serviceModules->getByName('Atividades');

        foreach($modules as $module){
            $wizard[] = $this->serviceWizard->getByModule($module)->getWizardId();
        }

        if($request->isXmlHttpRequest()){
            $jsonModel = new JsonModel();

            $data = $request->getPost()->toArray();
            $data['coordenatorId'] = "f5565542-6f05-4097-9b65-0a75d150dfa7"/*$this->user->getId()*/;
            $data['form'] = 1; // flag para definir o form que será editado basciForm = 1 \ complementForm = 2

            try{
                $mergeOrPersistWizardEvent = $this->mergeOrPersistWizardEvent;
                $mergeOrPersistEvent = $this->mergeOrPersistEvent;
                $eventReturn = $mergeOrPersistEvent($event, $data);
                $eventId = $id !== "" ? $id : (string) $eventReturn->getEventId();

                if($id === ''){
                    $wizard['eventId'] = $eventId;
                    $mergeOrPersistWizardEvent($wizard);
                }

                $jsonModel->setVariables([
                    'success' => true,
                    'message' => 'Evento Editado',
                    'eventId' => $eventId,
                    'chooseModels' => $id !== "" ? false : true
                ]);




            }catch (\Exception $e){
                $jsonModel->setVariables([
                    'success' => false,
                    'message' => $e->getMessage()
                ]);
            }

            return $jsonModel;
        }


        $viewModel = new ViewModel();
        $viewModel->setTemplate('event/merge-or-persist/basic-form');
        $viewModel->setVariables([
            'event' => $event,
            'categories' => $this->serviceCategory->listAll(),
            'eventTypes' => $this->serviceEventType->listAll()
        ]);

        return $viewModel;
    }

    public function mergeFormComplementAction()
    {
        $request = $this->getRequest();
        $id = (string) $this->params()->fromRoute('idevent');
        $event = $this->service->getById($id);

        if($request->isXmlHttpRequest()){
            $jsonModel = new JsonModel();

            $data = $request->getPost()->toArray();

            try{
                $data['form'] = 2;
                //new ValidateImage($data['image']);

                $mergeOrPersist = $this->mergeOrPersistEvent;

                $mergeOrPersist($event, $data);

                $jsonModel->setVariables([
                    'success' => true,
                    'message' => 'Evento Editado',
                    'eventId' => $id
                ]);
            }catch (\Exception $e){
                $jsonModel->setVariables([
                    'success' => false,
                    'message' => $e->getMessage()
                ]);
            }

            return $jsonModel;
        }

        $viewModel = new ViewModel();
        $viewModel->setTemplate('event/merge-or-persist/basic-form-complement');
        $viewModel->setVariables([
            'event' => $event
        ]);

        return $viewModel;
    }


    public function activateAction()
    {
        $jsonModel = new JsonModel;

        $request = $this->getRequest();

        $id = (string) $this->params()->fromRoute('id');

        try {
            $event = $this->service->getById($id);

            if ($request->isXmlHttpRequest()) {
                try {
                    $this->mergeOrPersistEvent->activateEvent($event, $request->getPost('active'));

                    $jsonModel->setVariables([
                        'success' => true
                    ]);
                } catch (\Exception $e) {
                    $jsonModel->setVariables([
                        'success' => false,
                        'message' => $e->getMessage()
                    ]);
                }

                return $jsonModel;
            }

        } catch (\Exception $e) {
            $jsonModel->setVariables([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        $jsonModel->setVariables([
            'success' => false,
            'message' => 'Falha ao atualizar status da notícia'
        ]);

        return $jsonModel;
    }
}