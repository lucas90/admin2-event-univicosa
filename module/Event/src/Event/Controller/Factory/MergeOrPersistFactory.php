<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 13/07/2016
 * Time: 09:18
 */

namespace Event\Controller\Factory;

use Event\Controller\MergeOrPersist;
use Event\Service\Event\MergeOrPersistService;
use Event\Service\Event\ListAllOrByIdService as ListAllOrByIdServiceEvent;
use Event\Service\Category\ListAllOrByIdService as ListAllOrByIdServiceCategory;
use Event\Service\EventType\ListAllOrByIdService as ListAllOrByIdServiceEventType;
use Modules\Service\Module\ListAllOrByIdService as ListAllOrByIdServiceModules;
use Wizard\Service\Wizard\ListAllOrByIdService as ListAllOrByIdServiceWizard;
use Wizard\Service\WizardEvent\MergeOrPersistService as MergeOrPersistServiceWizardEvent;

use OAuth\Token\GetParserToken;
use OAuth\Service\GetUserService;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MergeOrPersistFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        $realServiceLocator = $serviceLocator->getServiceLocator();
        $mergeOrPersistServiceEvent = $realServiceLocator->get(MergeOrPersistService::class);
        $mergeOrPersistServiceWizardEvent = $realServiceLocator->get(MergeOrPersistServiceWizardEvent::class);
        $listAllOrByIdServiceEvent = $realServiceLocator->get(ListAllOrByIdServiceEvent::class);
        $listAllOrByIdServiceCategory = $realServiceLocator->get(ListAllOrByIdServiceCategory::class);
        $listAllOrByIdServiceEventType = $realServiceLocator->get(ListAllOrByIdServiceEventType::class);
        $listAllOrByIdServiceModules = $realServiceLocator->get(ListAllOrByIdServiceModules::class);
        $listAllOrByIdServiceWizard = $realServiceLocator->get(ListAllOrByIdServiceWizard::class);

        /** @var $request \Zend\Http\Header\Cookie */
        $cookie = $realServiceLocator->get('Application')->getMvcEvent()->getRequest()->getCookie();

       // $userId = (new GetParserToken($cookie['token']))->getJti();

        /** @var $userService GetUserService */
        $userService = $realServiceLocator->get(GetUserService::class);
      //  $user = $userService->getById($userId);

        return new MergeOrPersist(
            $mergeOrPersistServiceEvent,
            $listAllOrByIdServiceEvent,
            $listAllOrByIdServiceCategory,
            $listAllOrByIdServiceEventType,
            $listAllOrByIdServiceModules,
            $listAllOrByIdServiceWizard,
            $mergeOrPersistServiceWizardEvent
            //, $user
        );
    }
}