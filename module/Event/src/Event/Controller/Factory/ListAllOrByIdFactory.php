<?php

namespace Event\Controller\Factory;

use Event\Controller\ListAllOrById;
use Event\Service\Event\ListAllOrByIdService;

use OAuth\Token\GetParserToken;
use OAuth\Service\GetUserService;
use OAuth\Service\GetSystemService;
use OAuth\Service\ListPermissionsByUserService;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class ListAllOrByIdFactory implements FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        $realServiceLocator  = $serviceLocator->getServiceLocator();

        /** @var $request \Zend\Http\Header\Cookie */

        $cookie = $realServiceLocator->get('Application')->getMvcEvent()->getRequest()->getCookie();

        //$userId = (new GetParserToken($cookie['token']))->getJti();

        /** @var $userService GetUserService */
        //$userService = $realServiceLocator->get(GetUserService::class);
       // $user = $userService->getById($userId);

        /** @var $systemService GetSystemService */
       // $systemService = $realServiceLocator->get(GetSystemService::class);
       // $system = $systemService->getSystemBySystem('evento_new');

        /** @var $permissionService ListPermissionsByUserService */
      //  $permissionService = $realServiceLocator->get(ListPermissionsByUserService::class);
       // $permission = $permissionService->GetPermissionByUserAndSystem($user, $system);

        $listAllOrByIdService = $realServiceLocator->get(ListAllOrByIdService::class);

        return new ListAllOrById(
            $listAllOrByIdService
          //  , $permission
        );
    }
}
