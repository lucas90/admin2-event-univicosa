<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 04/07/2016
 * Time: 14:11
 */

namespace Event\Entity;

use Event\Entity\Category;
use Date\Entity\Date;
use Event\Entity\EventType;

use Doctrine\ORM\Mapping as ORM;
use Rhumsaa\Uuid\Uuid;

/**
 * Class Event
 * @package Event\Entity
 * @ORM\Entity
 * @ORM\Table(name="evento")
 */
class Event
{
    /**
     * @ORM\Id
     * @ORM\Column(name="evento_id", type="string")
     */
    private $eventId;

    /**
     * @ORM\OneToOne(targetEntity="Date\Entity\Date", cascade={"remove"})
     * @ORM\JoinColumn(name="data_id", referencedColumnName="data_id")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Event\Entity\EventType")
     * @ORM\JoinColumn(name="tipo_evento_id", referencedColumnName="tipo_evento_id")
     */
    private $eventType;

    /**
     * @ORM\ManyToOne(targetEntity="Event\Entity\Category")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="categoria_id")
     */
    private $category;

    /**
     * @ORM\Column(name="coordernador_id", type="string")
     */
    private $coordenatorId;

    /**
     * @ORM\Column(name="evento", type="string")
     */
    private $event;

    /**
     * @ORM\Column(name="banner", type="string")
     */
    private $banner;

    /**
     * @ORM\Column(name="descricao", type="string")
     */
    private $description;

    /**
     * @ORM\Column(name="aberto", type="string")
     */
    private $open;

    /**
     * @ORM\Column(name="ativo", type="string")
     */
    private $active;

    /**
     * @param Date $date
     * @param EventType $eventType
     * @param Category $category
     * @param $coordenatorId
     * @param $event
     * @param $banner
     * @param $description
     * @param $open
     * @param $active
     */
    public function __construct(Date $date, EventType $eventType, Category $category, $coordenatorId, $event, $banner, $description, $open, $active)
    {
        $this->eventId = Uuid::uuid4();
        $this->date = $date;
        $this->eventType = $eventType;
        $this->category = $category;
        $this->coordenatorId = $coordenatorId;
        $this->event = trim($event);
        $this->banner = trim($banner);
        $this->description = trim($description);
        $this->open = $open;
        $this->active = $active;

    }


    /**
     * @return mixed
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate(Date $date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @param mixed $eventType
     */
    public function setEventType(EventType $eventType)
    {
        $this->eventType = $eventType;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getCoordenatorId()
    {
        return $this->coordenatorId;
    }

    /**
     * @param mixed $coordenatorId
     */
    public function setCoordenatorId($coordenatorId)
    {
        $this->coordenatorId = $coordenatorId;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param mixed $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     * @return mixed
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * @param mixed $banner
     */
    public function setBanner($banner)
    {
        $this->banner = $banner;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getOpen()
    {
        return $this->open;
    }

    /**
     * @param mixed $open
     */
    public function setOpen($open)
    {
        $this->open = $open;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }


}