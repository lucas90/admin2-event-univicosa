<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 04/07/2016
 * Time: 17:40
 */

namespace Event\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class EventType
 * @package Event\Entity
 * @ORM\Entity
 * @ORM\Table(name="tipo_evento")
 */
class EventType
{
    /**
     * @ORM\Id
     * @ORM\Column(name="tipo_evento_id")
     */
    private $eventTypeId;

    /**
     * @ORM\Column(name="tipo")
     */
    private $type;

    /**
     * EventType constructor.
     * @param $type
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getEventTypeId()
    {
        return $this->eventTypeId;
    }

}