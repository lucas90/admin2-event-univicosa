<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 04/07/2016
 * Time: 16:59
 */

namespace Event\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Category
 * @package Event\Entity
 * @ORM\Entity
 * @ORM\Table(name="categoria")
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\Column(name="categoria_id", type="string")
     */
    private $categoryId;

    /**
     * @ORM\Column(name="categoria", type="string")
     */
    private $category;

    /**
     * Category constructor.
     * @param $category
     */
    public function __construct($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }



    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }



}