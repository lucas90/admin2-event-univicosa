<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 01/08/2016
 * Time: 11:31
 */

namespace Costs\Controller;

use Costs\Service\MergeOrPersistService;
use Costs\Service\ListAllOrByIdService;
use Event\Service\Event\ListAllOrByIdService as LisAllOrByIdServiceEvent;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class MergeOrPersist extends AbstractActionController
{
    private $mergeOrPersist;
    private $service;
    private $serviceEvent;

    public function __construct(
        MergeOrPersistService $mergeOrPersistService,
        ListAllOrByIdService $service,
        LisAllOrByIdServiceEvent $serviceEvent
    ){
        $this->mergeOrPersist = $mergeOrPersistService;
        $this->service = $service;
        $this->serviceEvent = $serviceEvent;
    }

    public function mergeOrPersistAction()
    {
        $types = $this->service->listAllType();
        $eventId = (string) $this->params()->fromRoute('idevent');
        $event = $this->serviceEvent->getById($eventId);
        $cost = $this->service->getCostByEvent($event);
        $itemsEstimate = [];

        if($cost !== null){
            $itemsEstimate = $this->service->listItemEstimateByEstimate($cost->getCostId());
        }

        $items = null;

        if(count($itemsEstimate) > 0){
            $items = $itemsEstimate;
        }

        foreach($types as $type){
            switch ($type->getType()){
                case 'Receita':
                    $receitas = $this->service->listSubtypeByType($type);
                    break;
                case 'Despesa':
                    $despesas = $this->service->listSubtypeByType($type);
                    break;
            }
        }

        $request = $this->getRequest();

        if($request->isXmlHttpRequest()){
            $jsonModel = new JsonModel();

            $data = $request->getPost()->toArray();
            $data['eventId'] = $eventId;

            try{
                $mergeOrPersist = $this->mergeOrPersist;
                $mergeOrPersist($data);

                $jsonModel->setVariables([
                    'success' => true,
                    'message' => 'Custos aceitos',
                    'eventId' => $eventId
                ]);
            }catch (\Exception $e){
                $jsonModel->setVariables([
                    'success' => false,
                    'message' => $e->getMessage()
                ]);
            }

            return $jsonModel;
        }

        $viewModel = new ViewModel();
        $viewModel->setTemplate('costs/merge-or-persist/index');
        $viewModel->setVariables([
            'receitas' => $receitas,
            'despesas' => $despesas,
            'eventId' => $eventId,
            'itemsEstimate' => $items,
            'event' => $event
        ]);

        return $viewModel;
    }
}