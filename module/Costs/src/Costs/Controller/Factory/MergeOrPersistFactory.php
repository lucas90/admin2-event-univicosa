<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 01/08/2016
 * Time: 11:30
 */

namespace Costs\Controller\Factory;

use Costs\Service\MergeOrPersistService;
use Costs\Service\ListAllOrByIdService;
use Event\Service\Event\ListAllOrByIdService as ListAllOrByIdServiceEvent;
use Costs\Controller\MergeOrPersist;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MergeOrPersistFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $realServiceLocator = $serviceLocator->getServiceLocator();
        $mergeOrPersistServiceCosts = $realServiceLocator->get(MergeOrPersistService::class);
        $service = $realServiceLocator->get(ListAllOrByIdService::class);
        $serviceEvent = $realServiceLocator->get(ListAllOrByIdServiceEvent::class);

        return new MergeOrPersist(
            $mergeOrPersistServiceCosts,
            $service,
            $serviceEvent
        );
    }
}