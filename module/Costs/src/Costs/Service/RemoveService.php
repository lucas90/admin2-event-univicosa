<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 25/08/2016
 * Time: 09:10
 */

namespace Costs\Service;

use Costs\Repository\GetCostByEventRepository;
use Costs\Repository\GetEstimateByIdRepository;
use Costs\Repository\ListItemEstimateByEstimate;

use Doctrine\Common\Persistence\ObjectManager;

use Core\Doctrine\ObjectManagerAbstract;

class RemoveService extends ObjectManagerAbstract
{
    private $getCostByEvent;
    private $getEstimateById;
    private $listItemEstimateByEstimate;

    public function __construct(
        ObjectManager $objectManager,
        GetCostByEventRepository $getCostByEventRepository,
        GetEstimateByIdRepository $getEstimateByIdRepository,
        ListItemEstimateByEstimate $listItemEstimateByEstimate
    ){
        parent::__construct($objectManager);
        $this->getCostByEvent = $getCostByEventRepository;
        $this->getEstimateById = $getEstimateByIdRepository;
        $this->listItemEstimateByEstimate = $listItemEstimateByEstimate;
    }

    public function removeAllByEvent($eventId)
    {
        $getCost = $this->getCostByEvent;
        $cost = $getCost($eventId);

        if($cost !== null){
            $getEstimate = $this->getEstimateById;
            $estimate = $getEstimate($cost);

            $listItemEstimate = $this->listItemEstimateByEstimate;
            $itemsEstimate = $listItemEstimate($estimate);

            foreach($itemsEstimate as $item){
                $this->objectManager->remove($item);
                $this->objectManager->flush();
            }

            $this->objectManager->remove($estimate);
            $this->objectManager->flush();

            $this->objectManager->remove($cost);
            $this->objectManager->flush();



        }

    }
}