<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 01/08/2016
 * Time: 11:29
 */

namespace Costs\Service;

use Costs\Entity\Estimate;
use Costs\Entity\Cost;
use Costs\Entity\ItemEstimate;
use Costs\Repository\GetSubtypeByIdRepository;
use Costs\Repository\GetCostByEventRepository;
use Costs\Repository\GetEstimateByIdRepository;
use Costs\Repository\ListItemEstimateByEstimate;

use Event\Repository\Event\GetByIdRepository as GetEventByIdRepository;

use Doctrine\Common\Persistence\ObjectManager;
use Core\Doctrine\ObjectManagerAbstract;

class MergeOrPersistService extends ObjectManagerAbstract
{
    private $getEventById;
    private $getSubtypeById;
    private $getCostByEvent;
    private $getEstimateById;
    private $listItemEstimateByEstimate;

    public function __construct(
        ObjectManager $objectManager,
        GetEventByIdRepository $getEventByIdRepository,
        GetSubtypeByIdRepository $getSubtypeByIdRepository,
        GetCostByEventRepository $getCostByEventRepository,
        GetEstimateByIdRepository $getEstimateByIdRepository,
        ListItemEstimateByEstimate $listItemEstimateByEstimate
    ){
        parent::__construct($objectManager);
        $this->getEventById = $getEventByIdRepository;
        $this->getSubtypeById = $getSubtypeByIdRepository;
        $this->getCostByEvent = $getCostByEventRepository;
        $this->getEstimateById = $getEstimateByIdRepository;
        $this->listItemEstimateByEstimate = $listItemEstimateByEstimate;
    }

    public function __invoke(array $data){

        $getCostByEvent = $this->getCostByEvent;
        $getEventById = $this->getEventById;
        $getEstimateById = $this->getEstimateById;
        $event = $getEventById($data['eventId']);
        $cost = $getCostByEvent($event);

        try{
            if($cost !== null){

                $listItemEstimateByEstimate = $this->listItemEstimateByEstimate;
                $estimate = $getEstimateById($cost->getCostId());
                $itemsEstimate = $listItemEstimateByEstimate($estimate);

                foreach($itemsEstimate as $itemEstimate){
                    $this->objectManager->remove($itemEstimate);
                }

                $this->insert($data, $estimate);
            }

            if($cost === null){

                $estimate = new Estimate();
                $cost = new Cost($estimate->getEstimateId(), $event);

                $this->objectManager->persist($estimate);
                $this->objectManager->persist($cost);

                $this->insert($data, $estimate);
            }
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

        return null;
    }

    public function insert($data, $estimate)
    {
        $getSubtypeById = $this->getSubtypeById;

        $count = 0;
        foreach($data['RevenueExpenditureId'] as $id){
            $subtype = $getSubtypeById($id);
            $itemEstimate = new ItemEstimate($estimate, $subtype, $data['RevenueExpenditureValue'][$count++], '');

            $this->objectManager->persist($itemEstimate);
        }

        if(isset($data['RevenueExpenditureOthersName'])){
            $count = 0;
            foreach($data['RevenueExpenditureOthersId'] as $id){
                $subtype = $getSubtypeById($id);

                if($data['RevenueExpenditureOthersValue'][$count] !== ''){
                    $itemEstimate = new ItemEstimate($estimate, $subtype, $data['RevenueExpenditureOthersValue'][$count], $data['RevenueExpenditureOthersName'][$count]);
                }
                $count++;

                $this->objectManager->persist($itemEstimate);
            }

        }

        $this->objectManager->flush();
    }
}