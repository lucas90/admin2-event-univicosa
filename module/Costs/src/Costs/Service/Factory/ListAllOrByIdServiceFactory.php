<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 02/08/2016
 * Time: 14:27
 */

namespace Costs\Service\Factory;

use Costs\Service\ListAllOrByIdService;

use Costs\Repository\ListSubtypeByTypeRepository;
use Costs\Repository\ListAllTypeRepository;
use Costs\Repository\GetCostByEventRepository;
use Costs\Repository\ListItemEstimateByEstimate;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ListAllOrByIdServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new ListAllOrByIdService(
            new ListSubtypeByTypeRepository($entityManager),
            new ListAllTypeRepository($entityManager),
            new GetCostByEventRepository($entityManager),
            new ListItemEstimateByEstimate($entityManager)
        );
    }
}