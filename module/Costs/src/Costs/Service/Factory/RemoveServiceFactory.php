<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 25/08/2016
 * Time: 09:09
 */

namespace Costs\Service\Factory;

use Costs\Service\RemoveService;
use Costs\Repository\GetCostByEventRepository;
use Costs\Repository\GetEstimateByIdRepository;
use Costs\Repository\ListItemEstimateByEstimate;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RemoveServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new RemoveService(
            $entityManager,
            new GetCostByEventRepository($entityManager),
            new GetEstimateByIdRepository($entityManager),
            new ListItemEstimateByEstimate($entityManager)

        );
    }
}