<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 01/08/2016
 * Time: 11:30
 */

namespace Costs\Service\Factory;

use Costs\Service\MergeOrPersistService;
use Event\Repository\Event\GetByIdRepository as GetEventByIdRepository;
use Costs\Repository\GetSubtypeByIdRepository;
use Costs\Repository\GetCostByEventRepository;
use Costs\Repository\GetEstimateByIdRepository;
use Costs\Repository\ListItemEstimateByEstimate;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MergeOrPersistServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new MergeOrPersistService(
            $entityManager,
            new GetEventByIdRepository($entityManager),
            new GetSubtypeByIdRepository($entityManager),
            new GetCostByEventRepository($entityManager),
            new GetEstimateByIdRepository($entityManager),
            new ListItemEstimateByEstimate($entityManager)
        );
    }
}