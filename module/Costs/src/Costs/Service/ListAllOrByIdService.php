<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 02/08/2016
 * Time: 14:27
 */

namespace Costs\Service;

use Costs\Repository\ListSubtypeByTypeRepository;
use Costs\Repository\ListAllTypeRepository;
use Costs\Repository\GetCostByEventRepository;
use Costs\Repository\ListItemEstimateByEstimate;

class ListAllOrByIdService
{
    private $listSubtypeByType;
    private $listAllType;
    private $getCostByEvent;
    private $listItemEstimateByEstimate;

    public function __construct(
        ListSubtypeByTypeRepository $listSubtypeByTypeRepository,
        ListAllTypeRepository $listAllTypeRepository,
        GetCostByEventRepository $getCostByEventRepository,
        ListItemEstimateByEstimate $listItemEstimateByEstimate
    ){
        $this->listSubtypeByType = $listSubtypeByTypeRepository;
        $this->listAllType = $listAllTypeRepository;
        $this->getCostByEvent = $getCostByEventRepository;
        $this->listItemEstimateByEstimate = $listItemEstimateByEstimate;
    }

    public function listSubtypeByType($type)
    {
        $repository = $this->listSubtypeByType;

        return $repository($type);
    }

    public function listAllType()
    {
        $repository = $this->listAllType;

        return $repository();
    }

    public function getCostByEvent($event)
    {
        $repository = $this->getCostByEvent;

        return $repository($event);
    }

    public function listItemEstimateByEstimate($estimate)
    {
        $repository = $this->listItemEstimateByEstimate;

        return $repository($estimate);
    }
}