<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 04/08/2016
 * Time: 11:40
 */

namespace Costs\Repository;

use Costs\Entity\Cost;

use Core\Doctrine\ObjectManagerAbstract;

class GetCostByEventRepository extends ObjectManagerAbstract
{
    public function __invoke($event)
    {
        return $this->objectManager->getRepository(Cost::class)->findOneBy(
            ['event' => $event]
        );
    }
}