<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 02/08/2016
 * Time: 14:30
 */

namespace Costs\Repository;

use Costs\Entity\SubType;

use Core\Doctrine\ObjectManagerAbstract;

class ListSubtypeByTypeRepository extends ObjectManagerAbstract
{
    public function __invoke($type)
    {
        return $this->objectManager->getRepository(SubType::class)->findBy(
            ['type' => $type],
            ['order' => 'ASC']
        );
    }
}