<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 04/08/2016
 * Time: 15:08
 */

namespace Costs\Repository;

use Costs\Entity\Estimate;

use Core\Doctrine\ObjectManagerAbstract;

class GetEstimateByIdRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(Estimate::class)->findOneBy(['estimateId' => $id]);
    }
}