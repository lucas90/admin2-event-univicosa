<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 02/08/2016
 * Time: 15:44
 */

namespace Costs\Repository;

use Costs\Entity\Type;

use Core\Doctrine\ObjectManagerAbstract;

class ListAllTypeRepository extends ObjectManagerAbstract
{
    public function __invoke()
    {
        return $this->objectManager->getRepository(Type::class)->findAll();
    }
}