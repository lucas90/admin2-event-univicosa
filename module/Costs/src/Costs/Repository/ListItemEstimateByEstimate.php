<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 04/08/2016
 * Time: 15:14
 */

namespace Costs\Repository;

use Core\Doctrine\ObjectManagerAbstract;
use Costs\Entity\ItemEstimate;

class ListItemEstimateByEstimate extends ObjectManagerAbstract
{
    public function __invoke($estimate)
    {
        return $this->objectManager->getRepository(ItemEstimate::class)->findBy(
            ['estimate' => $estimate]
        );
    }
}