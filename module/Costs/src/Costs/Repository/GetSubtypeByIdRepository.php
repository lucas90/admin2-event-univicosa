<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 04/08/2016
 * Time: 09:08
 */

namespace Costs\Repository;

use Costs\Entity\SubType;

use Core\Doctrine\ObjectManagerAbstract;

class GetSubtypeByIdRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(SubType::class)->findOneBy(
            ['subtypeId' => $id]
        );
    }
}