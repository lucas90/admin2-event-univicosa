<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 01/08/2016
 * Time: 17:33
 */

namespace Costs\Entity;

use Doctrine\ORM\Mapping as ORM;
use Rhumsaa\Uuid\Uuid;

/**
 * Class Estimate
 * @package Costs\Entity
 * @ORM\Entity
 * @ORM\Table(name="custo.estimativa")
 */
class Estimate
{
    /**
     * @ORM\Id
     * @ORM\Column(name="estimativa_id", type="string")
     */
    private $estimateId;

    /**
     * Estimate constructor.
     */
    public function __construct()
    {
        $this->estimateId = (string) Uuid::uuid4();
    }

    /**
     * @return mixed
     */
    public function getEstimateId()
    {
        return $this->estimateId;
    }



}