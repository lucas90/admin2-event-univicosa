<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 01/08/2016
 * Time: 11:33
 */

namespace Costs\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class SubType
 * @package Costs\Entity
 * @ORM\Entity
 * @ORM\Table(name="custo.subtipo")
 */
class SubType
{
    /**
     * @ORM\Id
     * @ORM\Column(name="subtipo_id", type="string")
     */
    private $subtypeId;

    /**
     * @ORM\ManyToOne(targetEntity="Costs\Entity\Type")
     * @ORM\JoinColumn(name="tipo_id", referencedColumnName="tipo_id")
     */
    private $type;

    /**
     * @ORM\Column(name="subtipo", type="string")
     */
    private $subtype;

    /**
     * @ORM\Column(name="ordem", type="string")
     */
    private $order;

    /**
     * @return mixed
     */
    public function getSubtypeId()
    {
        return $this->subtypeId;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getSubtype()
    {
        return $this->subtype;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

}