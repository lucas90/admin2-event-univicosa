<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 01/08/2016
 * Time: 11:32
 */

namespace Costs\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Type
 * @package Costs\Entity
 * @ORM\Entity
 * @ORM\Table(name="custo.tipo")
 */
class Type
{
    /**
     * @var
     * @ORM\Id
     * @ORM\Column(name="tipo_id", type="string")
     */
    private $typeId;

    /**
     * @var
     * @ORM\Column(name="tipo", type="string")
     */
    private $type;

    /**
     * @var
     * @ORM\Column(name="fator", type="string")
     */
    private $factor;

    /**
     * @return mixed
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getFactor()
    {
        return $this->factor;
    }


}