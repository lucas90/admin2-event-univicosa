<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 01/08/2016
 * Time: 11:38
 */

namespace Costs\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Cost
 * @package Costs\Entity
 * @ORM\Entity
 * @ORM\Table(name="custo.custo")
 */
class Cost
{
    /**
     * @ORM\Id
     * @ORM\Column(name="custo_id", type="string")
     */
    private $costId;

    /**
     * @ORM\OneToOne(targetEntity="Event\Entity\Event")
     * @ORM\JoinColumn(name="evento_id", referencedColumnName="evento_id")
     */
    private $event;

    /**
     * Cost constructor.
     * @param $costId
     * @param $event
     */
    public function __construct($costId, $event)
    {
        $this->costId = $costId;
        $this->event = $event;
    }

    /**
     * @return mixed
     */
    public function getCostId()
    {
        return $this->costId;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }
}