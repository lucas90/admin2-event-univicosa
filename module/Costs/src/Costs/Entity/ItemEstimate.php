<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 01/08/2016
 * Time: 11:35
 */

namespace Costs\Entity;

use Costs\Entity\Estimate;
use Costs\Entity\SubType;

use Doctrine\ORM\Mapping as ORM;
use Rhumsaa\Uuid\Uuid;

/**
 * Class ItemEstimate
 * @package Costs\Entity
 * @ORM\Entity
 * @ORM\Table(name="custo.item_estimativa")
 */
class ItemEstimate
{
    /**
     * @ORM\Id
     * @ORM\Column(name="item_estimativa_id", type="string")
     */
    private $itemEstimateId;

    /**
     * @ORM\ManyToOne(targetEntity="Costs\Entity\Estimate")
     * @ORM\JoinColumn(name="estimativa_id", referencedColumnName="estimativa_id")
     */
    private $estimate;

    /**
     * @ORM\ManyToOne(targetEntity="Costs\Entity\SubType")
     * @ORM\JoinColumn(name="subtipo_id", referencedColumnName="subtipo_id")
     */
    private $subtype;

    /**
     * @ORM\Column(name="valor", type="string")
     */
    private $value;

    /**
     * @ORM\Column(name="descricao", type="string")
     */
    private $description;

    /**
     * ItemEstimate constructor.
     * @param $estimate
     * @param $subtype
     * @param $value
     * @param $description
     */
    public function __construct(Estimate $estimate, SubType $subtype, $value, $description)
    {
        $this->itemEstimateId = (string) Uuid::uuid4();
        $this->estimate = $estimate;
        $this->subtype = $subtype;
        $this->value = $value;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getItemEstimateId()
    {
        return $this->itemEstimateId;
    }

    /**
     * @return mixed
     */
    public function getEstimate()
    {
        return $this->estimate;
    }

    /**
     * @return mixed
     */
    public function getSubtype()
    {
        return $this->subtype;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


}