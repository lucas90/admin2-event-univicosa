<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use Costs\Controller\MergeOrPersist;
use Costs\Controller\Factory\MergeOrPersistFactory;

use Costs\Service\MergeOrPersistService;
use Costs\Service\Factory\MergeOrPersistServiceFactory;

use Costs\Service\ListAllOrByIdService;
use Costs\Service\Factory\ListAllOrByIdServiceFactory;

use Costs\Service\RemoveService;
use Costs\Service\Factory\RemoveServiceFactory;

use Zend\Mvc\Router\Http\Literal;
use Zend\Mvc\Router\Http\Segment;

return [
    'controllers' => [
        'factories' => [
            MergeOrPersist::class => MergeOrPersistFactory::class,

        ]
    ],
    'service_manager' => [
        'factories' => [
            MergeOrPersistService::class => MergeOrPersistServiceFactory::class,
            ListAllOrByIdService::class => ListAllOrByIdServiceFactory::class,
            RemoveService::class => RemoveServiceFactory::class
        ],
    ],
    'router' => [
        'routes' => [
            'custos' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/custos[/:idevent]',
                    'constraints' => [
                        'idevent' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}'
                    ],
                    'defaults' => [
                        'controller' => '',
                        'action' => ''
                    ],
                ]
            ],

            'custo' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/custo',
                    'defaults' => [
                        'controller' => MergeOrPersist::class
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'adicionar-editar' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/adicionar-editar[/:idevent]',
                            'constraints' => [
                                'idevent' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}'
                            ],
                            'defaults' => [
                                'action' => 'mergeOrPersist'
                            ]
                        ]
                    ],
                    'remover' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/remover-atividade',
                            'defaults' => [
                                'controller' => '',
                                'action' => ''
                            ]
                        ]
                    ],
                ]
            ]
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy'
        ]
    ]
];
