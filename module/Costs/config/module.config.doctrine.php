<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'doctrine' => [
        'driver' => [
            'costs_entities' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Costs/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Costs\Entity' => 'costs_entities'
                ]
            ]
        ]
    ]
];