<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 01/08/2016
 * Time: 11:23
 */

namespace Costs;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return array_merge_recursive(
            require __DIR__ . '/config/module.config.php',
            require __DIR__ . '/config/module.config.doctrine.php'
        );
    }
}