<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use Activity\Controller\ListAllOrById;
use Activity\Controller\Factory\ListAllOrByIdFactory;

use Activity\Service\Activity\ListAllOrByIdService;
use Activity\Service\Activity\Factory\ListAllOrByIdServiceFactory;

use Activity\Service\Activity\MergeOrPersistService;
use Activity\Service\Activity\Factory\MergeOrPersistServiceFactory;

use Activity\Controller\Factory\MergeOrPersistFactory;
use Activity\Controller\MergeOrPersist;

use Activity\Controller\Factory\RemoveFactory;
use Activity\Controller\Remove;

use Activity\Service\Activity\RemoveService;
use Activity\Service\Activity\Factory\RemoveServiceFactory;

use Activity\Service\ActivityType\ListAllOrByIdService as ListAllOrByIdActivityTypeService;
use Activity\Service\ActivityType\Factory\ListAllOrByIdServiceFactory as ListAllOrByIdActivityTypeServiceFactory;

use Activity\Service\Vacancies\MergeOrPersistService as MergeOrPersistServiceVacancies;
use Activity\Service\Vacancies\Factory\MergeOrPersistServiceFactory as MergeOrPersistServiceFactoryVacancies;

use Activity\Service\Vacancies\RemoveService as RemoveServiceVacancies;
use Activity\Service\Vacancies\Factory\RemoveServiceFactory as RemoveServiceFactoryVacancies;

use Activity\Service\Vacancies\ListAllOrByIdService as ListAllOrByIdServiceVacancies;
use Activity\Service\Vacancies\Factory\ListAllOrByIdServiceFactory as ListAllOrByIdServiceFactoryVacancies;

use Zend\Mvc\Router\Http\Literal;
use Zend\Mvc\Router\Http\Segment;

return [
    'controllers' => [
        'factories' => [
            ListAllOrById::class => ListAllOrByIdFactory::class,
            MergeOrPersist::class => MergeOrPersistFactory::class,
            Remove::class => RemoveFactory::class
        ]
    ],
    'service_manager' => [
        'factories' => [
            ListAllOrByIdService::class => ListAllOrByIdServiceFactory::class,
            MergeOrPersistService::class => MergeOrPersistServiceFactory::class,
            RemoveService::class => RemoveServiceFactory::class,
            ListAllOrByIdActivityTypeService::class => ListAllOrByIdActivityTypeServiceFactory::class,
            MergeOrPersistServiceVacancies::class => MergeOrPersistServiceFactoryVacancies::class,
            RemoveServiceVacancies::class => RemoveServiceFactoryVacancies::class,
            ListAllOrByIdServiceVacancies::class => ListAllOrByIdServiceFactoryVacancies::class
        ],
    ],
    'router' => [
        'routes' => [
            'atividades' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/atividades[/:idevent]',
                    'constraints' => [
                        'idevent' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}'
                    ],
                    'defaults' => [
                        'controller' => ListAllOrById::class,
                        'action' => 'listAll'
                    ],
                ]
            ],

            'atividade' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/atividade',
                    'defaults' => [
                        'controller' => MergeOrPersist::class
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'adicionar-editar' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/adicionar-editar[/:idevent][/:idactivity]',
                            'constraints' => [
                                'idevent' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}',
                                'idactivity' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}'
                            ],
                            'defaults' => [
                                'action' => 'mergeOrPersist'
                            ]
                        ]
                    ],
                    'remover-atividade' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/remover-atividade',
                            'defaults' => [
                                'controller' => Remove::class,
                                'action' => 'removeActivity'
                            ]
                        ]
                    ],
                ]
            ]
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy'
        ]
    ]
];
