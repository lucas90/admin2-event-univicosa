<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'doctrine' => [
        'driver' => [
            'activity_entities' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Activity/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Activity\Entity' => 'activity_entities'
                ]
            ]
        ]
    ]
];