<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 14:27
 */

namespace Activity\Service\ActivityType;

use Activity\Repository\ActivityType\GetByIdRepository;
use Activity\Repository\ActivityType\ListAllRepository;

class ListAllOrByIdService
{
    private $getById;
    private $listAll;

    public function __construct(GetByIdRepository $getByIdRepository, ListAllRepository $listAllRepository)
    {
        $this->getById = $getByIdRepository;
        $this->listAll = $listAllRepository;
    }

    public function getById($id)
    {
        $repository = $this->getById;

        return $repository($id);
    }

    public function listAll()
    {
        $repository = $this->listAll;

        return $repository();
    }
}