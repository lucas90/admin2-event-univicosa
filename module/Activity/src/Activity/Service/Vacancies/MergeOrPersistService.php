<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 25/08/2016
 * Time: 14:47
 */

namespace Activity\Service\Vacancies;

use Activity\Repository\Vacancies\GetByActivityRepository;
use Activity\Repository\Activity\GetByIdRepository as GetActivityByIdRepository;
use Activity\Entity\Vacancies;

use Doctrine\Common\Persistence\ObjectManager;
use Core\Doctrine\ObjectManagerAbstract;

class MergeOrPersistService extends ObjectManagerAbstract
{
    private $getVacanciesByActivity;
    private $getActivityById;

    public function __construct(
        ObjectManager $objectManager,
        GetByActivityRepository $getByActivityRepository,
        GetActivityByIdRepository $getActivtyByIdRepository
    ){
        parent::__construct($objectManager);
        $this->getVacanciesByActivity = $getByActivityRepository;
        $this->getActivityById = $getActivtyByIdRepository;
    }

    public function __invoke($data){
        $getActivity = $this->getActivityById;
        $activity = $getActivity($data['activityId']);

        $getVacancies = $this->getVacanciesByActivity;
        $vacancies = $getVacancies($activity);

        if($vacancies !== null){
            $vacancies->setParticipant($data['participant']);
            $vacancies->setGuest($data['guest']);

            $this->objectManager->merge($vacancies);
        }

        if($vacancies === null){
            $vacancies = new Vacancies(
                $activity,
                $data['participant'],
                $data['guest']
            );

            $this->objectManager->persist($vacancies);
        }



        $this->objectManager->flush();
    }
}