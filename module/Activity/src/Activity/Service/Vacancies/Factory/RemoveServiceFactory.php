<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 25/08/2016
 * Time: 14:27
 */

namespace Activity\Service\Vacancies\Factory;

use Activity\Service\Vacancies\RemoveService;
use Activity\Repository\Vacancies\GetByActivityRepository;
use Activity\Repository\Activity\GetByIdRepository;
use Activity\Repository\Activity\ListAllRepository;
use Event\Repository\Event\GetByIdRepository as GetByIdRepositoryActivity;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RemoveServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new RemoveService(
            $entityManager,
            new GetByActivityRepository($entityManager),
            new GetByIdRepository($entityManager),
            new ListAllRepository($entityManager),
            new GetByIdRepositoryActivity($entityManager)
        );
    }
}