<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 25/08/2016
 * Time: 14:46
 */

namespace Activity\Service\Vacancies\Factory;

use Activity\Service\Vacancies\MergeOrPersistService;
use Activity\Repository\Vacancies\GetByActivityRepository;
use Activity\Repository\Activity\GetByIdRepository as GetActivityByIdRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MergeOrPersistServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new MergeOrPersistService(
            $entityManager,
            new GetByActivityRepository($entityManager),
            new GetActivityByIdRepository($entityManager)
        );
    }
}