<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 25/08/2016
 * Time: 14:47
 */

namespace Activity\Service\Vacancies\Factory;

use Activity\Service\Vacancies\ListAllOrByIdService;
use Activity\Repository\Vacancies\GetByActivityRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ListAllOrByIdServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new ListAllOrByIdService(
            new GetByActivityRepository($entityManager)
        );
    }
}