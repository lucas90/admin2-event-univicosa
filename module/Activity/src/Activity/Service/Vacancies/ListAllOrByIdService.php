<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 25/08/2016
 * Time: 14:47
 */

namespace Activity\Service\Vacancies;

use Activity\Repository\Vacancies\GetByActivityRepository;


class ListAllOrByIdService
{
    private $getByActivity;

    public function __construct(
        GetByActivityRepository $getByActivityRepository
    ){
        $this->getByActivity = $getByActivityRepository;
    }

    public function getByActivity($activity)
    {
        $repository = $this->getByActivity;

        return $repository($activity);
    }

}