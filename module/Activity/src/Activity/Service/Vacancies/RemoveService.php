<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 25/08/2016
 * Time: 14:28
 */

namespace Activity\Service\Vacancies;

use Activity\Repository\Vacancies\GetByActivityRepository;
use Activity\Repository\Activity\GetByIdRepository;
use Activity\Repository\Activity\ListAllRepository;
use Event\Repository\Event\GetByIdRepository as GetByIdRepositoryActivity;
use Doctrine\Common\Persistence\ObjectManager;

use Core\Doctrine\ObjectManagerAbstract;

class RemoveService extends ObjectManagerAbstract
{
    private $getByActivity;
    private $getActivityById;
    private $listAll;
    private $getEventById;

    public function __construct(
        ObjectManager $objectManager,
        GetByActivityRepository $getByActivityRepository,
        GetByIdRepository $getByIdRepository,
        ListAllRepository $listAllRepository,
        GetByIdRepositoryActivity $getByIdRepositoryEvent
    ){
        parent::__construct($objectManager);

        $this->getByActivity = $getByActivityRepository;
        $this->getActivityById = $getByIdRepository;
        $this->listAll = $listAllRepository;
        $this->getEventById = $getByIdRepositoryEvent;
    }

    public function remove($activityId){
        try{
            $getActivity = $this->getActivityById;
            $activity = $getActivity($activityId);

            $getVacancies = $this->getByActivity;
            $vacancies = $getVacancies($activity);

            if($vacancies !== null){
                $this->objectManager->remove($vacancies);
                $this->objectManager->flush();
            }

        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function removeAll($eventId)
    {
        try{
            $getVacanciesByActivity = $this->getByActivity;

            $getEvent = $this->getEventById;
            $event = $getEvent($eventId);

            $listAllActivities = $this->listAll;
            $activities = $listAllActivities($event);

            foreach($activities as $activity){
                $vacancies = $getVacanciesByActivity($activity);
                $this->objectManager->remove($vacancies);
                $this->objectManager->remove($activity);
                $this->objectManager->flush();
            }

        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }
}