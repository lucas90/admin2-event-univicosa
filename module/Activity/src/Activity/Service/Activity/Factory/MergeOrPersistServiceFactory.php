<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 14:22
 */

namespace Activity\Service\Activity\Factory;

use Activity\Service\Activity\MergeOrPersistService;
use Activity\Repository\ActivityType\GetByIdRepository as GetByIdActivityType;
use Event\Repository\Event\GetByIdRepository as GetEventByIdRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MergeOrPersistServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new MergeOrPersistService(
            $entityManager,
            new GetByIdActivityType($entityManager),
            new GetEventByIdRepository($entityManager)
        );
    }
}