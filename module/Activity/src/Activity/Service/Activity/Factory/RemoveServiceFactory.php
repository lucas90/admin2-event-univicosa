<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 14:23
 */

namespace Activity\Service\Activity\Factory;

use Activity\Service\Activity\RemoveService;
use Activity\Repository\Activity\GetByIdRepository;
use Activity\Repository\Activity\ListAllRepository;
use Event\Repository\Event\GetByIdRepository as GetEventByIdRepository;
use Activity\Repository\Vacancies\GetByActivityRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RemoveServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new RemoveService(
            $entityManager,
            new GetByIdRepository($entityManager),
            new ListAllRepository($entityManager),
            new GetEventByIdRepository($entityManager),
            new GetByActivityRepository($entityManager)
        );
    }
}