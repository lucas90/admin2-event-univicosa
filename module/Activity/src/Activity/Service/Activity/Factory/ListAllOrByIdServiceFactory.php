<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 14:21
 */

namespace Activity\Service\Activity\Factory;

use Activity\Repository\Activity\GetByIdRepository;
use Activity\Repository\Activity\ListAllRepository;

use Activity\Service\Activity\ListAllOrByIdService;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ListAllOrByIdServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new ListAllOrByIdService(
            new GetByIdRepository($entityManager),
            new ListAllRepository($entityManager)
        );
    }
}