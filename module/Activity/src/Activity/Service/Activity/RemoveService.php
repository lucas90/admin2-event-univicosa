<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 14:25
 */

namespace Activity\Service\Activity;

use Activity\Repository\Activity\GetByIdRepository;
use Activity\Repository\Activity\ListAllRepository;
use Event\Repository\Event\GetByIdRepository as GetEventByIdRepository;
use Activity\Repository\Vacancies\GetByActivityRepository;

use Core\Doctrine\ObjectManagerAbstract;
use Doctrine\Common\Persistence\ObjectManager;

class RemoveService extends ObjectManagerAbstract
{
    private $getById;
    private $listAllByEvent;
    private $getEventById;
    private $getVacanciesByActivity;

    public function __construct(
        ObjectManager $objectManager,
        GetByIdRepository $getByIdRepository,
        ListAllRepository $listAllRepository,
        GetEventByIdRepository $getEventByIdRepository,
        GetByActivityRepository $getVacanciesByActivityRepository
    ){
        parent::__construct($objectManager);

        $this->getById = $getByIdRepository;
        $this->listAllByEvent = $listAllRepository;
        $this->getEventById = $getEventByIdRepository;
        $this->getVacanciesByActivity = $getVacanciesByActivityRepository;
    }

    public function removeActivity($id)
    {
        try{
            $getActivityById = $this->getById;
            $activity = $getActivityById($id);

            $this->objectManager->remove($activity);
            $this->objectManager->flush();

            return $activity;
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function removeAllByEvent($eventId)
    {
        $getVacancies = $this->getVacanciesByActivity;

        try{
            $getEvent = $this->getEventById;
            $event = $getEvent($eventId);
            $listAll = $this->listAllByEvent;
            $activities = $listAll($event);

            foreach($activities as $activity) {
                $vacancies = $getVacancies($activity);
                $this->objectManager->remove($vacancies);
                $this->objectManager->remove($activity);
                $this->objectManager->flush();
            }
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

    }
}