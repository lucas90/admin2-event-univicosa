<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 14:24
 */

namespace Activity\Service\Activity;

use Event\Repository\Event\GetByIdRepository as GetEventByIdRepository;
use Activity\Repository\ActivityType\GetByIdRepository as GetByIdActivityType;
use Activity\Entity\Activity;
use Date\DateValidate\DateValidate;

use Core\Doctrine\ObjectManagerAbstract;
use Doctrine\Common\Persistence\ObjectManager;
use Rhumsaa\Uuid\Uuid;

class MergeOrPersistService extends ObjectManagerAbstract
{
    private $getByIdActivityType;
    private $getEventById;

    public function __construct(
        ObjectManager $objectManager,
        GetByIdActivityType $getByIdActivityType,
        GetEventByIdRepository $getEventByIdRepository
    ){
        parent::__construct($objectManager);
        $this->getByIdActivityType = $getByIdActivityType;
        $this->getEventById = $getEventByIdRepository;
    }

    public function __invoke(Activity $activity = null, array $data)
    {
        $getByIdActivityType = $this->getByIdActivityType;
        $getEventById = $this->getEventById;

        try{
            $activityType = $getByIdActivityType($data['activityTypeId']);
            $date = \DateTime::createFromFormat('d/m/Y H:i', $data['date']);
            $event = $getEventById($data['eventId']);

            $dates = ['startEvent' => $event->getDate()->getStartEvent(), 'endEvent' => $event->getDate()->getEndEvent(), 'startActivity' => $date];

            new DateValidate($dates);

            if($activity !== null){
                $activity->setName($data['name']);
                $activity->setDescription($data['description']);
                $activity->setValue($data['value']);
                $activity->setActivityType($activityType);
                $activity->setDate($date);

                $this->objectManager->merge($activity);
            }

            if($activity === null){
                $activity = new Activity(
                    $activityType,
                    $event,
                    Uuid::uuid4(),
                    $data['name'],
                    $data['description'],
                    $data['value'],
                    $date
                );

                $this->objectManager->persist($activity);
            }

            $this->objectManager->flush();

            return $activity;
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

    }
}