<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 14:24
 */

namespace Activity\Service\Activity;

use Activity\Repository\Activity\GetByIdRepository;
use Activity\Repository\Activity\ListAllRepository;

class ListAllOrByIdService
{
    private $getById;
    private $listAll;

    public function __construct(GetByIdRepository $getByIdRepository, ListAllRepository $listAllRepository)
    {
        $this->getById = $getByIdRepository;
        $this->listAll = $listAllRepository;
    }

    public function getById($id)
    {
        $repository = $this->getById;

        return $repository($id);
    }

    public function listAll($id)
    {
        $repository = $this->listAll;

        return $repository($id);
    }
}