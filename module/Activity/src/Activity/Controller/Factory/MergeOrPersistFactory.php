<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 22/07/2016
 * Time: 09:55
 */

namespace Activity\Controller\Factory;

use Activity\Service\Activity\MergeOrPersistService;
use Activity\Service\Activity\ListAllOrByIdService;
use Activity\Controller\MergeOrPersist;
use Activity\Service\ActivityType\ListAllOrByIdService as ListAllOrByIdActivityTypeService;
use Event\Service\Event\ListAllOrByIdService as ListAllOrByIdEventService;
use Activity\Service\Vacancies\MergeOrPersistService as MergeOrPersistServiceVacancies;
use Activity\Service\Vacancies\ListAllOrByIdService as ListAllOrByIdServiceVacancies;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MergeOrPersistFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $realServiceLocator = $serviceLocator->getServiceLocator();
        $mergeOrPersistServiceActivity = $realServiceLocator->get(MergeOrPersistService::class);
        $listAllOrByIdServiceActivity = $realServiceLocator->get(ListAllOrByIdService::class);
        $listAllOrByIdServiceActivityType = $realServiceLocator->get(ListAllOrByIdActivityTypeService::class);
        $listAllOrByIdEventService = $realServiceLocator->get(ListAllOrByIdEventService::class);
        $mergeOrPersistServiceVacancies = $realServiceLocator->get(MergeOrPersistServiceVacancies::class);
        $listAllOrByIdServiceVacancies = $realServiceLocator->get(ListAllOrByIdServiceVacancies::class);

        return new MergeOrPersist(
            $mergeOrPersistServiceActivity,
            $listAllOrByIdServiceActivity,
            $listAllOrByIdServiceActivityType,
            $listAllOrByIdEventService,
            $mergeOrPersistServiceVacancies,
            $listAllOrByIdServiceVacancies
        );
    }
}