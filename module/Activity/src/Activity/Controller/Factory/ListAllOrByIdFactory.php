<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 21/07/2016
 * Time: 16:48
 */

namespace Activity\Controller\Factory;

use Activity\Service\Activity\ListAllOrByIdService;
use Activity\Controller\ListAllOrById;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ListAllOrByIdFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $realServiceLocator  = $serviceLocator->getServiceLocator();
        $listAllOrByIdService = $realServiceLocator->get(ListAllOrByIdService::class);

        return new ListAllOrById($listAllOrByIdService);
    }
}