<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 25/07/2016
 * Time: 16:44
 */

namespace Activity\Controller\Factory;

use Activity\Controller\Remove;
use Activity\Service\Activity\RemoveService;
use Activity\Service\Vacancies\RemoveService as RemoveServiceVacancies;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RemoveFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $realServiceLocator  = $serviceLocator->getServiceLocator();
        $service = $realServiceLocator->get(RemoveService::class);
        $serviceVacancies = $realServiceLocator->get(RemoveServiceVacancies::class);

        return new Remove(
            $service,
            $serviceVacancies
        );
    }
}