<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 21/07/2016
 * Time: 16:48
 */

namespace Activity\Controller;

use Activity\Service\Activity\ListAllOrByIdService;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ListAllOrById extends AbstractActionController
{
    private $service;

    public function __construct(ListAllOrByIdService $listAllOrByIdService)
    {
        $this->service = $listAllOrByIdService;
    }

    public function listAllAction()
    {
        $id = (string) $this->params()->fromRoute('idevent');

        try{
            $listAll = $this->service->listAll($id);

        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

        $viewModel = new ViewModel();
        $viewModel->setTemplate('activity/list-all/activities');
        $viewModel->setVariables([
            'activities' => $listAll,
            'idEvent' => $id
        ]);

        return $viewModel;
    }
}
