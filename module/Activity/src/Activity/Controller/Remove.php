<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 25/07/2016
 * Time: 16:44
 */

namespace Activity\Controller;

use Activity\Service\Activity\RemoveService;
use Activity\Service\Vacancies\RemoveService as RemoveServiceVacancies;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class Remove extends AbstractActionController
{
    private $service;
    private $serviceVacancies;

    public function __construct(
        RemoveService $service,
        RemoveServiceVacancies $serviceVacancies
    ){
        $this->service = $service;
        $this->serviceVacancies = $serviceVacancies;
    }

    public function removeActivityAction()
    {
        $request = $this->getRequest();
        $jsonModel = new JsonModel();

        if ($request->isXmlHttpRequest()){
            try {
                $activityId = (string) $request->getPost('activityId');

                $this->serviceVacancies->remove($activityId);
                $activity = $this->service->removeActivity($activityId);


                $jsonModel->setVariables([
                    'success' => true,
                    'message' => 'Atividade removida com sucesso.',
                    'eventId' => $activity->getEvent()->getEventId()
                ]);
            } catch(\Exception $e){
                $jsonModel->setVariables([
                    'success' => false,
                    'message' => $e->getMessage()
                ]);
            }
        }

        return $jsonModel;
    }
}