<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 22/07/2016
 * Time: 09:55
 */

namespace Activity\Controller;

use Activity\Service\Activity\ListAllOrByIdService;
use Activity\Service\Activity\MergeOrPersistService;
use Activity\Service\ActivityType\ListAllOrByIdService as ListAllOrByIdActivityTypeService;
use Event\Service\Event\ListAllOrByIdService as ListAllOrByIdEventService;
use Activity\Service\Vacancies\MergeOrPersistService as MergeOrPersistServiceVacancies;
use Activity\Service\Vacancies\ListAllOrByIdService as ListAllOrByIdServiceVacancies;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class MergeOrPersist extends AbstractActionController
{

    private $service;
    private $serviceActivityType;
    private $serviceEvent;
    private $mergeOrPersist;
    private $mergeOrPersistVacancies;
    private $serviceVacancies;

    public function __construct(
        MergeOrPersistService $mergeOrPersistService,
        ListAllOrByIdService $listAllOrByIdService,
        ListAllOrByIdActivityTypeService $listAllOrByIdActivityTypeService,
        ListAllOrByIdEventService $serviceEvent,
        MergeOrPersistServiceVacancies $mergeOrPersistServiceVacancies,
        ListAllOrByIdServiceVacancies $listAllOrByIdServiceVacancies
    ){
        $this->service = $listAllOrByIdService;
        $this->mergeOrPersist = $mergeOrPersistService;
        $this->serviceActivityType = $listAllOrByIdActivityTypeService;
        $this->serviceEvent = $serviceEvent;
        $this->mergeOrPersistVacancies = $mergeOrPersistServiceVacancies;
        $this->serviceVacancies = $listAllOrByIdServiceVacancies;
    }

    public function mergeOrPersistAction()
    {
        $eventId = (string) $this->params()->fromRoute('idevent');
        $activityId = (string) $this->params()->fromRoute('idactivity');
        $request = $this->getRequest();
        $activity = null;
        $vacancies = null;
        $persist = true; // flag para identificar persitencia ou edição de vacancies


        $event = $this->serviceEvent->getById($eventId);

        if($activityId !== ""){
            $activity = $this->service->getById($activityId);
            $vacancies = $this->serviceVacancies->getByActivity($activity);
            $persist = false;
        }

        if($request->isXmlHttpRequest()){
            $jsonModel = new JsonModel();

            $data = $request->getPost()->toArray();
            $data['eventId'] = $eventId;
            $data['persist'] = $persist;

            try{

                $mergeOrPersistActivity = $this->mergeOrPersist;
                $activityReturn = $mergeOrPersistActivity($activity, $data);

                $data['activityId'] = (string) $activityReturn->getActivityId();

                $mergeOrPersistVacancies = $this->mergeOrPersistVacancies;
                $mergeOrPersistVacancies($data);

                $jsonModel->setVariables([
                    'success' => true,
                    'message' => $persist ? 'Atividade adicionada' : 'Atividade Editada',
                    'eventId' => $eventId
                ]);
            } catch(\Exception $e){
                $jsonModel->setVariables([
                    'success' => false,
                    'message' => $e->getMessage()
                ]);
            }

            return $jsonModel;
        }


        $activityTypes = $this->serviceActivityType->listAll();
        $viewModel = new ViewModel();
        $viewModel->setTemplate('activity/merge-or-persist/form');
        $viewModel->setVariables([
            'activityTypes' => $activityTypes,
            'activity' => $activity,
            'event' => $event,
            'vacancies' => $vacancies
        ]);

        return $viewModel;
    }
}
