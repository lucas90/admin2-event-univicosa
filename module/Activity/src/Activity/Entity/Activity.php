<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 21/07/2016
 * Time: 14:36
 */

namespace Activity\Entity;

use Activity\Entity\ActivityType;
use Event\Entity\Event;

use Doctrine\ORM\Mapping as ORM;
use Rhumsaa\Uuid\Uuid;

/**
 * Class Activity
 * @package Activity\Entity
 * @ORM\Entity
 * @ORM\Table(name="atividade.atividade")
 */
class Activity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="atividade_id", type="string")
     */
    private $activityId;

    /**
     * @ORM\ManyToOne(targetEntity="Activity\Entity\ActivityType")
     * @ORM\JoinColumn(name="tipo_atividade_id", referencedColumnName="tipo_atividade_id")
     */
    private $activityType;

    /**
     * @ORM\ManyToOne(targetEntity="Event\Entity\Event")
     * @ORM\JoinColumn(name="evento_id", referencedColumnName="evento_id")
     */
    private $event;

    /**
     * @ORM\Column(name="ministrador_id", type="string")
     */
    private $trator;

    /**
     * @ORM\Column(name="nome", type="string")
     */
    private $name;

    /**
     * @ORM\Column(name="descricao", type="string")
     */
    private $description;

    /**
     * @ORM\Column(name="valor_inscricao", type="string")
     */
    private $value;

    /**
     * @ORM\Column(name="data_atividade", type="datetime")
     */
    private $date;

    /**
     * Activity constructor.
     * @param $activityType
     * @param $event
     * @param $trator
     * @param $name
     * @param $description
     * @param $value
     * @param $date
     */
    public function __construct( ActivityType $activityType, Event $event, $trator, $name, $description, $value, $date)
    {
        $this->activityId = Uuid::uuid4();
        $this->activityType = $activityType;
        $this->event = $event;
        $this->trator = $trator;
        $this->name = trim($name);
        $this->description = trim($description);
        $this->value = $value;
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * @return mixed
     */
    public function getActivityType()
    {
        return $this->activityType;
    }

    /**
     * @param mixed $activityType
     */
    public function setActivityType($activityType)
    {
        $this->activityType = $activityType;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param mixed $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     * @return mixed
     */
    public function getTrator()
    {
        return $this->trator;
    }

    /**
     * @param mixed $trator
     */
    public function setTrator($trator)
    {
        $this->trator = $trator;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        $value =  $this->value;
        return $value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }



}