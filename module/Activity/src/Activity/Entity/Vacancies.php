<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 25/08/2016
 * Time: 11:21
 */

namespace Activity\Entity;

use Activity\Entity\Activity;
use Rhumsaa\Uuid\Uuid;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Vacancies
 * @package Activity\Entity
 * @ORM\Entity
 * @ORM\Table(name="atividade.vaga")
 */
class Vacancies
{
    /**
     * @ORM\Id
     * @ORM\Column(name="vaga_id", type="string")
     */
    private $vacanciesId;

    /**
     * @ORM\ManyToOne(targetEntity="Activity\Entity\Activity")
     * @ORM\JoinColumn(name="atividade_id", referencedColumnName="atividade_id")
     */
    private $activity;

    /**
     * @ORM\Column(name="participante", type="integer")
     */
    private $participant;

    /**
     * @ORM\Column(name="convidado", type="integer")
     */
    private $guest;

    /**
     * @param $activity
     * @param $participant
     * @param $guest
     */
    public function __construct(Activity $activity, $participant, $guest)
    {
        $this->vacanciesId = Uuid::uuid4();
        $this->activity = $activity;
        $this->participant = $participant;
        $this->guest = $guest;
    }

    /**
     * @return mixed
     */
    public function getVacanciesId()
    {
        return $this->vacanciesId;
    }

    /**
     * @return mixed
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * @return mixed
     */
    public function getParticipant()
    {
        return $this->participant;
    }

    /**
     * @return mixed
     */
    public function getGuest()
    {
        return $this->guest;
    }

    /**
     * @param mixed $participant
     */
    public function setParticipant($participant)
    {
        $this->participant = $participant;
    }

    /**
     * @param mixed $guest
     */
    public function setGuest($guest)
    {
        $this->guest = $guest;
    }


}