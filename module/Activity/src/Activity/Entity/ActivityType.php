<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 15:16
 */

namespace Activity\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ActivityType
 * @package Activity\Entity
 * @ORM\Entity
 * @ORM\Table(name="atividade.tipo_atividade")
 */
class ActivityType
{
    /**
     * @ORM\Id
     * @ORM\Column(name="tipo_atividade_id", type="string")
     */
    private $activityTypeId;

    /**
     * @ORM\Column(name="tipo", type="string")
     */
    private $type;

    /**
     * @return mixed
     */
    public function getActivityTypeId()
    {
        return $this->activityTypeId;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }


}