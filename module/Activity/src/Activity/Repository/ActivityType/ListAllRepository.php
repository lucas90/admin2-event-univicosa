<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 21/07/2016
 * Time: 14:38
 */

namespace Activity\Repository\ActivityType;

use Activity\Entity\ActivityType;

use Core\Doctrine\ObjectManagerAbstract;

class ListAllRepository extends ObjectManagerAbstract
{
    public function __invoke()
    {
        return $this->objectManager->getRepository(ActivityType::class)->findAll();
    }
}