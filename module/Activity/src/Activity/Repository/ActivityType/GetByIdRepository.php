<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 21/07/2016
 * Time: 14:52
 */

namespace Activity\Repository\ActivityType;

use Activity\Entity\ActivityType;

use Core\Doctrine\ObjectManagerAbstract;

class GetByIdRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(ActivityType::class)->findOneBy(['activityTypeId' => $id]);
    }
}