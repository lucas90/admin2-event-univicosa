<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 25/08/2016
 * Time: 14:23
 */

namespace Activity\Repository\Vacancies;

use Activity\Entity\Vacancies;

use Core\Doctrine\ObjectManagerAbstract;

class GetByActivityRepository extends ObjectManagerAbstract
{
    public function __invoke($activity)
    {
        return $this->objectManager->getRepository(Vacancies::class)->findOneBy(['activity' => $activity]);
    }
}