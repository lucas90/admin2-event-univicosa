<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 14:34
 */

namespace Activity\Repository\Activity;

use Activity\Entity\Activity;

use Core\Doctrine\ObjectManagerAbstract;

class GetByIdRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(Activity::class)->findOneBy(['activityId' => $id]);
    }
}