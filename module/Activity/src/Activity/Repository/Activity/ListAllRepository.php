<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 14:35
 */

namespace Activity\Repository\Activity;

use Activity\Entity\Activity;

use Core\Doctrine\ObjectManagerAbstract;

class ListAllRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(Activity::class)->findBy(['event' => $id]);
    }
}