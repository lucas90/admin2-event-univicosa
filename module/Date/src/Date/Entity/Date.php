<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 04/07/2016
 * Time: 17:19
 */

namespace Date\Entity;

use Doctrine\ORM\Mapping as ORM;
use Rhumsaa\Uuid\Uuid;

/**
 * Class Date
 * @package Date\Entity
 * @ORM\Entity
 * @ORM\Table(name="data")
 */
class Date
{
    /**
     * @ORM\Id
     * @ORM\Column(name="data_id", type="string")
     */
    private $dateId;

    /**
     * @ORM\Column(name="inicio_evento", type="datetime")
     */
    private $startEvent;

    /**
     * @ORM\Column(name="fim_evento", type="datetime")
     */
    private $endEvent;

    /**
     * @ORM\Column(name="inicio_divulgacao", type="datetime")
     */
    private $startDisclosure;

    /**
     * @ORM\Column(name="inicio_inscricao", type="datetime")
     */
    private $startInscription;

    /**
     * @ORM\Column(name="fim_inscricao", type="datetime")
     */
    private $endInscription;

    /**
     * Date constructor.
     * @param $startEvent
     * @param $endEvent
     * @param $startDisclosure
     * @param $startInscription
     * @param $endInscription
     */
    public function __construct($startEvent, $endEvent, $startDisclosure, $startInscription, $endInscription)
    {
        $this->dateId = Uuid::uuid4();
        $this->startEvent = $startEvent;
        $this->endEvent = $endEvent;
        $this->startDisclosure = $startDisclosure;
        $this->startInscription = $startInscription;
        $this->endInscription = $endInscription;
    }

    /**
     * @return mixed
     */
    public function getDateId()
    {
        return $this->dateId;
    }

    /**
     * @return mixed
     */
    public function getStartEvent()
    {
        return $this->startEvent;
    }

    /**
     * @param mixed $startEvent
     */
    public function setStartEvent($startEvent)
    {
        $this->startEvent = $startEvent;
    }

    /**
     * @return mixed
     */
    public function getEndEvent()
    {
        return $this->endEvent;
    }

    /**
     * @param mixed $endEvent
     */
    public function setEndEvent($endEvent)
    {
        $this->endEvent = $endEvent;
    }

    /**
     * @return mixed
     */
    public function getStartDisclosure()
    {
        return $this->startDisclosure;
    }

    /**
     * @param mixed $startDisclosure
     */
    public function setStartDisclosure($startDisclosure)
    {
        $this->startDisclosure = $startDisclosure;
    }

    /**
     * @return mixed
     */
    public function getStartInscription()
    {
        return $this->startInscription;
    }

    /**
     * @param mixed $startInscription
     */
    public function setStartInscription($startInscription)
    {
        $this->startInscription = $startInscription;
    }

    /**
     * @return mixed
     */
    public function getEndInscription()
    {
        return $this->endInscription;
    }

    /**
     * @param mixed $endInscription
     */
    public function setEndInscription($endInscription)
    {
        $this->endInscription = $endInscription;
    }



}