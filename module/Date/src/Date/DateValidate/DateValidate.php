<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 09/08/2016
 * Time: 17:49
 */

namespace Date\DateValidate;


class DateValidate
{
    public function __construct(array $data){

        if($data['startEvent'] > $data['endEvent']){
            throw new \Exception('O campo de data inicial do evento não pode ser maior que a data final.');
        }

        if(isset($data['startActivity'])) {
            if($data['startActivity'] < $data['startEvent'] or $data['startActivity'] > $data['endEvent']){
                throw new \Exception('A data da atividade tem que estar entre os intervalos de início e fim do evento.');
            }

        }

        if(isset($data['startInscription'])){

            if(isset($data['startDisclosure'])){
                if($data['startDisclosure'] > $data['startInscription']){
                    throw new \Exception('A inscrição está sendo aberta antes da divulgação.');
                }
            }

            if($data['startInscription'] !== null or $data['endInscription'] !== null){
                if($data['startInscription'] >= $data['endInscription']){
                    throw new \Exception('Os campos de início e fim das inscrições estão inconsistentes.');
                }

                if($data['endInscription'] > $data['startEvent']){
                    throw new \Exception('As inscrições tem que acontecer antes do inicio do evento.');
                }
            }
        }


    }
}