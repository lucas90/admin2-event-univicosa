<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 05/07/2016
 * Time: 10:54
 */

namespace Date\Repository;

use Core\Doctrine\ObjectManagerAbstract;
use Date\Entity\Date;

use Rhumsaa\Uuid\Uuid;

class GetDateByIdRepository extends ObjectManagerAbstract
{

    /**
     * @param $id
     * @return Date
     */
    public function __invoke($id)
    {
        if (! Uuid::isValid($id)) {
            //disparo de exceção
        }

        return $this->objectManager->getRepository(Date::class)->find($id);
    }
}