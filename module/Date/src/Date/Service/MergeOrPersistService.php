<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 05/07/2016
 * Time: 11:00
 */

namespace Date\Service;

use Date\Repository\GetDateByIdRepository;
use Date\Entity\Date;


class MergeOrPersistService
{
    private $getDateById;

    public function __construct(GetDateByIdRepository $getDateByIdRepository)
    {
        $this->getDateById = $getDateByIdRepository;
    }

    public function __invoke(array $dataDate)
    {
        if(! $dataDate['dateId']){
            return new Date(
                $dataDate['startEvent'],
                $dataDate['endEvent'],
                $dataDate['startInscription'] ?? null,
                $dataDate['endInscription'] ?? null
            );
        }

        $repository = $this->getDateById;

        return $repository($dataDate['dateId']);
    }
}