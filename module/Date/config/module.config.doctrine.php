<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'doctrine' => [
        'driver' => [
            'date_entities' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Date/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Date\Entity' => 'date_entities'
                ]
            ]
        ]
    ]
];