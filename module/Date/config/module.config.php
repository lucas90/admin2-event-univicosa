<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */

use Date\Service\MergeOrPersistService;
use Date\Service\Factory\MergeOrPersistServiceFactory;

return [
    'service_manager' => [
        'factories' => [
            MergeOrPersistService::class => MergeOrPersistServiceFactory::class
        ]
    ]
];
