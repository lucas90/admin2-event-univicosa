<?php

namespace OAuth;

use Zend\Http\Client;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
final class ClientConfig
{
    const TIMEOUT = 30;

    /**
     * @var \Zend\Http\Client $client
     */
    private $client;

    /**
     * @param string $token
     * @param string $path
     */
    public function __construct($token, $path)
    {
        $this->client = new Client;
        $this->client->setUri($path);
        $this->client->setOptions(['timeout' => static::TIMEOUT]);
        $this->client->setHeaders([
            'Authorization' => 'Bearer ' .$token
        ]);
    }

    /**
     * @return \Zend\Http\Response
     */
    public function send()
    {
        return $this->client->send();
    }
}