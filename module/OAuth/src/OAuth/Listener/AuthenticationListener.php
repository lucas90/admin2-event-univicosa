<?php

namespace OAuth\Listener;

use OAuth\Authorization\ValidateAuthentication;
use OAuth\Service\GetUserService;
use OAuth\Service\ListPermissionsByUserService;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\MvcEvent;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class AuthenticationListener implements ListenerAggregateInterface
{
    /**
     * @var GetUserService $getUserService
     */
    private $getUserService;

    /**
     * @var ListPermissionsByUserService $listPermissionsByUserService
     */
    private $listPermissionsByUserService;

    /**
     * @var \[]
     */
    protected $listeners = [];

    /**
     * @param GetUserService               $getUserService
     * @param ListPermissionsByUserService $listPermissionsByUserService
     */
    public function __construct(GetUserService $getUserService, ListPermissionsByUserService $listPermissionsByUserService)
    {
        $this->getUserService = $getUserService;
        $this->listPermissionsByUserService = $listPermissionsByUserService;
    }

    /**
     * @param EventManagerInterface $events
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH, [$this, 'authorized'], 1000);
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH, [$this, 'withoutAuthorization'], 1000);
    }

    /**
     * @param EventManagerInterface $events
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    /**
     * @param MvcEvent $event
     *
     * @return ValidateAuthentication
     */
    private function validateAuthentication(MvcEvent $event)
    {
        $request = $event->getRequest();
        $tokenFromQuery = $request->getQuery('t');
        $cookie = $request->getCookie();
        $token = isset($cookie['token']) || ! empty($cookie['token']) ? $cookie['token'] : false;
        $config = $event->getApplication()->getServiceManager()->get('Config')['auth_config'];

        return new ValidateAuthentication(
            $event,
            $config,
            $token,
            $tokenFromQuery,
            $this->getUserService,
            $this->listPermissionsByUserService
        );
    }

    /**
     * @param MvcEvent $event
     */
    public function authorized(MvcEvent $event)
    {
        $this->validateAuthentication($event)->authorized();
    }

    /**
     * @param MvcEvent $event
     */
    public function withoutAuthorization(MvcEvent $event)
    {
        $this->validateAuthentication($event)->withoutAuthorization();
    }
}