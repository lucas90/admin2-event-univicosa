<?php

namespace OAuth\Event;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
trait RedirectToTrait
{
    /**
     * @param MvcEvent $event
     * @param string   $toUrl
     */
    public function redirect(MvcEvent $event, $toUrl)
    {
        $sharedManager = $event->getApplication()->getEventManager()->getSharedManager();
        $sharedManager->attach(AbstractActionController::class, MvcEvent::EVENT_DISPATCH, function($e) use($toUrl) {
            $controller = $e->getTarget();
            $controller->plugin('redirect')->toUrl($toUrl);

            $e->stopPropagation();
            return false;
        }, 100);
    }
}