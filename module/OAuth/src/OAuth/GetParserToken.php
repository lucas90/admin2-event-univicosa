<?php

namespace OAuth;

use Lcobucci\JWT\Parser;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
final class GetParserToken
{
    /**
     * @var \Lcobucci\JWT\Token $parser
     */
    private $parser;

    /**
     * @param string $token
     */
    public function __construct($token)
    {
        $this->parser = (new Parser())->parse((string) $token);
    }

    /**
     * @return string
     */
    public function getJti()
    {
        return $this->parser->getClaim('jti');
    }

    /**
     * @return mixed
     */
    public function getExp()
    {
        return $this->parser->getClaim('exp');
    }
}