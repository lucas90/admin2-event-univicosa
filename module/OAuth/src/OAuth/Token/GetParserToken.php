<?php
namespace OAuth\Token;

use Lcobucci\JWT\Parser;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
final class GetParserToken
{
    /**
     * @var \Lcobucci\JWT\Token $parser
     */
    private $parser;

    /**
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->parser = (new Parser())->parse($token);
    }

    /**
     * @return string
     */
    public function getJti() : string
    {
        return $this->parser->getClaim('jti');
    }

    /**
     * @return int
     */
    public function getExp() : int
    {
        return $this->parser->getClaim('exp');
    }
}
