<?php

namespace OAuth;

use Zend\Http\Header\SetCookie;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
final class CookieConfig
{
    /**
     * @var string $token
     */
    private $token;

    /**
     * @var integer $expiration
     */
    private $expiration;

    /**
     * @var boolean $isHttpOnly
     */
    private $isHttpOnly;

    /**
     * @var boolean $isSecure
     */
    private $isSecure;

    /**
     * @param string  $token
     * @param integer $expiration
     * @param boolean $isHttpOnly
     * @param boolean $isSecure
     */
    public function __construct(
        $token,
        $expiration,
        $isHttpOnly = true,
        $isSecure = true
    ) {
        $this->token = (string) $token;
        $this->expiration = (int) $expiration;
        $this->isHttpOnly = (bool) $isHttpOnly;
        $this->isSecure = (bool) $isSecure;
    }

    /**
     * @return \Zend\Http\Header\SetCookie
     */
    public function __invoke()
    {
        return (new SetCookie())
            ->setName('token')
            ->setValue($this->token)
            ->setPath('/')
            ->setExpires($this->expiration)
            ->setSecure($this->isSecure)
            ->setHttponly($this->isHttpOnly);
    }
}