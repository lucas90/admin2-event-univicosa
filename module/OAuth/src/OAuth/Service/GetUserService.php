<?php

namespace OAuth\Service;

use OAuth\Repository\GetUserByIdRepository;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class GetUserService
{
    /**
     * @var GetUserByIdRepository $getUserByIdRepository
     */
    private $getUserByIdRepository;

    /**
     * @param GetUserByIdRepository $getUserByIdRepository
     */
    public function __construct(GetUserByIdRepository $getUserByIdRepository)
    {
        $this->getUserByIdRepository = $getUserByIdRepository;
    }

    /**
     * @param string $id
     *
     * @return \OAuth\Entity\User\User
     */
    public function getById($id)
    {
        $repository = $this->getUserByIdRepository;

        return $repository($id);
    }
}