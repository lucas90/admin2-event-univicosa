<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 13/09/2016
 * Time: 14:10
 */

namespace OAuth\Service;

use OAuth\Repository\GetSystemBySystemRepository;

class GetSystemService
{
    private $getSystemBySystem;

    public function __construct(
        GetSystemBySystemRepository $getSystemBySystemRepository
    ){
        $this->getSystemBySystem = $getSystemBySystemRepository;
    }

    public function getSystemBySystem ($systemName)
    {
        $repository = $this->getSystemBySystem;

        return $repository($systemName);
    }
}