<?php

namespace OAuth\Service\Factory;

use OAuth\Repository\GetPermissionByUserAndSystemRepository;
use OAuth\Repository\FindAllPermissionsByUserRepository;
use OAuth\Service\ListPermissionsByUserService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class ListPermissionsByUserServiceFactory implements FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_auth');

        return new ListPermissionsByUserService(
            new FindAllPermissionsByUserRepository($entityManager),
            new GetPermissionByUserAndSystemRepository($entityManager)
        );
    }
}