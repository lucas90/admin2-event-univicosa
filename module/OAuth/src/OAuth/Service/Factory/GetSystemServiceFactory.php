<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 13/09/2016
 * Time: 14:10
 */

namespace OAuth\Service\Factory;

use OAuth\Service\GetSystemService;
use OAuth\Repository\GetSystemBySystemRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class GetSystemServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_auth');

        return new GetSystemService(
            new GetSystemBySystemRepository($entityManager)
        );
    }
}