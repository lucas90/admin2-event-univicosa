<?php

namespace OAuth\Service\Factory;

use OAuth\Repository\GetUserByIdRepository;
use OAuth\Service\GetUserService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class GetUserServiceFactory implements FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_auth');

        return new GetUserService(
            new GetUserByIdRepository($entityManager)
        );
    }
}