<?php

namespace OAuth\Service;

use OAuth\Entity\User\User;
use OAuth\Repository\FindAllPermissionsByUserRepository;
use OAuth\Repository\GetPermissionByUserAndSystemRepository;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class ListPermissionsByUserService
{
    /**
     * @var FindAllPermissionsByUserRepository $repository
     */
    private $repository;

    private $getPermissionByUserAndSystem;

    /**
     * @param FindAllPermissionsByUserRepository $repository
     * @param GetPermissionByUserAndSystemRepository $getPermissionByUserAndSystemRepository
     */
    public function __construct(
        FindAllPermissionsByUserRepository $repository,
        GetPermissionByUserAndSystemRepository $getPermissionByUserAndSystemRepository
    )
    {
        $this->repository = $repository;
    }

    /**
     * @param User $user
     *
     * @return \OAuth\Entity\Permission
     */
    public function findAll(User $user)
    {
        $repository = $this->repository;

        return $repository($user);
    }

    public function GetPermissionByUserAndSystem($user, $system)
    {
        $repository = $this->getPermissionByUserAndSystem;

        return $repository($user, $system);
    }
}