<?php

namespace OAuth\Authorization;

use OAuth\Event\RedirectToTrait;
use OAuth\Service\ListPermissionsByUserService;
use Zend\Mvc\MvcEvent;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class VerifyPermissions
{
    use RedirectToTrait;

    /**
     * @var ListPermissionsByUserService
     */
    private $permission;

    /**
     * @var \[] $config
     */
    private $config;

    /**
     * @param \[]|\OAuth\Entity\Permission $permission
     * @param \[] $config
     */
    public function __construct($permission, $config)
    {
        $this->permission = $permission;
        $this->config = $config;
    }

    /**
     * @param MvcEvent $event
     */
    public function __invoke(MvcEvent $event)
    {
        $domains = [];
        foreach ($this->permission as $permission) {
            $domains[] = $permission->getSystem()->getDomain();
        }

        if (! in_array($this->config['url_domain'], $domains)) {
            /** @method RedirectToTrait */
            $this->redirect($event, $this->config['auth_domain']);
        }
    }
}