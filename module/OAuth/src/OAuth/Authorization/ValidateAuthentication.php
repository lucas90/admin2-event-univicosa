<?php

namespace OAuth\Authorization;

use OAuth\Event\RedirectToTrait;
use OAuth\Service\GetUserService;
use OAuth\Service\ListPermissionsByUserService;
use OAuth\ClientConfig;
use OAuth\GetParserToken;
use OAuth\CookieConfig;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class ValidateAuthentication
{
    use RedirectToTrait;

    /**
     * @var MvcEvent $event
     */
    private $event;

    /**
     * @var \[] $config
     */
    private $config;

    /**
     * @var string $token
     */
    private $token;

    /**
     * @var string $tokenFromQuery `token de parâmetro na url (caso exista)`
     */
    private $tokenFromQuery;

    /**
     * @var GetUserService $getUserService
     */
    private $getUserService;

    /**
     * @var ListPermissionsByUserService $listPermissionsByUserService
     */
    private $listPermissionsService;

    /**
     * @param MvcEvent                     $event
     * @param \[]                          $config
     * @param string                       $token
     * @param string                       $tokenFromQuery
     * @param GetUserService               $getUserService
     * @param ListPermissionsByUserService $listPermissionsService
     */
    public function __construct(
        MvcEvent $event,
        array $config,
        $token,
        $tokenFromQuery,
        GetUserService $getUserService,
        ListPermissionsByUserService $listPermissionsService
    ) {
        $this->event = $event;
        $this->config = $config;
        $this->token = $token;
        $this->tokenFromQuery = $tokenFromQuery;
        $this->getUserService = $getUserService;
        $this->listPermissionsService = $listPermissionsService;
    }

    /**
     * Verifica se o usuário é autorizado a continuar navegando no sistema
     */
    public function authorized()
    {
        $config = $this->config;
        $event = $this->event;

        if ($this->tokenFromQuery && false === $this->token) {
            $expiration = (new GetParserToken($this->tokenFromQuery))->getExp();

            $cookie = new CookieConfig(
                $this->tokenFromQuery,
                $expiration,
                $config['isHttpOnly'],
                $config['isSecure']
            );

            $event->getResponse()->getHeaders()->addHeader($cookie());

            /** @method RedirectToTrait */
            $this->redirect($event, $config['url_domain']);
        }

        if (false !== $this->token) {
            $response = (new ClientConfig($this->token, $config['url_domain']))->send();

            if ($response->isClientError()) {
                $event->getResponse()->setStatusCode(Response::STATUS_CODE_401);

                /** @method RedirectToTrait */
                $this->redirect($event, $config['url_auth']);
            }

            if ($this->tokenFromQuery) {
                /** @method RedirectToTrait */
                $this->redirect($event, $config['url_domain']);
            }

            $userId = (new GetParserToken($this->token))->getJti();
            $user = $this->getUserService->getById($userId);
            $permissions = $this->listPermissionsService->findAll($user);

            $verifyPermissions = new VerifyPermissions($permissions, $config);
            $verifyPermissions($this->event);

            $this->event->getViewModel()->setVariables([
                'user' => $user,
                'permissions' => $permissions
            ]);
        }
    }

    /**
     * Se o token for inválido, será redirecionado para tela de autenticação
     */
    public function withoutAuthorization()
    {
        $config = $this->config;

        if (false === $this->token) {
            $this->event->getResponse()->setStatusCode(Response::STATUS_CODE_401);

            if ($this->event->getRequest()->isXmlHttpRequest()) {
                return (new JsonModel)->setVariable('success', false);
            }

            /** @method RedirectToTrait */
            $this->redirect($this->event, $config['url_auth']);
        }
    }
}