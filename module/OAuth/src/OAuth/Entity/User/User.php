<?php

namespace OAuth\Entity\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="public.user")
 */
class User
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="user_id", type="string")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}