<?php

namespace OAuth\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="public.permission")
 */
class Permission
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="permission_id", type="string")
     */
    private $id;

    /**
     * @var \OAuth\Entity\User\User
     * @ORM\ManyToOne(targetEntity="OAuth\Entity\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    private $user;

    /**
     * @var \OAuth\Entity\System
     * @ORM\ManyToOne(targetEntity="OAuth\Entity\System")
     * @ORM\JoinColumn(name="system_id", referencedColumnName="system_id")
     */
    private $system;

    /**
     * @var \DateTime
     * @ORM\Column(name="registration_date", type="datetime")
     */
    private $registeredDate;

    /**
     * @ORM\Column(name="admin", type="boolean")
     */
    private $admin;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \OAuth\Entity\User\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return \OAuth\Entity\System
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredDate()
    {
        return $this->registeredDate;
    }

    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->admin;
    }


}