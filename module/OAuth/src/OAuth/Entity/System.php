<?php

namespace OAuth\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="public.system")
 */
class System
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="system_id", type="string")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="system", type="string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="initials", type="string")
     */
    private $initials;

    /**
     * @var string
     * @ORM\Column(name="domain", type="string")
     */
    private $domain;

    /**
     * @var string
     * @ORM\Column(name="icon", type="string")
     */
    private $icon;

    public function __construct()
    {
        $this->modules = new ArrayCollection;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getInitials()
    {
        return $this->initials;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }
}