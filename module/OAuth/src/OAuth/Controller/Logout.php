<?php

namespace OAuth\Controller;

use OAuth\ClientConfig;
use OAuth\CookieConfig;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class Logout extends AbstractActionController
{
    /**
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function onDispatchAction()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $cookie = $request->getCookie();
        $config = $this->getAuthConfig();

        $token = isset($cookie['token']) || ! empty($cookie['token']) ? $cookie['token'] : false;

        if (false === $token) {
            $response->setStatusCode(Response::STATUS_CODE_200);
            $this->redirect()->toUrl($config['url_domain']);
        }

        $clientResponse = (new ClientConfig($cookie['token'], $config['url_domain']))->send();

        if ($clientResponse->isOk()) {
            $cookie = new CookieConfig(null, 0);

            $response->getHeaders()->addHeader($cookie());
            $response->setStatusCode(Response::STATUS_CODE_200);

            $this->redirect()->toUrl($config['url_auth']);
        }

        $this->layout()->setTerminal(true);
        return $response;
    }

    /**
     * @return \[]
     */
    private function getAuthConfig()
    {
        return $this->getServiceLocator()->get('Config')['auth_config'];
    }
}