<?php

namespace OAuth\Repository;

use OAuth\Entity\User\User;
use Core\Doctrine\ObjectManagerAbstract;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class GetUserByIdRepository extends ObjectManagerAbstract
{
    /**
     * @param string $id
     */
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(User::class)->findOneById($id);
    }
}