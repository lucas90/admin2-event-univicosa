<?php

namespace OAuth\Repository;

use OAuth\Entity\User\User;
use OAuth\Entity\Permission;
use Core\Doctrine\ObjectManagerAbstract;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class FindAllPermissionsByUserRepository extends ObjectManagerAbstract
{
    /**
     * @param User $user
     *
     * @return Permission
     */
    public function __invoke(User $user)
    {
        return $this->objectManager->getRepository(Permission::class)->findByUser($user);
    }
}