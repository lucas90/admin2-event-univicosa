<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 13/09/2016
 * Time: 14:04
 */

namespace OAuth\Repository;

use OAuth\Entity\System;

use Core\Doctrine\ObjectManagerAbstract;

class GetSystemBySystemRepository extends ObjectManagerAbstract
{
    public function __invoke($name)
    {
        return $this->objectManager->getRepository(System::class)->findOneBy(
            ['name' => $name]
        );
    }
}