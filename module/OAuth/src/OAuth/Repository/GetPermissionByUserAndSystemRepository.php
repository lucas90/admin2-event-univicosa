<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 13/09/2016
 * Time: 14:33
 */

namespace OAuth\Repository;

use OAuth\Entity\Permission;
use Core\Doctrine\ObjectManagerAbstract;

class GetPermissionByUserAndSystemRepository extends ObjectManagerAbstract
{
    public function __invoke($user, $system)
    {
        return $this->objectManager->getRepository(Permission::class)->findOneBy(
            [
                'user' => $user,
                'system' => $system
            ]
        );
    }
}