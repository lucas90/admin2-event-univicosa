<?php

namespace OAuth;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * @author  Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class Module implements ConfigProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfig()
    {
        return array_merge_recursive(
            require __DIR__ . '/config/module.config.auth.php',
            require __DIR__ . '/config/module.config.php',
            require __DIR__ . '/config/module.config.doctrine.php'
        );
    }
}