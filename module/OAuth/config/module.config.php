<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use OAuth\Controller\Logout,
    OAuth\Controller\Factory\LogoutFactory,
    OAuth\Service\GetUserService,
    OAuth\Service\Factory\GetUserServiceFactory,
    OAuth\Service\ListPermissionsByUserService,
    OAuth\Service\Factory\ListPermissionsByUserServiceFactory,
    OAuth\Service\Factory\GetSystemServiceFactory,
    OAuth\Service\GetSystemService;


use Zend\Mvc\Router\Http\Literal;

return [
    'service_manager' => [
        'factories' => [
            GetUserService::class => GetUserServiceFactory::class,
            ListPermissionsByUserService::class => ListPermissionsByUserServiceFactory::class,
            GetSystemService::class => GetSystemServiceFactory::class
        ]
    ],
    'controllers' => [
        'factories' => [
            Logout::class => LogoutFactory::class
        ]
    ],
    'router' => [
        'routes' => [
            'logout' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/logout',
                    'defaults' => [
                        'controller' => Logout::class,
                        'action' => 'onDispatch'
                    ]
                ],
            ]
        ]
    ],
];
