<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'doctrine' => [
        'driver' => [
            'application_entities' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/OAuth/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'OAuth\Entity' => 'application_entities'
                ]
            ]
        ]
    ]
];
