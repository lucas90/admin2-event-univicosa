<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

$config = [
    'auth_config' => [
        'isHttpOnly' => true,
        'isSecure' => true,
        'url_domain' => 'https://admin.univicosa.com.br/evento_new',
        'url_auth' => 'https://auth.univicosa.com.br/?continue='
    ]
];

$config['auth_config']['url_auth'] = $config['auth_config']['url_auth'] . $config['auth_config']['url_domain'];

return $config;