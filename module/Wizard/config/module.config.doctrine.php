<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'doctrine' => [
        'driver' => [
            'wizard_entities' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Wizard/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Wizard\Entity' => 'wizard_entities'
                ]
            ]
        ]
    ]
];