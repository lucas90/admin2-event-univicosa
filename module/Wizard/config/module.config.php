<?php

use Wizard\Service\Wizard\Factory\ListAllOrByIdServiceFactory as ListAllOrByIdServiceFactoryWizard;
use Wizard\Service\Wizard\ListAllOrByIdService as ListAllOrByIdServiceWizard;

use Wizard\Service\WizardEvent\Factory\ListAllOrByIdServiceFactory as ListAllOrByIdServiceFactoryWizardEvent;
use Wizard\Service\WizardEvent\Factory\MergeOrPersistServiceFactory as MergeOrPersistServiceFactoryWizardEvent;

use Wizard\Service\WizardEvent\ListAllOrByIdService as ListAllOrByIdServiceWizardEvent;
use Wizard\Service\WizardEvent\MergeOrPersistService as MergeOrPersistServiceWizardEvent;

use Wizard\Service\WizardEvent\RemoveService;
use Wizard\Service\WizardEvent\Factory\RemoveServiceFactory;

return [
    'service_manager' => [
        'factories' => [
            ListAllOrByIdServiceWizard::class => ListAllOrByIdServiceFactoryWizard::class,
            ListAllOrByIdServiceWizardEvent::class => ListAllOrByIdServiceFactoryWizardEvent::class,
            MergeOrPersistServiceWizardEvent::class => MergeOrPersistServiceFactoryWizardEvent::class,
            RemoveService::class => RemoveServiceFactory::class
        ]
    ],
];
