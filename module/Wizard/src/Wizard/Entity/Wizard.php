<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 11:07
 */

namespace Wizard\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Wizard
 * @package Wizard\Entity
 * @ORM\Entity
 * @ORM\Table(name="wizard")
 */
class Wizard
{
    /**
     * @ORM\Id
     * @ORM\Column(name="wizard_id", type="string")
     */
    private $wizardId;

    /**
     * @ORM\ManyToOne(targetEntity="Modules\Entity\Module")
     * @ORM\JoinColumn(name="modulo_id", referencedColumnName="modulo_id")
     */
    private $module;

    /**
     * @ORM\Column(name="pergunta", type="string")
     */
    private $question;

    /**
     * @ORM\Column(name="ordem", type="integer")
     */
    private $order;

    /**
     * @ORM\Column(name="ativo", type="boolean")
     */
    private $active;

    /**
     * @return mixed
     */
    public function getWizardId()
    {
        return $this->wizardId;
    }

    /**
     * @return mixed
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }



}