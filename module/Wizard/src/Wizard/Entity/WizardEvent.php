<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 11:07
 */

namespace Wizard\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class WizardEvent
 * @package Wizard\Entity
 * @ORM\Entity
 * @ORM\Table(name="wizard_evento")
 */
class WizardEvent
{
    /**
     * @ORM\Column(name="evento_id", type="string")
     */
    private $eventId;

    /**
     * @ORM\Id
     * @ORM\Column(name="wizard_id", type="string")
     */
    private $wizardId;

    /**
     * @ORM\Column(name="resposta", type="string")
     */
    private $answer;

    /**
     * @param $wizardId
     * @param $eventId
     * @param $answer
     */
    public function __construct($wizardId, $eventId, $answer)
    {
        $this->wizardId = $wizardId;
        $this->eventId = $eventId;
        $this->answer = $answer;
    }

    /**
     * @return mixed
     */
    public function getWizardId()
    {
        return $this->wizardId;
    }

    /**
     * @return mixed
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @return mixed
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param mixed $answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }


}