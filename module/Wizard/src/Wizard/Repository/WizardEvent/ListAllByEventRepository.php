<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 17/08/2016
 * Time: 14:46
 */

namespace Wizard\Repository\WizardEvent;

use Core\Doctrine\ObjectManagerAbstract;
use Wizard\Entity\WizardEvent;



class ListAllByEventRepository extends ObjectManagerAbstract
{
    public function __invoke($eventId)
    {
        /*$qb = $this->entityManager->createQueryBuilder();
        $qb
            ->select('we')
            ->from(WizardEvent::class, 'we')
            ->innerJoin('we.wizard', 'w' )
            ->where($qb->expr()->eq('we.eventId', ':eventId'))
            ->orderBy('w.order', 'ASC')
            ->setParameter('eventId', $eventId)
        ;

        $wizardEvents = $qb->getQuery()->getResult();

        return $wizardEvents;*/

        return $this->objectManager->getRepository(WizardEvent::class)->findBy(
            ['eventId' => $eventId]
        );
    }
}