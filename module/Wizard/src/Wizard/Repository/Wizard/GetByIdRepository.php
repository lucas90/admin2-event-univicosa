<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 19/08/2016
 * Time: 14:17
 */

namespace Wizard\Repository\Wizard;

use Wizard\Entity\Wizard;

use Core\Doctrine\ObjectManagerAbstract;

class GetByIdRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(Wizard::class)->findOneBy(['wizardId' => $id]);
    }
}