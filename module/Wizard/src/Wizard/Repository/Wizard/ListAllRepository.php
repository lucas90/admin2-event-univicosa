<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 17/08/2016
 * Time: 14:44
 */

namespace Wizard\Repository\Wizard;

use Wizard\Entity\Wizard;
use Core\Doctrine\ObjectManagerAbstract;

class ListAllRepository extends ObjectManagerAbstract
{
    public function __invoke()
    {
        return $this->objectManager->getRepository(Wizard::class)->findBy(
            ['active' => 't'],
            ['order' => 'ASC']
        );
    }
}