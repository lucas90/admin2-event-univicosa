<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 23/08/2016
 * Time: 11:19
 */

namespace Wizard\Repository\Wizard;

use Wizard\Entity\Wizard;

use Core\Doctrine\ObjectManagerAbstract;

class GetByModuleRepository extends ObjectManagerAbstract
{
    public function __invoke($module)
    {
        return $this->objectManager->getRepository(Wizard::class)->findOneBy(
            ['module' => $module]
        );
    }
}