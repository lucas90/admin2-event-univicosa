<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 17/08/2016
 * Time: 15:16
 */

namespace Wizard\Service\Wizard;

use Wizard\Repository\Wizard\ListAllRepository;
use Wizard\Repository\Wizard\GetByIdRepository;
use Wizard\Repository\Wizard\GetByModuleRepository;

class ListAllOrByIdService
{
    private $listAll;
    private $getById;
    private $getByModule;

    public function __construct(
        ListAllRepository $listAllRepository,
        GetByIdRepository $getByIdRepository,
        GetByModuleRepository $getByModuleRepository
    ){
        $this->listAll = $listAllRepository;
        $this->getById = $getByIdRepository;
        $this->getByModule = $getByModuleRepository;
    }

    public function listAll()
    {
        $repository = $this->listAll;

        return $repository();
    }

    public function getById($id)
    {
        $repository = $this->getById;

        return $repository($id);
    }

    public function getByModule($module)
    {
        $repository = $this->getByModule;

        return $repository($module);
    }
}