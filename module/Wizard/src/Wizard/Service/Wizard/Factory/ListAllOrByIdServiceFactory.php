<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 17/08/2016
 * Time: 15:15
 */

namespace Wizard\Service\Wizard\Factory;

use Wizard\Service\Wizard\ListAllOrByIdService;
use Wizard\Repository\Wizard\ListAllRepository;
use Wizard\Repository\Wizard\GetByIdRepository;
use Wizard\Repository\Wizard\GetByModuleRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ListAllOrByIdServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new ListAllOrByIdService(
            new ListAllRepository($entityManager),
            new GetByIdRepository($entityManager),
            new GetByModuleRepository($entityManager)
        );
    }
}