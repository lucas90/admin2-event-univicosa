<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 17/08/2016
 * Time: 15:17
 */

namespace Wizard\Service\WizardEvent;

use Wizard\Repository\Wizard\GetByIdRepository as GetWizardByIdRepository;
use Event\Repository\Event\GetByIdRepository as GetEventByIdRepository;

use Wizard\Entity\WizardEvent;

use Doctrine\Common\Persistence\ObjectManager;
use Core\Doctrine\ObjectManagerAbstract;

class MergeOrPersistService extends ObjectManagerAbstract
{
    private $getWizardByIdRepository;
    private $getEventByIdRepository;

    public function __construct(
        ObjectManager $objectManager,
        GetWizardByIdRepository $getWizardByIdRepository,
        GetEventByIdRepository $getEventByIdRepository
    ){
        parent::__construct($objectManager);
        $this->getWizardByIdRepository = $getWizardByIdRepository;
        $this->getEventByIdRepository = $getEventByIdRepository;
    }

    public function __invoke(array $data)
    {
        try{

            $eventId = $data['eventId'];

            unset($data['eventId']);

            foreach($data as $wizardId){
                $wizardEvent = new WizardEvent(
                    $wizardId,
                    $eventId,
                    ''
                );

                $this->objectManager->persist($wizardEvent);
            }

            $this->objectManager->flush();
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

        return null;
    }
}