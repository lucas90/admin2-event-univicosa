<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 11:41
 */

namespace Wizard\Service\WizardEvent;

use Wizard\Repository\WizardEvent\ListAllByEventRepository;

class ListAllOrByIdService
{
    private $listAllByEvent;

    public function __construct(
        ListAllByEventRepository $listAllByEventRepository
    ){
        $this->listAllByEvent = $listAllByEventRepository;
    }

    public function listAllByEvent($event)
    {
        $repository = $this->listAllByEvent;

        return $repository($event);
    }
}