<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 17/08/2016
 * Time: 15:17
 */

namespace Wizard\Service\WizardEvent\Factory;

use Wizard\Service\WizardEvent\MergeOrPersistService;

use Wizard\Repository\Wizard\GetByIdRepository as GetWizardByIdRepository;
use Event\Repository\Event\GetByIdRepository as GetEventBiIdRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MergeOrPersistServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new MergeOrPersistService(
            $entityManager,
            new GetWizardByIdRepository($entityManager),
            new GetEventBiIdRepository($entityManager)
        );
    }
}