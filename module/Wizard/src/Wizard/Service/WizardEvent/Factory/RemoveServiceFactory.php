<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 24/08/2016
 * Time: 15:12
 */

namespace Wizard\Service\WizardEvent\Factory;

use Wizard\Service\WizardEvent\RemoveService;
use Wizard\Repository\WizardEvent\ListAllByEventRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RemoveServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new RemoveService(
            $entityManager,
            new ListAllByEventRepository($entityManager)
        );
    }
}