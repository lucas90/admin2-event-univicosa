<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 24/08/2016
 * Time: 15:12
 */

namespace Wizard\Service\WizardEvent;

use Wizard\Repository\WizardEvent\ListAllByEventRepository;

use Doctrine\Common\Persistence\ObjectManager;
use Core\Doctrine\ObjectManagerAbstract;

class RemoveService extends ObjectManagerAbstract
{
    private $listAllByEvent;

    public function __construct(
        ObjectManager $objectManager,
        ListAllByEventRepository $listAllByEventRepository
    ){
        parent::__construct($objectManager);
        $this->listAllByEvent = $listAllByEventRepository;
    }

    public function removeAllByEvent($eventId)
    {
        try{
            $listAll = $this->listAllByEvent;
            $modules = $listAll($eventId);

            foreach($modules as $module){
                $this->objectManager->remove($module);
                $this->objectManager->flush();
            }
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

    }
}