<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 10:59
 */

namespace Wizard;


use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{

    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return array_merge_recursive(
            require __DIR__ . '/config/module.config.php',
            require __DIR__ . '/config/module.config.doctrine.php'
        );
    }
}