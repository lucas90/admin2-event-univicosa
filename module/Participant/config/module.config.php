<?php

/**
 * @author Alisson R. Araújo <alisson@univicosa.com.br>
 */

use Participant\Controller\ListAllOrById;
use Participant\Controller\Factory\ListAllOrByIdFactory;

use Participant\Service\Inscription\ListAllOrByIdService;
use Participant\Service\Inscription\Factory\ListAllOrByIdServiceFactory;

use Participant\Service\Inscription\MergeOrPersistService;
use Participant\Service\Inscription\Factory\MergeOrPersistServiceFactory;

use Participant\Controller\Factory\MergeOrPersistFactory;
use Participant\Controller\MergeOrPersist;


use Zend\Mvc\Router\Http\Literal;
use Zend\Mvc\Router\Http\Segment;

return [
    'controllers' => [
        'factories' => [
            ListAllOrById::class => ListAllOrByIdFactory::class,
            MergeOrPersist::class => MergeOrPersistFactory::class
        ]
    ],
    'service_manager' => [
        'factories' => [
            ListAllOrByIdService::class => ListAllOrByIdServiceFactory::class,
            MergeOrPersistService::class => MergeOrPersistServiceFactory::class
        ],
    ],
    'router' => [
        'routes' => [
            'participantes' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/participantes[/:idactivity]',
                    'constraints' => [
                        'idactivity' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}'
                    ],
                    'defaults' => [
                        'controller' => ListAllOrById::class,
                        'action' => 'listAll'
                    ],
                ]
            ],

            'partipante' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/participante',
                    'defaults' => [
                        'controller' => MergeOrPersist::class
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'adicionar-editar' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/adicionar-editar[/:idevent][/:idactivity]',
                            'constraints' => [
                                'idevent' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}',
                                'idactivity' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}'
                            ],
                            'defaults' => [
                                'action' => 'mergeOrPersist'
                            ]
                        ]
                    ],
                ]
            ]
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy'
        ]
    ]
];
