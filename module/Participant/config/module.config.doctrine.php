<?php

/**
 * @author Alisson R. Araújo <alisson@univicosa.com.br>
 */

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'doctrine' => [
        'driver' => [
            'participant_entities' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Participant/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Participant\Entity' => 'participant_entities'
                ]
            ]
        ]
    ]
];