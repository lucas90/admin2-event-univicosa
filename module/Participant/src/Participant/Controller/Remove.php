<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 21/07/2016
 * Time: 10:01
 */

namespace Participant\Controller;

use Event\Service\Event\RemoveService;
use Wizard\Service\WizardEvent\RemoveService as RemoveServiceWizardEvent;
use Activity\Service\Activity\RemoveService as RemoveServiceActivity;
use Demand\Service\RemoveService as RemoveServiceDemand;
use Costs\Service\RemoveService as RemoveServiceCosts;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class Remove extends AbstractActionController
{
    private $service;
    private $serviceWizardEvent;
    private $serviceActivity;
    private $serviceDemand;
    private $serviceCosts;

    public function __construct(
        RemoveService $removeService,
        RemoveServiceWizardEvent $removeServiceWizardEvent,
        RemoveServiceActivity $removeServiceActivity,
        RemoveServiceDemand $removeServiceDemand,
        RemoveServiceCosts $removeServiceCosts
    ){
        $this->service = $removeService;
        $this->serviceWizardEvent = $removeServiceWizardEvent;
        $this->serviceActivity = $removeServiceActivity;
        $this->serviceDemand = $removeServiceDemand;
        $this->serviceCosts = $removeServiceCosts;
    }

    public function removeEventAction()
    {
        $request = $this->getRequest();
        $jsonModel = new JsonModel();

        if ($request->isXmlHttpRequest()){
            try {
                $eventId = (string) $request->getPost('eventId');


                $this->serviceActivity->removeAllByEvent($eventId);
                $this->serviceDemand->removeAllByEvent($eventId);
                $this->serviceCosts->removeAllByEvent($eventId);
                $this->serviceWizardEvent->removeAllByEvent($eventId);
                $this->service->removeEvent($eventId);

                $jsonModel->setVariables([
                    'success' => true,
                    'message' => 'Evento removido com sucesso.'
                ]);
            } catch(\Exception $e){
                $jsonModel->setVariables([
                    'success' => false,
                    'message' => $e->getMessage()
                ]);
            }
        }

        return $jsonModel;
    }
}