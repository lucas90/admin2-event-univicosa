<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 21/07/2016
 * Time: 09:53
 */

namespace Event\Controller\Factory;

use Event\Service\Event\RemoveService;
use Wizard\Service\WizardEvent\RemoveService as RemoveServiceWizardEvent;
use Activity\Service\Activity\RemoveService as RemoveServiceActivity;
use Demand\Service\RemoveService as RemoveServiceDemand;
use Costs\Service\RemoveService as RemoveServiceCosts;

use Event\Controller\Remove;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RemoveFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $realServiceLocator  = $serviceLocator->getServiceLocator();
        $service = $realServiceLocator->get(RemoveService::class);
        $serviceWizardEvent = $realServiceLocator->get(RemoveServiceWizardEvent::class);
        $serviceActivity = $realServiceLocator->get(RemoveServiceActivity::class);
        $serviceDemand = $realServiceLocator->get(RemoveServiceDemand::class);
        $serviceCosts = $realServiceLocator->get(RemoveServiceCosts::class);

        return new Remove(
            $service,
            $serviceWizardEvent,
            $serviceActivity,
            $serviceDemand,
            $serviceCosts
        );
    }
}