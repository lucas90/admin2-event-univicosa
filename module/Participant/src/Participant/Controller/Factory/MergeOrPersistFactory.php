<?php
/**
 * Created by PhpStorm.
 * User: Alisson R. Araújo
 * Date: 15/09/2016
 * Time: 14:54
 */

namespace Participant\Controller\Factory;

use Participant\Controller\MergeOrPersist;
use Participant\Service\Inscription\MergeOrPersistService;
use Participant\Service\Inscription\ListAllOrByIdService as ListAllOrByIdServiceEvent;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class MergeOrPersistFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        $realServiceLocator = $serviceLocator->getServiceLocator();
        $mergeOrPersistServiceEvent = $realServiceLocator->get(MergeOrPersistService::class);
        $listAllOrByIdServiceEvent = $realServiceLocator->get(ListAllOrByIdServiceEvent::class);

        return new MergeOrPersist(
            $mergeOrPersistServiceEvent,
            $listAllOrByIdServiceEvent
        );
    }

}