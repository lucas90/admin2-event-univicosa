<?php
/**
 * Created by PhpStorm.
 * User: Alisson R. Araújo
 * Date: 15/09/2016
 * Time: 14:54
 */

namespace Participant\Controller;


use Zend\View\Model\ViewModel;

class MergeOrPersist
{

    public function participanteAction()
    {
        return (new ViewModel)
        ->setTemplate('participant/list-all/form');
    }


}