<?php
/**
 * Created by PhpStorm.
 * User: Alisson R. Araújo
 * Date: 15/09/2016
 * Time: 08:41
 */

namespace Participant\Entity;

use Doctrine\ORM\Mapping as ORM;
use Rhumsaa\Uuid\Uuid;

/**
 * Class Inscription
 * @package Participant\Entity
 * @ORM\Entity
 * @ORM\Table(name="participante.inscricao")
 */
class Inscription
{
    /**
     * @ORM\Id
     * @ORM\Column(name="inscricao_id", type="string")
     */
    private $inscriptionId;

    /**
     * @ORM\ManyToOne(targetEntity="Participant\Entity\Participant")
     * @ORM\JoinColumn(name="participante_id", referencedColumnName="participante_id")
     */
    private $paticipant;

    /**
     * @ORM\ManyToOne(targetEntity="Participant\Entity\ParticipantType")
     * @ORM\JoinColumn(name="tipo_participante_id", referencedColumnName="tipo_partipante_id")
     */
    private $paticipanteType;

    /**
     * @ORM\ManyToOne(targetEntity="Event\Entity\Event")
     * @ORM\JoinColumn(name="evento_id", referencedColumnName="evento_id")
     */
    private $event;

    /**
     * @ORM\Column(name="isento", type="boolean")
     */
    private $free;

    /**
     * @ORM\Column(name="presente", type="boolean")
     */
    private $presence;

    /**
     * @ORM\Column(name="pago", type="string")
     */
    private $pay;

    /**
     * @param $paticipant
     * @param $paticipanteType
     * @param $event
     */
    public function __construct($paticipant, $paticipanteType, $event)
    {
        $this->inscriptionId = Uuid::uuid4();
        $this->paticipant = $paticipant;
        $this->paticipanteType = $paticipanteType;
        $this->event = $event;
    }

    /**
     * @return mixed
     */
    public function getInscriptionId()
    {
        return $this->inscriptionId;
    }

    /**
     * @return mixed
     */
    public function getPaticipant()
    {
        return $this->paticipant;
    }

    /**
     * @return mixed
     */
    public function getPaticipanteType()
    {
        return $this->paticipanteType;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @return mixed
     */
    public function getFree()
    {
        return $this->free;
    }

    /**
     * @return mixed
     */
    public function getPresence()
    {
        return $this->presence;
    }

    /**
     * @return mixed
     */
    public function getPay()
    {
        return $this->pay;
    }

    /**
     * @param mixed $free
     */
    public function setFree($free)
    {
        $this->free = $free;
    }

    /**
     * @param mixed $presence
     */
    public function setPresence($presence)
    {
        $this->presence = $presence;
    }

    /**
     * @param mixed $pay
     */
    public function setPay($pay)
    {
        $this->pay = $pay;
    }


}