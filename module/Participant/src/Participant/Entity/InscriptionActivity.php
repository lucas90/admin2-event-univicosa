<?php
/**
 * Created by PhpStorm.
 * User: Alisson R. Araújo
 * Date: 15/09/2016
 * Time: 08:42
 */

namespace Participant\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class InscriptionActivity
 * @package Participant\Entity
 * @ORM\Entity
 * @ORM\Table(name="participante.inscricao_atividade")
 */
class InscriptionActivity
{
    //usada apenas para palestrante
    /**
     * @ORM\Id
     * @ORM\Column(name="inscricao_id", type="string")
     */
    private $inscriptionId;

    /**
     * @ORM\ManyToOne(targetEntity="Activity\Entity\Activity")
     * @ORM\JoinColumn(name="atividade_id", referencedColumnName="atividade_id")
     */
    private $activity;

    /**
     * @param $inscriptionId
     * @param $activity
     */
    public function __construct($inscriptionId, $activity)
    {
        $this->inscriptionId = $inscriptionId;
        $this->activityId = $activity;
    }

    /**
     * @return mixed
     */
    public function getInscriptionId()
    {
        return $this->inscriptionId;
    }

    /**
     * @return mixed
     */
    public function getActivity()
    {
        return $this->activity;
    }



}