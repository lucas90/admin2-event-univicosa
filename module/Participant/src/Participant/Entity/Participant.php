<?php
/**
 * Created by PhpStorm.
 * User: Alisson R. Araújo
 * Date: 15/09/2016
 * Time: 08:41
 */

namespace Participant\Entity;

use Doctrine\ORM\Mapping as ORM;
use Rhumsaa\Uuid\Uuid;


/**
 * Class Participant
 * @package Participant\Entity
 * @ORM\Entity
 * @ORM\Table(name="participante.participante")
 */
class Participant
{
    //user_id
    /**
     * @ORM\Id
     * @ORM\Column(name="participante_id", type="string")
     */
    private $participantId;

    /**
     * @ORM\Column(name="nome", type="string")
     */
    private $name;

    /**
     * @ORM\Column(name="email", type="string")
     */
    private $email;

    /**
     * @ORM\Column(name="matricula", type="string")
     */
    private $registration;

    /**
     * @ORM\Column(name="cpf", type="string")
     */
    private $cpf;

    /**
     * Participant constructor.
     * @param $name
     * @param $email
     * @param $registration
     * @param $cpf
     */
    public function __construct($name, $email, $registration, $cpf)
    {
        $this->participantId = Uuid::uuid4();
        $this->name = $name;
        $this->email = $email;
        $this->registration = $registration;
        $this->cpf = $cpf;
    }


    /**
     * @return mixed
     */
    public function getParticipantId()
    {
        return $this->participantId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getRegistration()
    {
        return $this->registration;
    }

    /**
     * @return mixed
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $registration
     */
    public function setRegistration($registration)
    {
        $this->registration = $registration;
    }

    /**
     * @param mixed $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }


}