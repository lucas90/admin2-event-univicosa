<?php
/**
 * Created by PhpStorm.
 * User: Alisson R. Araújo
 * Date: 15/09/2016
 * Time: 08:42
 */

namespace Participant\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ParticipantType
 * @package Participant\Entity
 * @ORM\Entity
 * @ORM\Table(name="participante.tipo_participante")
 */
class ParticipantType
{
    /**
     * @ORM\Id
     * @ORM\Column(name="tipo_paticipante_id", type="string")
     */
    private $participantTypeId;

    /**
     * @ORM\Column(name="type", type="string")
     */
    private $type;

    /**
     * @return mixed
     */
    public function getParticipantTypeId()
    {
        return $this->participantTypeId;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }


}