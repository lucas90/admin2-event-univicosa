<?php
/**
 * Created by PhpStorm.
 * User: Alisson R. Araújo
 * Date: 15/09/2016
 * Time: 14:50
 */

namespace Participant\Service\Inscription\Factory;

use Participant\Service\Inscription\ListAllOrByIdService;
use Participant\Repository\Inscription\ListByEventRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ListAllOrByIdServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new ListAllOrByIdService(
            new ListByEventRepository($entityManager)
        );
    }
}