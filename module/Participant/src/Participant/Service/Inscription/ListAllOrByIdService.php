<?php
/**
 * Created by PhpStorm.
 * User: Alisson R. Araújo
 * Date: 15/09/2016
 * Time: 14:50
 */

namespace Participant\Service\Inscription;

use Participant\Repository\Inscription\ListByEventRepository;

class ListAllOrByIdService
{
    private $listByEvent;

    public function __construct(
        ListByEventRepository $listByEventRepository
    ){
        $this->listByEvent = $listByEventRepository;
    }

    public function listByEvent($event)
    {
        $repository = $this->listByEvent;

        return $repository($event);
    }
}