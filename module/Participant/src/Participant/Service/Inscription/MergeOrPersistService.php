<?php
/**
 * Created by PhpStorm.
 * User: Alisson R. Araújo
 * Date: 15/09/2016
 * Time: 14:51
 */

namespace Participant\Service\Inscription;


use Core\Doctrine\ObjectManagerAbstract;
use Doctrine\Common\Persistence\ObjectManager;

class MergeOrPersistService extends ObjectManagerAbstract
{
    public function __construct(
        ObjectManager $objectManager
    ){
        parent::__construct($objectManager);
    }

    public function __invoke(array $data)
    {
        return $data;
    }
}