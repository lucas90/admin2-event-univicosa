<?php
/**
 * Created by PhpStorm.
 * User: Alisson R. Araújo
 * Date: 15/09/2016
 * Time: 14:38
 */

namespace Participant\Repository\InscriptionActivity;

use Participant\Entity\InscriptionActivity;

use Core\Doctrine\ObjectManagerAbstract;

class ListByInscriptionRepository extends ObjectManagerAbstract
{
    public function __invoke($inscriptionId)
    {
        return $this->objectManager->getRepository(InscriptionActivity::class)->findBy(
            ['inscriptionId' => $inscriptionId]
        );
    }
}