<?php
/**
 * Created by PhpStorm.
 * User: Alisson R. Araújo
 * Date: 15/09/2016
 * Time: 14:36
 */

namespace Participant\Repository\Inscription;

use Participant\Entity\Inscription;

use Core\Doctrine\ObjectManagerAbstract;

class ListByEventRepository extends ObjectManagerAbstract
{
    public function __invoke($event)
    {
        return $this->objectManager->getRepository(Inscription::class)->findBy(
            ['event' => $event]
        );
    }
}