<?php
/**
 * Created by PhpStorm.
 * User: Alisson R. Araújo
 * Date: 15/09/2016
 * Time: 14:39
 */

namespace Participant\Repository\ParticipantType;

use Participant\Entity\ParticipantType;

use Core\Doctrine\ObjectManagerAbstract;

class ListAllRepository extends ObjectManagerAbstract
{
    public function __invoke()
    {
        return $this->objectManager->getRepository(ParticipantType::class)->findAll();
    }
}