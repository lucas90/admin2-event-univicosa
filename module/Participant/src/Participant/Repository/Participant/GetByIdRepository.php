<?php
/**
 * Created by PhpStorm.
 * User: Alisson R. Araújo
 * Date: 15/09/2016
 * Time: 14:44
 */

namespace Participant\Repository\Participant;

use Participant\Entity\Participant;

use Core\Doctrine\ObjectManagerAbstract;

class GetByIdRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(Participant::class)->findOneBy(
            ['participantId' => $id]
        );
    }
}