<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira90@gmail.com>
 */

use Modules\Service\Module\Factory\MergeOrPersistServiceFactory;
use Modules\Service\Module\MergeOrPersistService;

use Modules\Service\Module\ListAllOrByIdService;
use Modules\Service\Module\Factory\ListAllOrByIdServiceFactory;

use Modules\Service\ModuleEvent\ListAllOrByIdService as ListAllOrByIdServiceModuleEvent;
use Modules\Service\ModuleEvent\Factory\ListAllOrByIdServiceFactory as ListAllOrByIdServiceFactoryModuleEvent;

use Modules\Service\Style\ListAllOrByIdService as ListAllOrByIdServiceStyle;
use Modules\Service\Style\Factory\ListAllOrByIdServiceFactory as ListAllOrByIdServiceFactoryStyle;

use Modules\Controller\Factory\MergeOrPersistFactory;
use Modules\Controller\MergeOrPersist;

use Modules\Controller\Factory\ListAllOrByIdFactory;
use Modules\Controller\ListAllOrById;

use Zend\Mvc\Router\Http\Literal;
use Zend\Mvc\Router\Http\Segment;

return [
    'controllers' => [
        'factories' => [
            MergeOrPersist::class => MergeOrPersistFactory::class,
            ListAllOrById::class => ListAllOrByIdFactory::class
        ]
    ],

    'service_manager' => [
        'factories' => [
            ListAllOrByIdServiceModuleEvent::class => ListAllOrByIdServiceFactoryModuleEvent::class,
            ListAllOrByIdService::class => ListAllOrByIdServiceFactory::class,
            MergeOrPersistService::class => MergeOrPersistServiceFactory::class,
            ListAllOrByIdServiceStyle::class => ListAllOrByIdServiceFactoryStyle::class
        ]
    ],

    'router' => [
        'routes' => [
            'modulos' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/modulos[/:idevent]',
                    'constraints' => [
                        'idevent' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}'
                    ],
                    'defaults' => [
                        'controller' => ListAllOrById::class,
                        'action' => 'onDispatch',
                    ],
                ]
            ],

            'modulo' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/modulo',
                    'defaults' => [
                        'controller' => MergeOrPersist::class
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'selecao' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/selecao[/:idevent]',
                            'constraints' => [
                                'idevent' => '[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}'
                            ],
                            'defaults' => [
                                'action' => 'onDispatch'
                            ]
                        ]
                    ],

                ]
            ]
        ]
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy'
        ]
    ]
];
