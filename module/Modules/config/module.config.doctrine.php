<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'doctrine' => [
        'driver' => [
            'modules_entities' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Modules/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Modules\Entity' => 'modules_entities'
                ]
            ]
        ]
    ]
];