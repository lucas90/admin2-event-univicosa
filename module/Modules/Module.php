<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 06/07/2016
 * Time: 14:13
 */

namespace Modules;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{

    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return array_merge_recursive(
            require __DIR__ . '/config/module.config.php',
            require __DIR__ . '/config/module.config.doctrine.php'
        );
    }
}