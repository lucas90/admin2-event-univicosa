<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 22/08/2016
 * Time: 13:22
 */

namespace Modules\Service\Style\Factory;

use Modules\Service\Style\ListAllOrByIdService;
use Modules\Repository\Style\ListAllRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ListAllOrByIdServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new ListAllOrByIdService(
            new ListAllRepository($entityManager)
        );
    }
}