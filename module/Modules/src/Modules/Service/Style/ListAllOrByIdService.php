<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 22/08/2016
 * Time: 13:23
 */

namespace Modules\Service\Style;

use Modules\Repository\Style\ListAllRepository;

class ListAllOrByIdService
{
    private $listAll;

    public function __construct(
        ListAllRepository $listAllRepository
    ){
        $this->listAll = $listAllRepository;
    }

    public function listAll()
    {
        $repository = $this->listAll;

        return $repository();
    }
}