<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 06/07/2016
 * Time: 14:18
 */

namespace Modules\Service\Module;

use Modules\Repository\Module\ListAllRepository;
use Modules\Repository\Module\GetByIdRepository;
use Modules\Repository\Module\GetByNameRepository;

class ListAllOrByIdService
{

    private $listAllModules;
    private $getById;
    private $getByName;

    public function __construct(
        ListAllRepository $listAllModulesRepository,
        GetByIdRepository $getByIdRepository,
        GetByNameRepository $getByNameRepository
    ){
        $this->listAllModules = $listAllModulesRepository;
        $this->getById = $getByIdRepository;
        $this->getByName = $getByNameRepository;
    }

    public function listAllModules()
    {
        $repository = $this->listAllModules;

        return $repository();
    }

    public function getById($id)
    {
        $repository = $this->getById;

        return $repository($id);
    }

    public function getByName($name)
    {
        $repository = $this->getByName;

        return $repository($name);
    }
}