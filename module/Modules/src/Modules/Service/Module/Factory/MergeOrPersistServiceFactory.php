<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 12/08/2016
 * Time: 08:45
 */

namespace Modules\Service\Module\Factory;

use Modules\Service\Module\MergeOrPersistService;

use Event\Repository\Event\GetByIdRepository as GetEventByIDRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MergeOrPersistServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new MergeOrPersistService(
            $entityManager,
            new GetEventByIDRepository($entityManager)
        );
    }
}