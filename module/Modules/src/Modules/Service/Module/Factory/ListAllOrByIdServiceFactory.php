<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 06/07/2016
 * Time: 14:36
 */

namespace Modules\Service\Module\Factory;

use Modules\Service\Module\ListAllOrByIdService;
use Modules\Repository\Module\ListAllRepository;
use Modules\Repository\Module\GetByIdRepository;
use Modules\Repository\Module\GetByNameRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ListAllOrByIdServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new ListAllOrByIdService(
            new ListAllRepository($entityManager),
            new GetByIdRepository($entityManager),
            new GetByNameRepository($entityManager)
        );
    }
}