<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 12/08/2016
 * Time: 08:46
 */

namespace Modules\Service\Module;

use Doctrine\Common\Persistence\ObjectManager;

use Event\Repository\Event\GetByIdRepository as GetEventByIdRepository;

use Core\Doctrine\ObjectManagerAbstract;

class MergeOrPersistService extends ObjectManagerAbstract
{
    private $getEventByid;

    public function __construct(
        ObjectManager $objectManager,
        GetEventByIdRepository $getEventById
    )
    {
        parent::__construct($objectManager);
        $this->getEventByid = $getEventById;
    }

    public function __invoke(array $data)
    {
        return null;
    }
}