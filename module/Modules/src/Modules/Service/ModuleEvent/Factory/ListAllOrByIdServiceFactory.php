<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 06/07/2016
 * Time: 15:20
 */

namespace Modules\Service\ModuleEvent\Factory;

use Modules\Service\ModuleEvent\ListAllOrByIdService;
use Modules\Repository\ModuleEvent\GetByIdRepository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ListAllOrByIdServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('doctrine.entitymanager.em_pgsql');

        return new ListAllOrByIdService(
            new GetByIdRepository($entityManager)
        );
    }
}