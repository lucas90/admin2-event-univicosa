<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 06/07/2016
 * Time: 15:19
 */

namespace Modules\Service\ModuleEvent;

use Modules\Repository\ModuleEvent\GetByIdRepository;

class ListAllOrByIdService
{
    private $getById;

    public function __construct(
        GetByIdRepository $getByIdRepository
    ){
        $this->getById = $getByIdRepository;
    }

    public function getById($id)
    {
        $repository = $this->getById;

        return $repository($id);
    }
}