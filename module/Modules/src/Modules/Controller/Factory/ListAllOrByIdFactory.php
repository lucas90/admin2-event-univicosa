<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 12/08/2016
 * Time: 09:15
 */

namespace Modules\Controller\Factory;

use Modules\Controller\ListAllOrById;
use Wizard\Service\WizardEvent\ListAllOrByIdService as ListAllOrByIdServiceWizardEvent;
use Modules\Service\Style\ListAllOrByIdService as ListAllOrByIdServiceStyle;
use Event\Service\Event\ListAllOrByIdService as ListAllOrByIdServiceEvent;
use Wizard\Service\Wizard\ListAllOrByIdService as ListAllOrByIdServiceWizard;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ListAllOrByIdFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $realServiceLocator  = $serviceLocator->getServiceLocator();
        $listAllOrByIdServiceWizard = $realServiceLocator->get(ListAllOrByIdServiceWizardEvent::class);
        $listAllOrByIdStyle = $realServiceLocator->get(ListAllOrByIdServiceStyle::class);
        $listAllOrByIdEvent = $realServiceLocator->get(ListAllOrByIdServiceEvent::class);
        $listAllOrByIdWizard = $realServiceLocator->get(ListAllOrByIdServiceWizard::class);

        return new ListAllOrById(
            $listAllOrByIdServiceWizard,
            $listAllOrByIdStyle,
            $listAllOrByIdEvent,
            $listAllOrByIdWizard
        );
    }
}