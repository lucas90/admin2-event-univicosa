<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 12/08/2016
 * Time: 08:47
 */

namespace Modules\Controller\Factory;

use Modules\Controller\MergeOrPersist;
use Wizard\Service\WizardEvent\MergeOrPersistService as MergeOrPersistServiceWizardEvent;
use Wizard\Service\Wizard\ListAllOrByIdService as ListAllOrByIdServiceWizard;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MergeOrPersistFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $realServiceLocator  = $serviceLocator->getServiceLocator();
        $mergeOrPersistWizard = $realServiceLocator->get(MergeOrPersistServiceWizardEvent::class);
        $listAllOrByIdWizard = $realServiceLocator->get(ListAllOrByIdServiceWizard::class);
        
        return new MergeOrPersist(
            $mergeOrPersistWizard,
            $listAllOrByIdWizard
        );
    }
}