<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 12/08/2016
 * Time: 09:16
 */

namespace Modules\Controller;

use Wizard\Service\WizardEvent\ListAllOrByIdService as ListAllOrByIdServiceWizardEvent;
use Modules\Service\Style\ListAllOrByIdService as ListAllOrByIdServiceStyle;
use Event\Service\Event\ListAllOrByIdService as ListAllOrByIdServiceEvent;
use Wizard\Service\Wizard\ListAllOrByIdService as ListAllOrByIdServiceWizard;
use Wizard\Entity\Wizard;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ListAllOrById extends AbstractActionController
{
    private $serviceWizardEvent;
    private $serviceStyle;
    private $serviceEvent;
    private $serviceWizard;

    public function __construct(
        ListAllOrByIdServiceWizardEvent $service,
        ListAllOrByIdServiceStyle $serviceStyle,
        ListAllOrByIdServiceEvent $serviceEvent,
        ListAllOrByIdServiceWizard $serviceWizard
    ){
        $this->serviceWizardEvent = $service;
        $this->serviceStyle = $serviceStyle;
        $this->serviceEvent = $serviceEvent;
        $this->serviceWizard = $serviceWizard;
    }

    public function onDispatchAction()
    {
        $idEvent = (string) $this->params()->fromRoute('idevent');
        $event = $this->serviceEvent->getById($idEvent);
        $styles = $this->serviceStyle->listAll();
        $modulesEvent = $this->serviceWizardEvent->listAllByEvent($idEvent);
        $modules = [];
        foreach($modulesEvent as $module){
            $modules[] = $this->serviceWizard->getById($module->getWizardId());
        }

        //ordenando os modulos
        $this->arrayOrder($modules);

        $viewModel = new ViewModel();
        $viewModel->setTemplate('modules/list-all/index');
        $viewModel->setVariables([
            'modules' => $modules,
            'event' => $event,
            'styles' => $styles
        ]);

        return $viewModel;
    }

    function arrayOrder($modules)
    {
        usort($modules, function(Wizard $obj1, Wizard $obj2) {
            if ($obj1->getOrder() < $obj2->getOrder()) {
                return -1;
            } elseif ($obj1->getOrder() > $obj2->getOrder()) {
                return +1;
            }
            return 0;
        });

    }
}