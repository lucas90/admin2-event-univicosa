<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 12/08/2016
 * Time: 08:47
 */

namespace Modules\Controller;

use Wizard\Service\WizardEvent\MergeOrPersistService as MergeOrPersistServiceWizardEvent;
use Wizard\Service\Wizard\ListAllOrByIdService as ListAllOrByIdServiceWizard;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class MergeOrPersist extends AbstractActionController
{
    private $mergeOrPersistWizardEvent;
    private $serviceWizard;

    public function __construct(
        MergeOrPersistServiceWizardEvent $mergeOrPersistServiceWizardEvent,
        ListAllOrByIdServiceWizard $listAllOrByIdServiceWizard

    ){
        $this->mergeOrPersistWizardEvent = $mergeOrPersistServiceWizardEvent;
        $this->serviceWizard = $listAllOrByIdServiceWizard;
    }

    public function onDispatchAction()
    {
        $request = $this->getRequest();
        $wizard = $this->serviceWizard->listAll();

        if($request->isXmlHttpRequest()){
            $jsonModel = new JsonModel();

            $data = $request->getPost('answers');
            $data['eventId'] = $request->getPost('eventId');

            try{

                $mergeOrPersist = $this->mergeOrPersistWizardEvent;
                $mergeOrPersist($data);

                $jsonModel->setVariables([
                    'success' => true,
                    'message' => 'Módulos adicionados com sucesso',
                    'eventId' => $request->getPost('eventId')
                ]);
            }catch (\Exception $e){

                $jsonModel->setVariables([
                    'success' => false,
                    'message' => $e->getMessage()
                ]);
            }

            return $jsonModel;
        }

        $viewModel = new ViewModel();
        $viewModel->setTemplate('modules/merge-or-persist/index');
        $viewModel->setVariables([
            'steps' => $wizard
        ]);

        return $viewModel;
    }
}