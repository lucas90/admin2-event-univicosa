<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 06/07/2016
 * Time: 14:16
 */

namespace Modules\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Module
 * @package Event\Entity
 * @ORM\Entity
 * @ORM\Table(name="modulo")
 */
class Module
{
    /**
     * @ORM\Id
     * @ORM\Column(name="modulo_id", type="string")
     */
    private $moduleId;

    /**
     * @ORM\Column(name="modulo", type="string")
     */
    private $module;

    /**
     * @ORM\Column(name="schema", type="string")
     */
    private $schema;

    /**
     * @ORM\Column(name="action", type="string")
     */
    private $action;

    /**
     * @return mixed
     */
    public function getModuleId()
    {
        return $this->moduleId;
    }

    /**
     * @return mixed
     */
    public function getModule()
    {
        return $this->module;
    }


    /**
     * @return mixed
     */
    public function getSchema()
    {
        return $this->schema;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }


}