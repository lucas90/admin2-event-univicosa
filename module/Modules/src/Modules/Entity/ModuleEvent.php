<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 16/08/2016
 * Time: 09:02
 */

namespace Modules\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ModuleEvent
 * @package Modules\Entity
 * @ORM\Entity
 * @ORM\Table(name="modulo_evento")
 */
class ModuleEvent
{

    /**
     * @ORM\ManyToOne(targetEntity="Module\Entity\Module")
     * @ORM\JoinColumn(name="modulo_id", referencedColumnName="modulo_id")
     */
    private $module;

    /**
     * @ORM\ManyToOne(targetEntity="Event\Entity\Event")
     * @ORM\JoinColumn(name="evento_id", referencedColumnName="evento_id")
     */
    private $event;

    /**
     * ModuleEvent constructor.
     * @param $module
     * @param $event
     */
    public function __construct($module, $event)
    {
        $this->module = $module;
        $this->event = $event;
    }

    /**
     * @return mixed
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }



}