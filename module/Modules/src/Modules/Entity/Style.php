<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 22/08/2016
 * Time: 11:29
 */

namespace Modules\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Style
 * @package Modules\Entity
 * @ORM\Entity
 * @ORM\Table(name="estilo")
 */
class Style
{
    /**
     * @ORM\Id
     * @ORM\Column(name="estilo_id", type="string")
     */
    private $styleId;

    /**
     * @ORM\OneToOne(targetEntity="Modules\Entity\Module")
     * @ORM\JoinColumn(name="modulo_id", referencedColumnName="modulo_id")
     */
    private $module;

    /**
     * @ORM\Column(name="icone", type="string")
     */
    private $icon;

    /**
     * @ORM\Column(name="cor", type="string")
     */
    private $color;

    /**
     * @ORM\Column(name="descricao", type="string")
     */
    private $description;

    /**
     * @return mixed
     */
    public function getStyleId()
    {
        return $this->styleId;
    }

    /**
     * @return mixed
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }


}