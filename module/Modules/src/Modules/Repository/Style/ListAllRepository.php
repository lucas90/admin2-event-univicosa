<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 22/08/2016
 * Time: 13:25
 */

namespace Modules\Repository\Style;

use Modules\Entity\Style;

use Core\Doctrine\ObjectManagerAbstract;

class ListAllRepository extends ObjectManagerAbstract
{

    public function __invoke()
    {
        return $this->objectManager->getRepository(Style::class)->findAll();
    }
}