<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 06/07/2016
 * Time: 15:18
 */

namespace Modules\Repository\ModuleEvent;

use Modules\Entity\ModuleEvent;
use Core\Doctrine\ObjectManagerAbstract;

class GetByIdRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(ModuleEvent::class)->find($id);
    }
}