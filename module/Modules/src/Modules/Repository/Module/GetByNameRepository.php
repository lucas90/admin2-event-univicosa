<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 23/08/2016
 * Time: 10:48
 */

namespace Modules\Repository\Module;

use Modules\Entity\Module;

use Core\Doctrine\ObjectManagerAbstract;

class GetByNameRepository extends ObjectManagerAbstract
{
    public function __invoke($modulo)
    {
        return $this->objectManager->getRepository(Module::class)->findOneBy(
            ['module' => $modulo]
        );
    }
}