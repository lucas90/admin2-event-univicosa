<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 06/07/2016
 * Time: 14:17
 */

namespace Modules\Repository\Module;

use Core\Doctrine\ObjectManagerAbstract;
use Modules\Entity\Module;

class ListAllRepository extends ObjectManagerAbstract
{
    public function __invoke()
    {
        return $this->objectManager->getRepository(Module::class)->findAll();
    }
}