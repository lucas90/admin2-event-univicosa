<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 21/07/2016
 * Time: 08:23
 */

namespace Modules\Repository\Module;

use Modules\Entity\Module;

use Core\Doctrine\ObjectManagerAbstract;

class GetByIdRepository extends ObjectManagerAbstract
{
    public function __invoke($id)
    {
        return $this->objectManager->getRepository(Module::class)->find($id);
    }
}