<?php

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */

use Core\ControllerPlugin\PaginatorConfigPlugin;
use Core\ViewHelper\ActiveMenuHelper;

return [
    'controller_plugins' => [
        'factories' => [
            'paginatorControllerPlugin' => function($serviceManager) {
                $serviceLocator = $serviceManager->getServiceLocator();

                return new PaginatorConfigPlugin($serviceLocator);
            }
        ]
    ],
    'view_helpers' => [
        'factories' => [
            'activeMenuHelper' => function($serviceManager) {
                $serviceLocator = $serviceManager->getServiceLocator();

                return new ActiveMenuHelper($serviceLocator);
            }
        ]
    ],
];
