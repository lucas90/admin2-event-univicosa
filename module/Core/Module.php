<?php

namespace Core;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * @author  Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class Module implements ConfigProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfig()
    {
        return require __DIR__ . '/config/module.config.php';
    }
}