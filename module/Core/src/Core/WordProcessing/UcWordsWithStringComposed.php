<?php

namespace Core\WordProcessing;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
final class UcWordsWithStringComposed
{
    /**
     * @var \[] $wordsToReplace
     */
    private $wordsToReplace = [
        'da', 'de', 'di', 'do', 'du',
        'das', 'des', 'dis', 'dos', 'dus'
    ];

    /**
     * @var string $nameOrTitle
     */
    private $nameOrTitle;

    /**
     * @param string $nameOrTitle
     */
    public function __construct($nameOrTitle)
    {
        $this->treatWordsToUcWords($nameOrTitle);
    }

    /**
     * @param string $nameOrTitle
     * @see http://stackoverflow.com/questions/2109325/how-to-strip-all-spaces-out-of-a-string-in-php/2109339
     * @return string
     */
    private function treatWordsToUcWords($nameOrTitle)
    {
        $removeSpacesBetweenWords = preg_replace('/\s+/', ' ', $nameOrTitle);

        $wordsToReplace = $this->wordsToReplace;
        $words = explode(' ', $removeSpacesBetweenWords);
        $nameOrTitle = [];

        foreach ($words as $word) {
            $toLower = mb_strtolower($word);

            $nameOrTitle[] = !in_array($toLower, $wordsToReplace) ? ucwords($toLower) : $toLower;
        }

        $this->nameOrTitle = implode(' ', $nameOrTitle);
    }

    public function __toString()
    {
        return $this->nameOrTitle;
    }
}