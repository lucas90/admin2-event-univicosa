<?php

namespace Core\Doctrine;

use Doctrine\Common\Persistence\ObjectManager;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
abstract class ObjectManagerAbstract
{
    /**
     * @var ObjectManager $objectManager
     */
    protected $objectManager;

    /**
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }
}