<?php

namespace Core\Doctrine;

use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Guilherme Pereira Nogueira <guilhermenogueira90@gmail.com>
 */
abstract class AbstractEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
}