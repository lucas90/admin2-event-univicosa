<?php

namespace Core\Doctrine;

use Doctrine\ORM\EntityManager;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
abstract class EntityManagerAbstract
{
    /**
     * @var EntityManager $entityManager
     */
    protected $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
}