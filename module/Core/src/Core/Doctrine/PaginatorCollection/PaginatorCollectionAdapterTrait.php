<?php

namespace Core\Doctrine\PaginatorCollection;

use Zend\Paginator\Paginator;
use Doctrine\Common\Collections\ArrayCollection;
use DoctrineModule\Paginator\Adapter\Collection as CollectionAdapter;

/**
 * @author  Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
trait PaginatorCollectionAdapterTrait
{
    /**
     * @param \[] $collection
     *
     * @return Paginator
     */
    public function mountCollection(array $collection)
    {
        $collectionAdapter = new CollectionAdapter(
            new ArrayCollection($collection)
        );

        return new Paginator($collectionAdapter);
    }
}