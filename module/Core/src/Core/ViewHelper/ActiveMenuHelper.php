<?php

namespace Core\ViewHelper;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class ActiveMenuHelper extends AbstractHelper
{
    /**
     * @var \Zend\Http\Request $request
     */
    private $event;

    /**
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->event = $serviceLocator->get('Application')->getMvcEvent();
    }

    /**
     * @param string ...$routes
     */
    public function __invoke(...$routes)
    {
        $matchedRouteName = (string) $this->event->getRouteMatch()->getMatchedRouteName();

        if (is_array($routes) && count($routes) > 0) {
            array_filter($routes, function($routeName) use ($matchedRouteName) {
                 echo $matchedRouteName === $routeName ? 'activeMenu' : null;
            });
        }
    }
}