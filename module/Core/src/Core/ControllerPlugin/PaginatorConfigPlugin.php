<?php

namespace Core\ControllerPlugin;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Paginator\Paginator;

/**
 * @author Guilherme P. Nogueira <guilhermenogueira@univicosa.com.br>
 */
class PaginatorConfigPlugin extends AbstractPlugin
{
    /**
     * @var \[] $config
     */
    private $config;

    /**
     * @var int $currentPageNumber
     */
    private $currentPageNumber;

    /**
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @throws \Exception `se o arquivo paginator não estiver incluso`
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $event = $serviceLocator->get('Application')->getMvcEvent();
        $config = $serviceLocator->get('Config');

        if (! isset($config['paginator'])) {
            throw new \RuntimeException(sprintf(
                'O arquivo de paginação "%s" não está incluso no arquivo de configuração "%s"',
                'config/paginator/paginator.config.php',
                'config/application.config.php'
            ));
        }

        $this->config = $config['paginator'];
        $this->currentPageNumber = $event->getRouteMatch()->getParam('pagina');
    }

    /**
     * @param Paginator $paginator
     *
     * @return Paginator
     */
    public function __invoke(Paginator $paginator)
    {
        $paginator->setItemCountPerPage($this->config['item_count_per_page']);
        $paginator->setCurrentPageNumber($this->currentPageNumber);

        return $paginator;
    }
}
